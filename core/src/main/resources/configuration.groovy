import org.nemesis.music.core.plugin.ID3TagPopulatorPlugin

environments {
    defaults {
        downloadFolder = "${System.getProperty("user.home")}${File.separator}Music"
        baseURIs = [
                MYZUKA : 'http://myzuka.ru:80',
                JETUNE : 'http://www.jetune.ru:80',
                MUSICMP3SPB : 'http://musicmp3spb.org:80',
                VKONTAKTE : 'http://vk.com:80'
        ]
        baseURI = "http://myzuka.ru:80"
        downloadThreadsCount = 5
        retryTimes = 3

        plugins = [
                TRACK_COMPLETED : [ID3TagPopulatorPlugin]
        ]

        preAuthenticationCredentials = [
                JETUNE : new Tuple('nemesisscraper', 'scraper'),
                VKONTAKTE: new Tuple('nuru666@gmail.com', '123')
        ]
    }
}