package org.nemesis.music.core.scraper.jetune

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.Configuration
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Track
import org.nemesis.music.domain.ScraperType
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 6:47 PM
 */
class JetuneTrackScraper implements TrackScraper {

    @Override
    List<Track> getAlbumTracks(Album album) {
        List<Track> result = new ArrayList<Track>()
        def bot = new JerryBot()
        bot.navigate album.link.toASCIIString()
        bot.$$("#itemslist tr:gt(0)").each {
            result.add(extractTrack(it, album))
        }
        return result
    }

    /**
     * Extracts Track from specified DOM element
     * @param element - DOM element to parse
     * @param album - related Album instance
     * @return - extracted Track instance
     */
    private Track extractTrack(Jerry element, Album album) {
        def trackNo = Integer.parseInt(element.$("td:eq(0)").text()?.trim())
        def trackLink = element.$("td:eq(4) a:eq(0)")
        def trackName = trackLink.text()?.trim()
        def baseURI = Configuration.getBaseURI(ScraperType.JETUNE)
        def id = PartsExtractor.instance.extractFromString(trackLink.attr("href"), 1, 2)
        def downloadPageLink = new URI("${baseURI.scheme}://${baseURI.rawAuthority}${trackLink.attr("href")}")
        def duration = element.$("td:eq(6)").text()?.trim()
        def bitRate = element.$("td:eq(8)").text()?.trim()
        def size = element.$("td:eq(10)").text()?.trim()

        return new Track(
                id: id,
                trackNo: trackNo,
                link: downloadPageLink,
                title: trackName,
                duration: duration,
                size: size,
                bitRate: bitRate,
                album: album)
    }
}
