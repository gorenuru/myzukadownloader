package org.nemesis.music.core.plugin

import org.nemesis.music.core.Configuration
import org.nemesis.music.core.event.DownloadEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * User: nuru
 * Date: 01.12.12
 * Time: 23:35
 */
class PluginExecutor {
    private final static Logger logger = LoggerFactory.getLogger(PluginExecutor.class)
    private final static PluginExecutor INSTANCE = new PluginExecutor()

    public static PluginExecutor getInstance() {
        return INSTANCE
    }

    public void executePlugins(DownloadEvent event) {
        def plugins = Configuration.plugins
        List<Class<Plugin>> pluginList = plugins.get(event.type.name())
        pluginList.each {
            Plugin instance = it.newInstance()
            try {
                instance.execute(event)
            } catch (Exception e) {
                logger.warn("Error during plugin execution.", e)
            }
        }
    }
}
