package org.nemesis.music.core.service

import org.nemesis.music.core.scraper.ScraperResolver
import org.nemesis.music.core.util.ScraperUtil
import org.nemesis.music.domain.ScraperType
import org.nemesis.music.domain.Artist

/**
 * User: aalbul
 * Date: 12/25/12
 * Time: 3:15 PM
 *
 * Service to work with Artist related entities
 */
class ArtistService {
    private final static ArtistService INSTANCE = new ArtistService()

    private ArtistService() {
    }

    public static ArtistService getInstance() {
        return INSTANCE
    }

    /**
     * Retrieves list of Artists by specified criteria
     * @param artistName - artist name
     * @param scraperType - scraper type
     * @return list of found Artists
     */
    public List<Artist> getArtistsByKeyword(String artistName, ScraperType scraperType) {
        List<Artist> artists = ScraperResolver.instance.getArtistScraper(scraperType).getArtistsByKeyword(artistName)
        ScraperUtil.populateScraperType(artists, scraperType)
        return artists
    }
}
