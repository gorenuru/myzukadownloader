package org.nemesis.music.core.scraper.allmusic

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.core.util.PartsExtractor
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Artist

/**
 * User: aalbul
 * Date: 1/10/13
 * Time: 6:00 PM
 */
class AllmusicAlbumScraper implements AlbumScraper {

    @Override
    List<Album> getListOfAlbums(Artist artist) {
        List<Album> albums = new ArrayList<>()
        def bot = new JerryBot()
        bot.navigate artist.link.toASCIIString()
        bot.$$(".album-table tr:gt(0)").each {
            albums.add(extractAlbum(it, artist))
        }
        return albums
    }

    /**
     * Extracts Album instance from specified DOM element
     * @param albumElement - DOM element to parse
     * @param artist - related Artist instance
     * @return extracted Album instance
     */
    private Album extractAlbum(Jerry albumElement, Artist artist) {
        def albumLink = albumElement.$("a.full-title")
        def title = albumLink.text()?.trim()
        def id = PartsExtractor.instance.extractFromString(albumLink.attr('href') + "/", 3, 4)
        def releaseDate = albumElement.$(".year").text()?.trim()
        def rating = albumElement.$(".rating").attr("data-stars")
        return new Album(
                id: id,
                title: title,
                releaseDate: releaseDate,
                rating: rating,
                link: new URI(albumLink.attr('href')),
                artist: artist)
    }
}
