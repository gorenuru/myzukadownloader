package org.nemesis.music.core.util

/**
 * User: aalbul
 * Date: 12/7/12
 * Time: 3:16 PM
 *
 * This class is mainly used to extract information parts from different places
 */
class PartsExtractor {
    private final static PartsExtractor INSTANCE = new PartsExtractor()

    public static PartsExtractor getInstance() {
        return INSTANCE
    }

    /**
     * Extracts part from the string
     * @param string - string contains valuable information
     * @param firstIndex - first delimited index
     * @param lastIndex - last delimited index
     * @return extracted part
     */
    public String extractFromString(String string, Integer firstIndex, Integer lastIndex) {
        return extractFromString(string, "/", firstIndex, lastIndex)
    }

    /**
     * Extracts from the entity related link
     * @param string - string contains valuable information
     * @param firstIndex - first delimited index
     * @return extracted part
     */
    public String extractFromString(String string, Integer firstIndex) {
        return extractFromString(string, "/", firstIndex, null)
    }

    /**
     * Extracts from the string
     * @param string - string contains valuable information
     * @param delimiter - statements delimited
     * @param firstIndex - first delimited index
     * @param lastIndex - last delimited index
     * @return extracted part
     */
    public String extractFromString(String string, String delimiter, Integer firstIndex, Integer lastIndex) {
        String result = null
        List<Number> slashOccurrences = string.findIndexValues {
            return it.equals(delimiter)
        }
        if (slashOccurrences.size() >= firstIndex && (lastIndex == null || slashOccurrences.size() >= lastIndex)) {
            def startFrom = slashOccurrences.get(firstIndex).intValue() + 1
            def endWith = lastIndex ? slashOccurrences.get(lastIndex).intValue() : string.size()
            result = string.substring(startFrom, endWith)
        }
        return result
    }
}
