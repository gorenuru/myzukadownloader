package org.nemesis.music.core.scraper.allmusic

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.domain.Artist

/**
 * User: aalbul
 * Date: 1/10/13
 * Time: 5:46 PM
 *
 * Allmusic.com specific artist scraper
 */
class AllmusicArtistScraper implements ArtistScraper {
    @Override
    List<Artist> getArtistsByKeyword(String artistName) {
        List<Artist> result = new ArrayList<>()
        def bot = new JerryBot()
        bot.navigate "http://www.allmusic.com/search/typeahead/artist/${URLEncoder.encode(artistName, "UTF-8")}"
        def response = bot.toJson(bot.page.html)

        response.each { artist ->
            result.add(new Artist(
                    id: artist.id,
                    name: artist.name,
                    link: new URI("http://www.allmusic.com${artist.url}")
            ))
        }
        return result
    }
}
