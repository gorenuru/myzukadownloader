package org.nemesis.music.core.scraper.myzuka

import org.nemesis.music.core.annotation.ScraperService
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.core.scraper.DownloadLinkScraper
import org.nemesis.music.core.scraper.ScraperFactory
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.domain.ScraperType

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 1:03 PM
 */
@ScraperService(ScraperType.MYZUKA)
class MyzukaScraperFactory implements ScraperFactory {
    private final ArtistScraper artistScraper = new MyzukaArtistScraper()
    private final AlbumScraper albumScraper = new MyzukaAlbumScraper()
    private final TrackScraper trackScraper = new MyzukaTrackScraper()
    private final DownloadLinkScraper linkScraper = new MyzukaDownloadLinkScraper()

    @Override
    ArtistScraper getArtistScraper() {
        return artistScraper
    }

    @Override
    AlbumScraper getAlbumScraper() {
        return albumScraper
    }

    @Override
    TrackScraper getTrackScraper() {
        return trackScraper
    }

    @Override
    DownloadLinkScraper getDownloadLinkScraper() {
        return linkScraper
    }
}
