package org.nemesis.music.core.scraper.myzuka

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.Configuration
import org.nemesis.music.domain.Track
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.ScraperType
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: aalbul
 * Date: 11/19/12
 * Time: 9:49 AM
 *
 * Myzuka - specific class to scrap tracks
 */
class MyzukaTrackScraper implements TrackScraper {

    @Override
    public List<Track> getAlbumTracks(Album album) {
        List<Track> result = new ArrayList<Track>()
        def bot = new JerryBot()

        bot.navigate album.link.toASCIIString()
        bot.$$(".rectable_center:eq(0) tr:gt(0)").each { track ->
            if (track.$("td").size() > 2) {
                result.add(extractTrack(track, album))
            }
        }
        return result
    }

    /**
     * Extracts Track from specified DOM element
     * @param element - DOM element to parse
     * @param album - related Album instance
     * @return - extracted Track instance
     */
    private Track extractTrack(Jerry element, Album album) {
        def trackNo = Integer.parseInt(element.$("td:eq(1)").text()?.trim())
        def trackLink = element.$("td:eq(3) a")
        def trackName = trackLink.text()?.trim()
        def baseURI = Configuration.getBaseURI(ScraperType.MYZUKA)
        def id = PartsExtractor.instance.extractFromString(trackLink.attr('href'), 1, 2)
        def downloadPageLink = new URI("${baseURI.scheme}://${baseURI.rawAuthority}/${trackLink.attr('href')}")
        def duration = element.$("td:eq(4)").text()?.trim()
        def size = element.$("td:eq(5)").text()?.trim()
        def bitRate = element.$("td:eq(6)").text()?.trim()

        return new Track(
                id: id,
                trackNo: trackNo,
                link: downloadPageLink,
                title: trackName,
                duration: duration,
                size: size,
                bitRate: bitRate,
                album: album)
    }
}
