package org.nemesis.music.core.downloader.concurrent

import org.nemesis.music.core.listener.DownloadProgressListener
import org.nemesis.music.core.service.TrackService

import java.util.concurrent.BlockingQueue
import org.nemesis.music.core.event.DownloadEvent
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ExecutorService
import org.nemesis.music.core.event.DownloadEventPublisher
import org.nemesis.music.core.event.DownloadEventType
import org.nemesis.music.core.event.DownloadEventDispatcher
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Track
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * User: aalbul
 * Date: 11/30/12
 * Time: 11:06 AM
 *
 * This task is responsible for album downloading procedure.
 * Actually, it is making a bit more that just downloading the album :)
 * It is separating this task to more little sub-tasks and schedule their execution
 *
 * Also, this class is responsible for sending events to notify our system that album downloading is started and finished
 */
class AlbumDownloadTask implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(AlbumDownloadTask.class)
    private final Album album
    private final DownloadProgressListener listener
    private final BlockingQueue<DownloadEvent> eventQueue = new ArrayBlockingQueue<DownloadEvent>(100)
    private final ExecutorService downloadExecutor
    private final DownloadEventPublisher publisher = new DownloadEventPublisher(eventQueue)
    private final DownloadEventDispatcher eventDispatcher
    
    AlbumDownloadTask(Album album, DownloadProgressListener listener, ExecutorService downloadExecutor) {
        this.album = album
        this.listener = listener
        this.downloadExecutor = downloadExecutor
        this.eventDispatcher = new DownloadEventDispatcher(listener)
    }

    @Override
    void run() {
        logger.debug("Downloading album {}. Track count: {}. Searching for track links...", album.title, album.trackCount)
        List<Track> tracks = TrackService.instance.getAlbumTracks(album)
        tracks.each { track ->
            logger.debug("Scheduling track download task. Track: {}", track.title)
            downloadExecutor.submit(new TrackDownloadTask(track, tracks, publisher))
        }
        eventDispatcher.dispatch(new DownloadEvent(type: DownloadEventType.ALBUM_STARTED, album: album))
        int completedTasks = 0;
        while (completedTasks < tracks.size()) {
            DownloadEvent event = eventQueue.take()
            logger.debug("Received event: {}, track: {}", event.type, event.track.title)
            eventDispatcher.dispatch(event)

            if (event.type == DownloadEventType.TRACK_COMPLETED || event.type == DownloadEventType.TRACK_ERROR) {
                completedTasks++
                logger.debug("Event signalled that track is downloaded. Track: {} ({}/{})", event.track.title, completedTasks, tracks.size())
            }
        }
        eventDispatcher.dispatch(new DownloadEvent(type: DownloadEventType.ALBUM_COMPLETED, album: album))
    }
}
