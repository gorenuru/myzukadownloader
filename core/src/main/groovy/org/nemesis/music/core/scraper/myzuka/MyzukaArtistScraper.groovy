package org.nemesis.music.core.scraper.myzuka

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Artist
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: aalbul
 * Date: 11/19/12
 * Time: 9:08 AM
 *
 * Myzuka - specific Class to scrap artists
 */
class MyzukaArtistScraper implements ArtistScraper {

    @Override
    public List<Artist> getArtistsByKeyword(String artistName) {
        def bot = new JerryBot()
        bot.navigate "http://www.myzuka.ru/Search?searchText=${URLEncoder.encode(artistName, "UTF-8")}#artists"
        def result = new ArrayList<Artist>()
        bot.$$(".rectable:eq(0) tr:gt(0)").each {
            result.add(extractArtist(it));
        }
        return result
    }

    /**
     * Extracts Artist instance from specified element
     * @param element - DOM element to parse
     * @return extracted Artist instance
     */
    private Artist extractArtist(Jerry element) {
        def nameElement = element.$("td:eq(1) a")
        def name = nameElement.text()?.trim()
        def id = PartsExtractor.instance.extractFromString(nameElement.attr('href'), 1, 2)
        def link = new URI("http://www.myzuka.ru/${nameElement.attr('href')}")
        def trackCount = Integer.valueOf(element.$("td:eq(2)").text()?.trim())
        return new Artist(
                id: id,
                name: name,
                link: link,
                trackCount: trackCount)
    }
}
