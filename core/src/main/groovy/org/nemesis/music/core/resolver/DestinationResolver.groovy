package org.nemesis.music.core.resolver

import org.nemesis.music.core.Configuration
import org.nemesis.music.domain.Track
import org.nemesis.music.domain.Artist
import org.nemesis.music.domain.Album

/**
 * User: aalbul
 * Date: 11/19/12
 * Time: 10:11 AM
 *
 * Responsible for destination resolution process
 * Actually, this class prepares track destination:
 * 1) Create directory structure according to current configuration and folder/file name pattern
 * 2) Removes file if it is already exists and create new one
 */
class DestinationResolver {

    public File resolveAndInitializeFolder(Track track) {
        def trackPath = getTrackPath(track)
        def filePath = "${getArtistAndAlbumAbsolutePath(track.album.artist, track.album)}${File.separator}${trackPath}"
        def file = new File(filePath)

        //Initializing folders
        file.parentFile.mkdirs()
        file.delete()
        file.createNewFile()
        return file
    }

    /**
     * Prepare artist path
     * @param artist - artist instance
     * @return artist path string
     */
    private def getArtistPath(Artist artist) {
        return stripFileSystemName("${artist.name}")
    }

    /**
     * Prepare album path;
     * @param album - album instance
     * @return album path string
     */
    private def getAlbumPath(Album album) {
        return stripFileSystemName("(${album.releaseDate}) ${album.title}")
    }

    /**
     * Prepares track path
     * @param track - track instance
     * @return track path string
     */
    private def getTrackPath(Track track) {
        return stripFileSystemName("${track.trackNo}. ${track.title}.mp3")
    }

    /**
     * Returns artist and album absolute path (respecting current download directory)
     * For example.
     * Download dir = "D:\Music"
     * Artist name = "Amon Amarth"
     * Album = "With Odin on Our Side"
     *
     * We will get: D:\Music\Amon Amarth\With Odin On Our Side
     *
     * @param artist - artist instance
     * @param album - album instance
     * @return artist and album absolute path
     */
    private def getArtistAndAlbumAbsolutePath(Artist artist, Album album) {
        return "${Configuration.downloadDir}${File.separator}${getArtistPath(artist)}${File.separator}${getAlbumPath(album)}"
    }

    /**
     * Strips file system name to prevent invalid symbols
     * @param name - name to strip
     * @return stripped name
     */
    private def stripFileSystemName(String name) {
        String resultName = name
        resultName = resultName.replace('?', ' ')
        resultName = resultName.replace(':', ' ');
        resultName = resultName.replace('[', ' ');
        resultName = resultName.replace(']', ' ');
        resultName = resultName.replace('*', ' ');
        resultName = resultName.replace('\'', ' ');
        resultName = resultName.replace('"', ' ');
        resultName = resultName.replace('|', ' ');
        return resultName.trim()
    }
}
