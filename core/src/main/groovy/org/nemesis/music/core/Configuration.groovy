package org.nemesis.music.core

import org.nemesis.music.domain.ScraperType

/**
 * User: aalbul
 * Date: 11/30/12
 * Time: 9:34 AM
 */
class Configuration {
    private final static def config = new ConfigSlurper("defaults").parse(Configuration.class.getClassLoader().getResource("configuration.groovy"))

    public static URI getBaseURI(ScraperType type) {
        return new URI(config.baseURIs[type.name()])
    }

    public static def getDownloadDir() {
        return config.downloadFolder
    }

    public static Integer getDownloadThreadsCount() {
        return config.downloadThreadsCount
    }

    public static def getPlugins() {
        return config.plugins
    }

    public static Long getRetryTimes() {
        return config.retryTimes
    }

    public static Tuple getPreAuthenticationCredentials(ScraperType type) {
        return config.preAuthenticationCredentials[type.name()]
    }
}
