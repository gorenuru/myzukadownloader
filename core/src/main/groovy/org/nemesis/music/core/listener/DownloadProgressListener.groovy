package org.nemesis.music.core.listener

import org.nemesis.music.core.event.DownloadEvent

/**
 * User: aalbul
 * Date: 11/30/12
 * Time: 9:03 AM
 *
 * Download progress listener interface
 * Mainly used to update UI state
 */
public interface DownloadProgressListener {

    /**
     * Indicates that download of the track is started
     * @param - download event instance
     */
    public void trackDownloadStarted(DownloadEvent event)

    /**
     * Indicates that track is downloaded
     * @param downloadedTrack - downloaded track instance
     */
    public void trackDownloaded(DownloadEvent event)

    /**
     * Indicates that error occurred during track download process
     * @param track - track instance
     */
    public void trackDownloadError(DownloadEvent event)

    /**
     * Indicates that album download process is started
     * @param tracks - list of album tracks
     */
    public void albumDownloadStarted(DownloadEvent event)


    /**
     * Indicates that album download process is finished
     * @param tracks - list of album tracks
     */
    public void albumDownloadCompleted(DownloadEvent event)
}