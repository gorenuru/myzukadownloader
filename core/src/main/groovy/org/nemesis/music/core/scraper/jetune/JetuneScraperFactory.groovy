package org.nemesis.music.core.scraper.jetune

import org.nemesis.music.core.annotation.ScraperService
import org.nemesis.music.core.authentication.jetune.JetunePreAuthenticator
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.core.scraper.DownloadLinkScraper
import org.nemesis.music.core.scraper.ScraperFactory
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.domain.ScraperType

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 1:51 PM
 */
@ScraperService(ScraperType.JETUNE)
class JetuneScraperFactory implements ScraperFactory {
    private final ArtistScraper artistScraper = new JetuneArtistScraper()
    private final AlbumScraper albumScraper = new JetuneAlbumScraper()
    private final TrackScraper trackScraper = new JetuneTrackScraper()
    private DownloadLinkScraper linkScraper = new JetuneDownloadLinkScraper()

    JetuneScraperFactory() {
        new JetunePreAuthenticator().preAuthenticate()
    }

    @Override
    ArtistScraper getArtistScraper() {
        return artistScraper
    }

    @Override
    AlbumScraper getAlbumScraper() {
        return albumScraper
    }

    @Override
    TrackScraper getTrackScraper() {
        return trackScraper
    }

    @Override
    DownloadLinkScraper getDownloadLinkScraper() {
        return linkScraper
    }
}
