package org.nemesis.music.core.scraper.myzuka

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Track
import org.nemesis.music.core.scraper.DownloadLinkScraper

/**
 * User: aalbul
 * Date: 12/21/12
 * Time: 10:39 AM
 *
 * Myzuka - specific implementation
 */
class MyzukaDownloadLinkScraper implements DownloadLinkScraper {

    @Override
    URI getDownloadLinkForTrack(Track track) {
        def bot = new JerryBot()
        bot.navigate track.link.toASCIIString()
        URI result = null
        bot.$$(".rectable a").each {
            if (it.attr("href").contains("/Song/Download/")) {
                result =  new URI("http://myzuka.ru${it.attr("href")}")
            }
        }
        return result
    }
}
