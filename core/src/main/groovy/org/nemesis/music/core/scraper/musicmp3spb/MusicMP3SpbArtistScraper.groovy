package org.nemesis.music.core.scraper.musicmp3spb

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.domain.Artist
import org.nemesis.music.core.util.PartsExtractor;

/**
 * User: nuru
 * Date: 22.12.12
 * Time: 19:01
 */
class MusicMP3SpbArtistScraper implements ArtistScraper {

    @Override
    List<Artist> getArtistsByKeyword(String artistName) {
        def result = new ArrayList<Artist>()
        def bot = new JerryBot()
        bot.executeRequest {
                url = "http://musicmp3spb.org/search/"
                params = [
                        Content : artistName,
                        category : '1'
                ]
        }
        bot.$$("#cntCenter div > a").each {
            result.add(extractArtist(it))
        }
        return result
    }

    /**
     * Extracts Artist instance from specified element
     * @param element - DOM element to parse
     * @return extracted Artist instance
     */
    private Artist extractArtist(Jerry element) {
        def name = element.text()?.trim()
        def rawLink = element.attr("href")
        def id = PartsExtractor.instance.extractFromString(rawLink.replace(".html", ""), 1)
        def link = new URI("http://musicmp3spb.org/${rawLink}")
        return new Artist(
                id: id,
                name: name,
                link: link)
    }
}

