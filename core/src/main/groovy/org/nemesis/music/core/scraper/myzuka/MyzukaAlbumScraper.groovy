package org.nemesis.music.core.scraper.myzuka

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.Configuration
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.AlbumType
import org.nemesis.music.domain.Artist
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.domain.ScraperType
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: aalbul
 * Date: 11/19/12
 * Time: 9:14 AM
 *
 * Myzuka - specific Class to scrap albums
 */
class MyzukaAlbumScraper implements AlbumScraper {

    @Override
    public List<Album> getListOfAlbums(Artist artist) {
        def bot = new JerryBot()
        List<Album> result = new ArrayList<Album>()

        bot.navigate artist.link.toASCIIString()
        def albums = bot.$$(".rectable tr:gt(0)")
        albums.eachWithIndex { album, index ->
            if (index < albums.size() - 1 && album.$("td").size() > 3) {
                result.add(extractAlbum(album, artist))
            }
        }
        return result
    }

    /**
     * Extracts Album instance from specified DOM element
     * @param albumElement - DOM element to parse
     * @param artist - related Artist instance
     * @return extracted Album instance
     */
    private Album extractAlbum(Jerry albumElement, Artist artist) {
        def albumLink = albumElement.$("td:eq(1) a")
        def title = albumLink.text()?.trim()
        def baseURI = Configuration.getBaseURI(ScraperType.MYZUKA)
        def id = PartsExtractor.instance.extractFromString(albumLink.attr('href'), 1, 2)
        def link = new URI("${baseURI.scheme}://${baseURI.rawAuthority}/${albumLink.attr('href')}")
        def releaseDate = albumElement.$("td:eq(2)").text()?.trim()
        def trackCount = Integer.valueOf(albumElement.$("td:eq(3)").text()?.trim())
        def rating = albumElement.$("td:eq(4)").text()?.trim()
        return new Album(
                id: id,
                title: title,
                releaseDate: releaseDate,
                trackCount: trackCount,
                rating: rating,
                link: link,
                artist: artist,
                type: determineAlbumType(albumElement))
    }

    /**
     * Determines the album type for specified album
     * @param albumElement - album DOM note instance
     * @return AlbumType instance
     */
    private AlbumType determineAlbumType(Jerry albumElement) {
        def albumType = albumElement.parent().prev().text()?.trim()
        AlbumType result;

        switch (albumType) {
            case "Студийные альбомы" :
                result = AlbumType.STUDIO
                break
            case "EP" :
                result = AlbumType.EP
                break
            case "Синглы" :
                result = AlbumType.SINGLE
                break
            case "Сборники исполнителя" :
                result = AlbumType.COMPILATION_OF_ARTIST
                break
            case "Демо" :
                result = AlbumType.DEMO
                break
            case "Саундтреки" :
                result = AlbumType.SOUNDTRACK
                break
            case "Live выступления" :
                result = AlbumType.LIVE
                break
            case "Микстейпы" :
                result = AlbumType.MIXTAPE
                break
            case "DJ Миксы" :
                result = AlbumType.DJ_MIX
                break
            case "Бутлеги" :
                result = AlbumType.BOOTLEG
                break
            case "Сборники разных исполнителей" :
                result = AlbumType.COMPILATION_OF_DIFFERENT_ARTISTS
                break
            case "Неофициальные сборники" :
                result = AlbumType.UNOFFICIAL_COMPILATIONS
                break
            default:
                result = AlbumType.OTHER
        }
        return result
    }
}
