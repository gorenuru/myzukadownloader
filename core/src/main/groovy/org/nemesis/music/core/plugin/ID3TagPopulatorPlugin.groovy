package org.nemesis.music.core.plugin

import org.nemesis.music.core.event.DownloadEvent
import org.jaudiotagger.audio.AudioFile
import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.Tag
import org.jaudiotagger.tag.FieldKey
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.apache.commons.lang3.StringUtils.*;

/**
 * User: nuru
 * Date: 01.12.12
 * Time: 23:32
 */
class ID3TagPopulatorPlugin implements Plugin {
    private final static Logger logger = LoggerFactory.getLogger(ID3TagPopulatorPlugin.class)

    @Override
    void execute(DownloadEvent event) {
        logger.debug("Populating tags for file: {}", event.downloadedFile.canonicalPath)
        AudioFile file = AudioFileIO.read(event.downloadedFile);
        Tag tag = file.getTag();
        tag.setField(FieldKey.ARTIST, capitalize(event.album.artist.name));
        tag.setField(FieldKey.ALBUM, capitalize(event.album.title))
        tag.setField(FieldKey.TITLE, capitalize(event.track.title))
        tag.setField(FieldKey.TRACK,  event.track.trackNo?.toString())
        tag.setField(FieldKey.YEAR, event.album.releaseDate)
        tag.setField(FieldKey.COMMENT, "Ripped by MyzukaDownloader")
        file.commit();
    }

    /**
     * Capitalize each word in the string
     * For example, if we have artist name like:
     *
     * long distance calling
     *
     * we will receive:
     *
     * Long Distance Calling
     *
     * @param sourceString - source string to capitalize
     * @return capitalized string
     */
    private String capitalize(String sourceString) {
        StringBuilder resultBuilder = new StringBuilder()
        trimToEmpty(sourceString).split(" ").each {
            resultBuilder.append(capitalize(it)).append(" ")
        }
        return trimToEmpty(resultBuilder.toString())
    }

}
