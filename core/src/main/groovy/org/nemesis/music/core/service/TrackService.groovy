package org.nemesis.music.core.service

import org.nemesis.music.core.scraper.ScraperResolver
import org.nemesis.music.core.util.ScraperUtil
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Track

/**
 * User: aalbul
 * Date: 12/25/12
 * Time: 3:54 PM
 *
 * The service to work with Track entities
 */
class TrackService {
    private final static TrackService INSTANCE = new TrackService()

    public static TrackService getInstance() {
        return INSTANCE
    }

    /**
     * Retrieves list of tracks for specified album
     * @param album - album instance
     * @return list of found tracks
     */
    public List<Track> getAlbumTracks(Album album) {
        List<Track> tracks = ScraperResolver.instance.getTrackScraper(album.scraperType).getAlbumTracks(album)
        ScraperUtil.populateScraperType(tracks, album.scraperType)
        return tracks
    }
}
