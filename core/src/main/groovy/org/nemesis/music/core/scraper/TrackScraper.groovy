package org.nemesis.music.core.scraper

import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Track

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 1:01 PM
 *
 * The interface to Track scraper
 */
public interface TrackScraper {

    /**
     * Search and return list of tracks for specified album
     * @param album - album instance
     * @return list of found tracks
     */
    public List<Track> getAlbumTracks(Album album)
}