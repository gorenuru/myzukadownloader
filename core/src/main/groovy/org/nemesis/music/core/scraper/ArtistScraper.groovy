package org.nemesis.music.core.scraper

import org.nemesis.music.domain.Artist

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 12:57 PM
 *
 * The interface for Artist scraper
 */
interface ArtistScraper {

    /**
     * Search for artists by specified keyword
     * @param baseURI - base URI instance
     * @param artistName - artist name
     * @return list of found artists
     */
    public List<Artist> getArtistsByKeyword(String artistName)
}
