package org.nemesis.music.core.util

import org.nemesis.music.domain.ScrapableObject
import org.nemesis.music.domain.ScraperType

/**
 * User: nuru
 * Date: 26.12.12
 * Time: 21:03
 *
 * The utility class for scraper - related operations
 */
class ScraperUtil {

    /**
     * Utility method to populate the scraper type on all the specified objects
     * @param objects - list of objects assignable from ScrapableObject
     * @param type - scraper type
     */
    public static void populateScraperType(Collection<? extends ScrapableObject> objects, ScraperType type) {
        objects.each {
            it.scraperType = type
        }
    }
}
