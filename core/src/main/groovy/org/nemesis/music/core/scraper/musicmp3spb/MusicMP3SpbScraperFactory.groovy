package org.nemesis.music.core.scraper.musicmp3spb

import org.nemesis.music.core.annotation.ScraperService
import org.nemesis.music.core.scraper.ScraperFactory
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.core.scraper.DownloadLinkScraper
import org.nemesis.music.domain.ScraperType

/**
 * User: nuru
 * Date: 22.12.12
 * Time: 18:57
 */
@ScraperService(ScraperType.MUSICMP3SPB)
class MusicMP3SpbScraperFactory implements ScraperFactory {
    private final MusicMP3SpbArtistScraper artistScraper = new MusicMP3SpbArtistScraper()
    private final MusicMP3SpbAlbumScraper albumScraper = new MusicMP3SpbAlbumScraper()
    private final MusicMP3SpbTrackScraper trackScraper = new MusicMP3SpbTrackScraper()
    private final MusicMP3SpbDownloadLinkScraper linkScraper = new MusicMP3SpbDownloadLinkScraper()

    @Override
    ArtistScraper getArtistScraper() {
        return artistScraper
    }

    @Override
    AlbumScraper getAlbumScraper() {
        return albumScraper
    }

    @Override
    TrackScraper getTrackScraper() {
        return trackScraper
    }

    @Override
    DownloadLinkScraper getDownloadLinkScraper() {
        return linkScraper
    }
}
