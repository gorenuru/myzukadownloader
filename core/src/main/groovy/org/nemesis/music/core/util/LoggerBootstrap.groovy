package org.nemesis.music.core.util

import java.util.logging.LogManager
import org.slf4j.bridge.SLF4JBridgeHandler

/**
 * User: nuru
 * Date: 02.12.12
 * Time: 18:41
 *
 * Bootstrap class to tunnel JUL to SLF4J
 * You should call this class at the first line of your code to get rid from the annoying JUL messages
 */
class LoggerBootstrap {

    public static void init() {
        LogManager.logManager.reset()
        SLF4JBridgeHandler.install()
    }
}
