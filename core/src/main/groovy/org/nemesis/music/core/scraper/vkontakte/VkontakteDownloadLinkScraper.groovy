package org.nemesis.music.core.scraper.vkontakte

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Track
import org.nemesis.music.core.scraper.DownloadLinkScraper

/**
 * User: nuru
 * Date: 23.12.12
 * Time: 17:40
 */
class VkontakteDownloadLinkScraper implements DownloadLinkScraper {
    private String authToken

    VkontakteDownloadLinkScraper(String authToken) {
        this.authToken = authToken
    }

    @Override
    URI getDownloadLinkForTrack(Track track) {
        def bot = new JerryBot()
        def result = bot.toJson bot.executeRequest {
                url = "https://api.vk.com/method/audio.search"
                params = [
                        q : "${track.album.artist.name} ${track.title}",
                        sort : '2',
                        access_token : authToken
                ]
        }
        return filter(track, result)
    }

    private URI filter(Track track, def rawMap) {
        List tracks = rawMap.response
        if (tracks.size() > 0) {
            tracks.remove(0)
        }
        def found = tracks.find {
            return it.artist.equalsIgnoreCase(track.album.artist.name) && it.title.equalsIgnoreCase(track.title)
        }
        if (found) {
            return new URI(found.url)
        } else {
            return null
        }
    }
}
