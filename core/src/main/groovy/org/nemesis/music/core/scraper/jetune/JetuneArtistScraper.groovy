package org.nemesis.music.core.scraper.jetune

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Artist
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.core.util.PartsExtractor

import static org.nemesis.http.domain.RequestMethod.*;

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 1:21 PM
 */
class JetuneArtistScraper implements ArtistScraper {

    @Override
    List<Artist> getArtistsByKeyword(String artistName) {
        List<Artist> result = new ArrayList<>()
        def bot = new JerryBot()
        bot.executeRequest {
                url = "http://www.jetune.ru/searchnow"
                form = [
                        ms_search_text : artistName
                ]
                method = POST
        }

        bot.$$(".ap_bgcat b a").eq(0).parent().parent().parent().parent().$("tr:gt(0)").each {
            result.add(extractArtist(it))
        }
        return result
    }

    /**
     * Extracts artist instance from current artist HTML DOM node
     * @param artistElement - HTML DOM node
     * @return composed Artist instance
     */
    private Artist extractArtist(Jerry artistElement) {
        def linkElement = artistElement.$(".ap_bgcat:eq(0) a")
        def name = linkElement.text()?.trim()
        def id = PartsExtractor.instance.extractFromString(linkElement.attr("href"), 1, 2)
        def link = new URI("http://www.jetune.ru${linkElement.attr("href")}")
        def trackCountRaw = artistElement.$(".ap_bgcat:eq(2)").text()?.trim()
        def trackCount = Integer.valueOf(trackCountRaw.substring(trackCountRaw.indexOf("/") + 1))
        return new Artist(
                id: id,
                name: name,
                link: link,
                trackCount: trackCount
        )
    }
}
