package org.nemesis.music.core.event

import java.util.concurrent.BlockingQueue

/**
 * User: nuru
 * Date: 30.11.12
 * Time: 23:09
 *
 * Interface to indirectly publish events to the queue
 * Actually, we need it to abstrogate user from the queue
 */
class DownloadEventPublisher {
    private BlockingQueue<DownloadEvent> eventQueue

    DownloadEventPublisher(BlockingQueue<DownloadEvent> eventQueue) {
        this.eventQueue = eventQueue
    }

    /**
     * Publish event to the event queue
     * @param event - event to publish
     * @param albumTracks - album track list
     */
    public void publishEvent(DownloadEvent event) {
        eventQueue.put(event)
    }
}
