package org.nemesis.music.core.authentication.jetune

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.Configuration
import org.nemesis.music.core.authentication.PreAuthenticator
import org.nemesis.music.domain.ScraperType
import static org.nemesis.http.domain.RequestMethod.*

/**
 * User: aalbul
 * Date: 12/21/12
 * Time: 9:13 AM
 *
 * The pre authenticator for Jetune service
 */
class JetunePreAuthenticator implements PreAuthenticator {

    @Override
    String preAuthenticate() {
        Tuple credentials = Configuration.getPreAuthenticationCredentials(ScraperType.JETUNE)
        if (credentials) {
            def bot = new JerryBot()
            bot.executeRequest {
                    url = "http://www.jetune.ru/userlogin"
                    form = [
                            redirect_by_ref: '1',
                            do_login: '1',
                            logintext: credentials.get(0).toString(),
                            logintext2: credentials.get(1).toString(),
                            remember_pass: '1'
                    ]
                    method = POST
            }
        }
        return null
    }
}
