package org.nemesis.music.core.scraper

import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Artist

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 12:59 PM
 *
 * The interface for Album scraper
 */
interface AlbumScraper {

    /**
     * Search and return list of albums for specified artist
     * @param artist - artist instance
     * @return list of related albums
     */
    public List<Album> getListOfAlbums(Artist artist)
}
