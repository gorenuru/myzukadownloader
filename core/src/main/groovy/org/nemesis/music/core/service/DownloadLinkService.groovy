package org.nemesis.music.core.service

import org.nemesis.music.core.scraper.ScraperResolver
import org.nemesis.music.domain.Track

/**
 * User: aalbul
 * Date: 12/25/12
 * Time: 3:57 PM
 *
 * Service to accure download links for the tracks
 */
class DownloadLinkService {
    private final static DownloadLinkService INSTANCE = new DownloadLinkService()

    private DownloadLinkService() {
    }

    public static DownloadLinkService getInstance() {
        return INSTANCE
    }

    /**
     * Retrieves download link for specified tracks
     * @param type - scraper type
     * @return download URI
     */
    public URI getDownloadLinkForTrack(Track track) {
        return ScraperResolver.instance.getDownloadLinkScraper(track.scraperType).getDownloadLinkForTrack(track)
    }
}
