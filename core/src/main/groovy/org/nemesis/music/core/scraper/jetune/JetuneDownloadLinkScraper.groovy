package org.nemesis.music.core.scraper.jetune

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Track
import org.nemesis.music.core.scraper.DownloadLinkScraper

/**
 * User: aalbul
 * Date: 12/21/12
 * Time: 10:46 AM
 *
 * Jetune - specific Download Link Scraper
 */
class JetuneDownloadLinkScraper implements DownloadLinkScraper {

    @Override
    URI getDownloadLinkForTrack(Track track) {
        def bot = new JerryBot()
        bot.navigate track.link.toASCIIString()
        return new URI(bot.$("td[colspan='2'] a strong").parent().attr("href"))
    }
}
