package org.nemesis.music.core.scraper

import org.nemesis.music.core.annotation.ScraperService
import org.nemesis.music.domain.ScraperType
import org.reflections.Reflections

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 1:08 PM
 */
class ScraperResolver {
    private final static ScraperResolver INSTANCE = new ScraperResolver()
    private Map<ScraperType, ScraperFactory> scraperFactories = new HashMap<>()
    private Reflections reflections = new Reflections("org.nemesis.music.core.scraper")

    public static ScraperResolver getInstance() {
        return INSTANCE
    }

    public ArtistScraper getArtistScraper(ScraperType type) {
        return getFactory(type).artistScraper
    }

    public AlbumScraper getAlbumScraper(ScraperType type) {
        return getFactory(type).albumScraper
    }

    public TrackScraper getTrackScraper(ScraperType type) {
        return getFactory(type).trackScraper
    }

    public DownloadLinkScraper getDownloadLinkScraper(ScraperType type) {
        return getFactory(type).downloadLinkScraper
    }

    /**
     * Retrieves ScraperFactory by ScraperType
     *
     * This method will search for all the ScraperFactory classes which are declare the ScraperService annotation
     * Also, caching is used here so reflection resolution will be used only once per factory
     *
     * @param type - scraper type
     * @return needed factory or null if no ScraperFactory for such a type declared
     */
    private ScraperFactory getFactory(ScraperType type) {
        if (!scraperFactories.containsKey(type)) {
            def factories = reflections.getTypesAnnotatedWith(ScraperService.class)
            factories.each {
                ScraperService scraperService = it.getAnnotation(ScraperService.class)
                if (scraperService.value() == type) {
                    scraperFactories.put(type, it.newInstance() as ScraperFactory)
                }
            }
        }
        return scraperFactories.get(type)
    }
}
