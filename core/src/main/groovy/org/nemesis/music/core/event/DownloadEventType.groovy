package org.nemesis.music.core.event

/**
 * User: nuru
 * Date: 30.11.12
 * Time: 22:58
 *
 * Enumeration of download events
 */
enum DownloadEventType {
    TRACK_STARTED,
    TRACK_ERROR,
    TRACK_COMPLETED,
    ALBUM_STARTED,
    ALBUM_COMPLETED
}
