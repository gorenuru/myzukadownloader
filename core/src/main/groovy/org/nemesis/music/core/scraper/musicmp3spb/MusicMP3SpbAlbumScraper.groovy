package org.nemesis.music.core.scraper.musicmp3spb

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Artist

import org.nemesis.music.core.Configuration
import org.nemesis.music.domain.ScraperType
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: nuru
 * Date: 22.12.12
 * Time: 20:28
 */
class MusicMP3SpbAlbumScraper implements AlbumScraper {

    @Override
    List<Album> getListOfAlbums(Artist artist) {
        List<Album> result = new ArrayList<Album>()
        def bot = new JerryBot()
        bot.navigate artist.link.toASCIIString()
        bot.$$(".cntAlbumList > div").each {
            result.add(extractAlbum(it, artist))
        }
        return result
    }

    /**
     * Extracts Album instance from specified DOM element
     * @param albumElement - DOM element to parse
     * @param artist - related Artist instance
     * @return extracted Album instance
     */
    private Album extractAlbum(Jerry albumElement, Artist artist) {
        def albumLink = albumElement.$("a > b").parent()
        def releaseDate = null
        if (albumLink.text().indexOf(" ") != -1) {
            releaseDate = albumLink.text().substring(0, albumLink.text().indexOf(" "))
        }
        def title = albumElement.$("b").text()
        def baseURI = Configuration.getBaseURI(ScraperType.MUSICMP3SPB)
        def rawLink = albumLink.attr("href")
        def id = PartsExtractor.instance.extractFromString(rawLink.replaceAll(".html", ""), 1)
        id = "${artist.id}#${id}"
        def link = new URI("${baseURI.scheme}://${baseURI.rawAuthority}/${rawLink}")
        return new Album(
                id: id,
                title: title,
                releaseDate: releaseDate,
                link: link,
                artist: artist)
    }
}
