package org.nemesis.music.core.downloader.concurrent

import org.nemesis.music.core.downloader.TrackDownloader

import org.nemesis.music.core.event.DownloadEventType
import org.nemesis.music.core.event.DownloadEventPublisher
import org.nemesis.music.core.event.DownloadEvent
import org.nemesis.music.domain.Track
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * User: aalbul
 * Date: 11/30/12
 * Time: 9:01 AM
 *
 * Task do download specific
 */
class TrackDownloadTask implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(TrackDownloadTask.class)
    private Track track
    private List<Track> albumTracks
    private DownloadEventPublisher publisher

    TrackDownloadTask(Track track, List<Track> albumTracks, DownloadEventPublisher publisher) {
        this.track = track
        this.albumTracks = albumTracks
        this.publisher = publisher
    }

    @Override
    public void run() {
        publisher.publishEvent(new DownloadEvent(type: DownloadEventType.TRACK_STARTED, track: track, albumTracks: albumTracks))
        try {
            File downloadedFile = TrackDownloader.instance.download(track)
            publisher.publishEvent(new DownloadEvent(type: DownloadEventType.TRACK_COMPLETED, track: track, albumTracks: albumTracks,
                    album: track.album, downloadedFile: downloadedFile))
        } catch (Exception e) {
            logger.error("Error while downloading track: {}", track.title, e)
            publisher.publishEvent(new DownloadEvent(type: DownloadEventType.TRACK_ERROR, track: track, albumTracks: albumTracks,
            exception: e))
        }
    }

}
