package org.nemesis.music.core.authentication.vkontakte

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.Configuration
import org.nemesis.music.core.authentication.PreAuthenticator
import org.nemesis.music.domain.ScraperType
import static org.nemesis.bot.util.ExtractionUtil.*

/**
 * User: nuru
 * Date: 23.12.12
 * Time: 17:11
 */
class VkontaktePreAuthenticator implements PreAuthenticator {
    private final static ACCESS_TOKEN_STRING = "access_token"

    @Override
    String preAuthenticate() {
        Tuple credentials = Configuration.getPreAuthenticationCredentials(ScraperType.VKONTAKTE)
        def bot = new JerryBot()
        bot.navigate "http://vk.com/login.php"
        bot.submit "#login", [
                email: (String) credentials.get(0),
                pass: (String) credentials.get(1)
        ]
        bot.executeRequest {
                url = "http://oauth.vk.com/authorize"
                params = [
                        client_id : '3283353',
                        scope : 'AUDIO',
                        redirect_uri : "http://oauth.vk.com/blank.html",
                        display : 'page',
                        response_type : 'token'
                ]
        }

        URI authorizationUri = bot.page.uri
        if (!bot.page.url.contains(ACCESS_TOKEN_STRING)) {
            def responseURIRaw = cut(bot.page.html, "approve() {", "}")
            authorizationUri = new URI(cut(responseURIRaw, "location.href = \"", "\""))
            bot.navigate authorizationUri.toASCIIString()
        }
        return cut(authorizationUri.toString(), ACCESS_TOKEN_STRING + "=", "&")
    }
}
