package org.nemesis.music.core.service

import org.nemesis.music.core.scraper.ScraperResolver
import org.nemesis.music.core.util.ScraperUtil
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.AlbumType
import org.nemesis.music.domain.Artist

/**
 * User: aalbul
 * Date: 12/25/12
 * Time: 3:35 PM
 *
 * The service to work with Album entities
 */
class AlbumService {
    private final static AlbumService INSTANCE = new AlbumService()

    private AlbumService() {
    }

    public static AlbumService getInstance() {
        return INSTANCE
    }

    /**
     * Retrieves album entities by specified criteria
     * @param artist - artist instance
     * @return list of albums
     */
    public List<Album> getListOfAlbums(Artist artist) {
        List<Album> albums = ScraperResolver.instance.getAlbumScraper(artist.scraperType).getListOfAlbums(artist)
        ScraperUtil.populateScraperType(albums, artist.scraperType)
        return albums
    }

    /**
     * Retrieves and filters the list of albums by the specified criteria
     * @param artist - artist instance
     * @param albumTypes - list of required album types
     * @return filtered list of albums
     */
    public List<Album> getListOfAlbumsFiltered(Artist artist, Collection<AlbumType> albumTypes) {
        List<Album> albums = getListOfAlbums(artist)
        return albums.findAll {
            return albumTypes.contains(it.type)
        }
    }
}
