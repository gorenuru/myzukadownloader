package org.nemesis.music.core.info

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.AlbumInfo
import org.nemesis.music.domain.ArtistInfo
import org.nemesis.music.domain.Artist

import static org.apache.commons.lang3.StringUtils.*

/**
 * User: aalbul
 * Date: 4/23/13
 * Time: 10:34 AM
 *
 * Responsible for retrieving information about the artist and album
 */
class InformationScraper {
    private final static InformationScraper INSTANCE = new InformationScraper()

    static InformationScraper getInstance() {
        return INSTANCE
    }

    /**
     * Retrieves artist information for specified artist name
     * @param artist - artist name to search for
     * @return artist information instance or null if artist not found
     */
    public ArtistInfo getArtistInfo(Artist artist) {
        def bot = new JerryBot()
        return bot.with {
            ArtistInfo info = null
            if (findArtist(artist, bot)) {
                info = extractArtistInfo(bot)
            }
            info
        }
    }

    /**
     * Extracts artist information from current page
     * @param bot - bot instance
     * @return populated ArtistInfo instance
     */
    private ArtistInfo extractArtistInfo(JerryBot bot) {
        ArtistInfo info = new ArtistInfo()
        bot.with {
            info.photo = new URI($(".artist-image img").attr("src"))
            info.name = $(".artist-name").text()?.trim()
            $$(".details .genres a").each { genre ->
                info.genres << genre.text()?.trim()
            }
            $$(".details .styles a").each { style ->
                info.styles << style.text()?.trim()
            }
            info.active = $(".details .active").text()?.trim()
            info.formed = normalizeSpace($(".details .birth span").text()?.trim())
            $$(".details .group-members a").each { member ->
                info.groupMembers << member.text()?.trim()
            }
            info.biography = $(".editorial-text").text()?.trim()
        }
        return info
    }

    /**
     * Retrieve album information by album's title
     * @param album - album instance
     * @return AlbumInfo instance
     */
    public AlbumInfo getAlbumInfo(Album album) {
        def bot = new JerryBot()
        return bot.with {
            AlbumInfo info = null

            if (findArtist(album.artist, bot)) {
                def found = $$(".primary_link .full-title").find { it.text()?.trim()?.equalsIgnoreCase(album.title) } as Jerry
                if (found && found.size() != 0) {
                    navigate found.attr("href")
                    info = extractAlbumInfo(bot)
                }
            }
            info
        }
    }

    /**
     * Extracts AlbumInfo from current page
     * @param bot - bot instance
     * @return filled AlbumInfo instance
     */
    private AlbumInfo extractAlbumInfo(JerryBot bot) {
        AlbumInfo info = new AlbumInfo()
        bot.with {
            info.cover = new URI($(".image-container img").attr("src"))
            info.name = $(".album-title").text()?.trim()
            info.editorRating = new BigDecimal($(".editorial-rating .rating").attr("data-stars").replace(",", "."))
            info.releaseDate = $(".release-date").text()?.trim()
            info.duration = $(".duration").text()?.trim()
            $$(".details .genres a").each { genre ->
                info.genres << genre.text()?.trim()
            }
            $$(".details .styles a").each { style ->
                info.styles << style.text()?.trim()
            }
            info.description = $(".editorial-text").text()?.trim()
        }
        return info
    }

    /**
     * Trying to find artist by it's name
     * This method will shift to workflow to artist's page
     * @param artist - artist instance
     * @param bot - bot instance
     * @return true if artist where found
     */
    private boolean findArtist(Artist artist, JerryBot bot) {
        return bot.with {
            navigate "http://www.allmusic.com/search/artists/${URLEncoder.encode(artist.name?.trim(), "UTF-8")}"
            def found = $$(".artist .name a").find { it.text()?.trim()?.equalsIgnoreCase(artist.name?.trim()) } as Jerry
            if (found && found.size() != 0) {
                navigate found.attr("href")
                true
            } else {
                false
            }
        }
    }
}
