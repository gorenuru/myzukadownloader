package org.nemesis.music.core.scraper

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 12:56 PM
 *
 * The factory class to get the specific scrapers
 */
public interface ScraperFactory {
    public ArtistScraper getArtistScraper()
    public AlbumScraper getAlbumScraper()
    public TrackScraper getTrackScraper()
    public DownloadLinkScraper getDownloadLinkScraper()
}