package org.nemesis.music.core.annotation

import org.nemesis.music.domain.ScraperType

import java.lang.annotation.Inherited
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * User: nuru
 * Date: 26.12.12
 * Time: 21:20
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ScraperService {
    ScraperType value()
}