package org.nemesis.music.core.authentication

/**
 * User: aalbul
 * Date: 12/21/12
 * Time: 9:07 AM
 *
 * This interface is used to pre-authenticate scraper providers before the actual work
 * It will be called on the application start-up
 */
interface PreAuthenticator {

    /**
     * Performs the authentication steps for current provider
     * @param template - http template instance
     * @return - authentication token when relevant
     */
    public String preAuthenticate()
}
