package org.nemesis.music.core.scraper.musicmp3spb

import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Track
import org.nemesis.music.core.scraper.DownloadLinkScraper

/**
 * User: nuru
 * Date: 23.12.12
 * Time: 11:14
 */
class MusicMP3SpbDownloadLinkScraper implements DownloadLinkScraper {

    @Override
    URI getDownloadLinkForTrack(Track track) {
        def bot = new JerryBot()
        bot.navigate track.link.toASCIIString()
        bot.submit "#cntMainCenter form"
        return new URI(bot.$("#cntMainCenterText a").attr("href"))
    }
}
