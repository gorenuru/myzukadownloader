package org.nemesis.music.core.listener

import org.nemesis.music.core.event.DownloadEvent

/**
 * User: nuru
 * Date: 30.11.12
 * Time: 23:27
 *
 * Adapter class to not implement all the listener methods
 */
class DownloadProgressAdapter implements DownloadProgressListener {

    @Override
    void trackDownloadStarted(DownloadEvent event) {

    }

    @Override
    void trackDownloaded(DownloadEvent event) {

    }

    @Override
    void trackDownloadError(DownloadEvent event) {

    }

    @Override
    void albumDownloadStarted(DownloadEvent event) {

    }

    @Override
    void albumDownloadCompleted(DownloadEvent event) {

    }
}
