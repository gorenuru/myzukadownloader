package org.nemesis.music.core.downloader

import org.nemesis.music.core.listener.DownloadProgressListener

import java.util.concurrent.Executors
import java.util.concurrent.Executor
import org.nemesis.music.core.downloader.concurrent.AlbumDownloadTask
import org.nemesis.music.core.Configuration
import java.util.concurrent.ExecutorService
import org.nemesis.music.domain.Album

/**
 * User: aalbul
 * Date: 11/30/12
 * Time: 9:02 AM
 *
 * Utility class to download albums
 * It is responsible for all the scheduling capabilities
 *
 * The number of downloader threads can be configured via downloadThreadsCount in configuration file
 */
class AlbumDownloader {
    private final static AlbumDownloader INSTANCE = new AlbumDownloader()

    private ExecutorService downloaderPool = Executors.newFixedThreadPool(Configuration.downloadThreadsCount)
    private Executor monitorThreadExecutor = Executors.newFixedThreadPool(1)

    private AlbumDownloader() {
    }

    public static AlbumDownloader getInstance() {
        return INSTANCE;
    }

    /**
     * Begins actual download progress.
     * This method will create the separate thread which will schedule tracks download progress.
     * So, no need to create threads from UI
     * @param album - album to download
     * @param listener - progress listener instance
     */
    public void downloadAlbum(Album album, DownloadProgressListener listener) {
        monitorThreadExecutor.execute(new AlbumDownloadTask(album, listener, downloaderPool))
    }

}
