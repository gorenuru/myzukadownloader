package org.nemesis.music.core.scraper.vkontakte

import org.nemesis.music.core.annotation.ScraperService
import org.nemesis.music.core.authentication.vkontakte.VkontaktePreAuthenticator
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.core.scraper.ArtistScraper
import org.nemesis.music.core.scraper.DownloadLinkScraper
import org.nemesis.music.core.scraper.ScraperFactory
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.core.scraper.allmusic.AllmusicAlbumScraper
import org.nemesis.music.core.scraper.allmusic.AllmusicArtistScraper
import org.nemesis.music.core.scraper.allmusic.AllmusicTrackScraper
import org.nemesis.music.domain.ScraperType

/**
 * User: nuru
 * Date: 23.12.12
 * Time: 17:53
 */
@ScraperService(ScraperType.VKONTAKTE)
class VkontakteScraperFactory implements ScraperFactory {
    private final ArtistScraper artistScraper = new AllmusicArtistScraper()
    private final AlbumScraper albumScraper = new AllmusicAlbumScraper()
    private final TrackScraper trackScraper = new AllmusicTrackScraper()
    private final DownloadLinkScraper downloadLinkScraper

    VkontakteScraperFactory() {
        String authToken = new VkontaktePreAuthenticator().preAuthenticate()
        downloadLinkScraper = new VkontakteDownloadLinkScraper(authToken)
    }

    @Override
    ArtistScraper getArtistScraper() {
        return artistScraper
    }

    @Override
    AlbumScraper getAlbumScraper() {
        return albumScraper
    }

    @Override
    TrackScraper getTrackScraper() {
        return trackScraper
    }

    @Override
    DownloadLinkScraper getDownloadLinkScraper() {
        return downloadLinkScraper
    }
}
