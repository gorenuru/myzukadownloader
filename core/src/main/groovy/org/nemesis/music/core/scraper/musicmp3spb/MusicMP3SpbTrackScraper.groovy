package org.nemesis.music.core.scraper.musicmp3spb

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.Configuration
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Track
import org.nemesis.music.domain.ScraperType
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: nuru
 * Date: 23.12.12
 * Time: 10:31
 */
class MusicMP3SpbTrackScraper implements TrackScraper {

    @Override
    List<Track> getAlbumTracks(Album album) {
        List<Track> result = new ArrayList<Track>()
        def bot = new JerryBot()
        bot.navigate album.link.toASCIIString()
        bot.$$(".albSong > div").eachWithIndex { track, index ->
            result.add(extractTrack(track, index, album))
        }
        return result
    }

    /**
     * Extracts Track from specified DOM element
     * @param element - DOM element to parse
     * @param album - related Album instance
     * @return - extracted Track instance
     */
    private Track extractTrack(Jerry element, Integer index, Album album) {
        def baseURI = Configuration.getBaseURI(ScraperType.MUSICMP3SPB)
        def trackElement = element.$(".Name")
        def trackName = trackElement.text()?.trim()
        def id = "${album.id}#${PartsExtractor.instance.extractFromString(trackElement.attr('href'), 1, 2)}"
        def downloadPageLink = new URI("${baseURI.scheme}://${baseURI.rawAuthority}/${trackElement.attr('href')}")
        def durationAndSize = element.$("i").text()
        def duration = durationAndSize.substring(0, durationAndSize.indexOf(" "))
        def rawSize = durationAndSize.substring(durationAndSize.indexOf(" "), durationAndSize.length())?.trim()
        def size = rawSize.substring(3, rawSize.length() - 1)

        return new Track(
                id: id,
                trackNo: index + 1,
                link: downloadPageLink,
                title: trackName,
                duration: duration,
                size: size,
                album: album)
    }
}
