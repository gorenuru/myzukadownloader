package org.nemesis.music.core.event

import org.nemesis.music.core.listener.DownloadProgressListener
import org.nemesis.music.core.plugin.PluginExecutor

/**
 * User: nuru
 * Date: 30.11.12
 * Time: 23:37
 *
 * This class is responsible for:
 *
 * 1) Dispatching download events to the listener
 * 2) Executing plugins according to event type
 */
class DownloadEventDispatcher {
    private DownloadProgressListener listener

    DownloadEventDispatcher(DownloadProgressListener listener) {
        this.listener = listener
    }

    /**
     * Dispatch download event to the listener
     * @param event
     */
    public void dispatch(DownloadEvent event) {
        PluginExecutor.instance.executePlugins(event)

        switch (event.type) {
            case DownloadEventType.TRACK_STARTED:
                listener.trackDownloadStarted(event)
                break
            case DownloadEventType.TRACK_ERROR:
                listener.trackDownloadError(event)
                break
            case DownloadEventType.TRACK_COMPLETED:
                listener.trackDownloaded(event)
                break
            case DownloadEventType.ALBUM_STARTED:
                listener.albumDownloadStarted(event)
                break
            case DownloadEventType.ALBUM_COMPLETED:
                listener.albumDownloadCompleted(event)
                break
        }
    }
}
