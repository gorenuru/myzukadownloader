package org.nemesis.music.core.scraper

import org.nemesis.music.domain.Track

/**
 * User: aalbul
 * Date: 12/21/12
 * Time: 10:38 AM
 *
 * Interface to scrap download link
 */
public interface DownloadLinkScraper {

    /**
     * Retrieves download link for specified tracks
     * @param track - track instance
     * @return download URI
     */
    public URI getDownloadLinkForTrack(Track track)
}