package org.nemesis.music.core.downloader

import org.apache.http.HttpResponse
import org.nemesis.bot.core.JerryBot
import org.nemesis.http.callback.ContentCallback
import org.nemesis.music.core.resolver.DestinationResolver
import org.nemesis.music.core.service.DownloadLinkService
import org.nemesis.music.domain.Track

/**
 * User: aalbul
 * Date: 11/19/12
 * Time: 10:07 AM
 *
 * Utility class to download tracks
 */
class TrackDownloader {
    private final static TrackDownloader INSTANCE = new TrackDownloader()
    private final static DestinationResolver resolver = new DestinationResolver()

    private TrackDownloader() {
    }

    public static TrackDownloader getInstance() {
        return INSTANCE;
    }

    /**
     * Download specified track
     * @param track - track to download
     * @return downloaded file
     */
    public File download(Track track) {
        URI downloadLink = DownloadLinkService.instance.getDownloadLinkForTrack(track)
        File destination = resolver.resolveAndInitializeFolder(track)
        def bot = new JerryBot()
        bot.executeRequest {
                url = downloadLink.toASCIIString()
                callback = new ContentCallback() {
                    @Override
                    String callback(HttpResponse httpResponse) {
                        destination << httpResponse.entity.content
                        return ""
                    }
                }
        }
        return destination
    }
}
