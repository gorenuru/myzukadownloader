package org.nemesis.music.core.event

import org.nemesis.music.domain.Track
import org.nemesis.music.domain.Album

/**
 * User: nuru
 * Date: 30.11.12
 * Time: 22:58
 */
class DownloadEvent {
    private DownloadEventType type
    private Track track
    private List<Track> albumTracks
    private Album album
    private File downloadedFile
    private List<File> albumFiles
    private Exception exception

    public DownloadEventType getType() {
        return type
    }

    public Track getTrack() {
        return track
    }

    public List<Track> getAlbumTracks() {
        return albumTracks
    }

    public Album getAlbum() {
        return album
    }

    public File getDownloadedFile() {
        return downloadedFile
    }

    public List<File> getAlbumFiles() {
        return albumFiles
    }

    public Exception getException() {
        return exception
    }

}
