package org.nemesis.music.core.scraper.allmusic

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.core.scraper.TrackScraper
import org.nemesis.music.core.util.PartsExtractor
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Track

/**
 * User: aalbul
 * Date: 1/16/13
 * Time: 5:46 PM
 */
class AllmusicTrackScraper implements TrackScraper {

    @Override
    List<Track> getAlbumTracks(Album album) {
        List<Track> result = new ArrayList<>()
        def bot = new JerryBot()
        bot.navigate album.link.toASCIIString()
        bot.$$(".table-container:eq(0) tr:gt(0)").each {
            result.add(extractTrack(it, album))
        }
        return result
    }

    /**
     * Extracts Track from specified DOM element
     * @param element - DOM element to parse
     * @param album - related Album instance
     * @return - extracted Track instance
     */
    private Track extractTrack(Jerry element, Album album) {
        def trackNo = Integer.parseInt(element.$(".tracknum").text()?.trim())
        def trackLink = element.$("div.title a")
        def trackName = trackLink.text()?.trim()
        def id = PartsExtractor.instance.extractFromString(trackLink.attr('href') + "/", 3, 4)
        def downloadPageLink = new URI(trackLink.attr('href'))
        def duration = element.$(".time").text()?.trim()

        return new Track(
                id: id,
                trackNo: trackNo,
                link: downloadPageLink,
                title: trackName,
                duration: duration,
                album: album)
    }
}
