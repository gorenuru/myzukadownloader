package org.nemesis.music.core.scraper.jetune

import jodd.jerry.Jerry
import org.nemesis.bot.core.JerryBot
import org.nemesis.music.domain.Album
import org.nemesis.music.domain.Artist
import org.nemesis.music.core.scraper.AlbumScraper
import org.nemesis.music.core.util.PartsExtractor

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 4:28 PM
 */
class JetuneAlbumScraper implements AlbumScraper {

    @Override
    List<Album> getListOfAlbums(Artist artist) {
        List<Album> result = new ArrayList<Album>()
        def bot = new JerryBot()
        bot.navigate "http://www.jetune.ru/artist/${artist.id}/type/0"
        bot.$$(".tr1 > .ap_bgcat").parent().each {
            result.add(extractAlbum(it, artist))
        }
        return result
    }

    /**
     * Extracts album instance from current album HTML DOM node
     * @param albumElement - HTML DOM node
     * @parent artist - artist instnace
     * @param baseURI - site base URI
     * @return composed Album instance
     */
    private Album extractAlbum(Jerry albumElement, Artist artist) {
        def albumLinkNode = albumElement.$("a:eq(1)")
        def title = albumLinkNode.text()?.trim()
        def link = new URI("http://www.jetune.ru${albumLinkNode.attr("href")}")
        def id = PartsExtractor.instance.extractFromString(albumLinkNode.attr("href"), 1, 2)
        def releaseDate = albumElement.$("td:eq(3)").text()?.trim()
        def trackCount = Integer.valueOf(albumElement.$("td:eq(5)").text()?.trim())

        return new Album(
                id: id,
                title: title,
                releaseDate: releaseDate,
                trackCount: trackCount,
                link: link,
                artist: artist)

    }
}
