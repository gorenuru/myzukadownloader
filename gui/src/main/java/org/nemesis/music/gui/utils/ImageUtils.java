package org.nemesis.music.gui.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Set of utils methods for working with the images.
 *
 * User: Eng
 * Date: 25.12.12
 * Time: 22:01
 */
public class ImageUtils {
    private static final Logger logger = LoggerFactory.getLogger(ImageUtils.class);


    /**
     * Loads image from classpath.
     *
     * @param path - relative path to the image
     * @return {@link javax.swing.ImageIcon} from the classpath.
     */
    public static ImageIcon loadImageIcon(String path) {
        ImageIcon imageIcon = null;

        if (StringUtils.isNotEmpty(path)) {
            logger.debug("Loading image from classpath: {}", path);

            // Resolving image URL in classpath.
            URL imageURL = ImageUtils.class.getClassLoader().getResource(path);

            logger.debug("Resolved image URL: {}", imageURL);
            if (imageURL != null) {
                // Creating image icon.
                imageIcon = new ImageIcon(imageURL);
            }
        }

        return imageIcon;
    }

    /**
     * Loads images from directory.
     *
     * @param path - relative path to directory with images.
     * @return List of images from directory.
     */
    public static List<Image> loadImageList(String path) {
        List<Image> imageList = new ArrayList<Image>();

        if (StringUtils.isNotEmpty(path)) {
            logger.debug("Loading images from classpath: {}", path);

            // Resolving images directory URL in class path.
            URL directoryURL = ImageUtils.class.getClassLoader().getResource(path);

            logger.debug("Resolved images directory URL: {}", directoryURL);
            if (directoryURL != null) {
                File directory = new File(directoryURL.getPath());

                if (directory.exists() && directory.isDirectory()) {
                    File[] files = directory.listFiles();

                    if (files != null) {
                        for (File file : files) {
                            logger.debug("Loading image: {}", file.getPath());

                            // Creating image icon.
                            ImageIcon imageIcon = new ImageIcon(file.getPath());

                            if (imageIcon.getImage() != null) {
                                // Adding image to image list.
                                imageList.add(imageIcon.getImage());
                            }
                        }
                    }
                }
            }
        }

        return imageList;
    }
}
