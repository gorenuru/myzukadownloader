package org.nemesis.music.gui.component;

import org.nemesis.music.core.service.DownloadLinkService;
import org.nemesis.music.domain.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.caprica.vlcj.binding.internal.libvlc_state_t;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;

import java.net.URI;

/**
 * User: malbul
 * Date: 1/11/13
 * Time: 3:09 PM
 */
public class VLCPlayer {
    private static final Logger logger = LoggerFactory.getLogger(VLCPlayer.class);

    private NativeDiscovery discovery;
    private MediaPlayer player;


    /**
     * Constructor.
     */
    public VLCPlayer() {
        discovery = new NativeDiscovery();

        init();
    }


    /**
     * Initializes VLCJ library.
     */
    private void init() {
        // Searching for VLC on the local machine.
        boolean isSupport = discovery.discover();

        // Creating headless player.
        player = new MediaPlayerFactory().newHeadlessMediaPlayer();

        if (isSupport) {
            logger.debug("VLC has been found, the audio playback is available.");

        } else {
            logger.debug("VLC is not found, the audio playback is not available.");
        }
    }


    /**
     * Sets the media player event listener.
     *
     * @param listener - {@link MediaPlayerEventListener}
     */
    public void setPlayerEventListener(MediaPlayerEventListener listener) {
        if (player != null) {
            player.addMediaPlayerEventListener(listener);
        }
    }

    public void setVolume(int volume) {
        if (player != null && player.getVolume() != volume) {
            player.setVolume(volume);
        }
    }

    public void setPosition(int position) {
        if (player != null) {
            float newPosition = position / 1000.0f;

            if (newPosition > 0.99f) {
                newPosition = 0.99f;
            }

            player.setPosition(newPosition);
        }
    }

    /**
     * Plays the track.
     *
     * @param track - {@link Track}
     */
    public void play(Track track) {
        if (player != null) {
            // Getting track URI.
            URI trackURI = DownloadLinkService.getInstance().getDownloadLinkForTrack(track);

            if (trackURI != null) {
                logger.debug("Trying to play a track: {}.", track.getTitle());

                player.stop();
                player.playMedia(trackURI.toString());
            } else {
                logger.debug("Can't receive track URI.");
            }
        }
    }

    /**
     * Stops the track playback.
     */
    public void stop() {
        if (player != null) {
            logger.debug("Stopping the track playback.");

            player.stop();
        }
    }

    /**
     * Pause/Resume the track playback.
     */
    public void pause() {
        if (player != null) {
            libvlc_state_t state = player.getMediaPlayerState();

            switch (state) {
                case libvlc_Playing:
                    logger.debug("Pausing the track playback.");

                    player.pause();
                    break;
                case libvlc_Paused:
                    logger.debug("Resuming the track playback.");

                    player.play();
                    break;
            }
        }
    }

    /**
     * Returns the pause flag.
     *
     * @return {@code true} - if playback is paused, otherwise - {@code false}.
     */
    public boolean isPaused() {
        return player != null && player.getMediaPlayerState() == libvlc_state_t.libvlc_Paused;
    }
}
