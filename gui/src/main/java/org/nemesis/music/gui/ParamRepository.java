package org.nemesis.music.gui;

import org.nemesis.music.domain.ScraperType;

/**
 * User: malbul
 * Date: 12/26/12
 * Time: 4:55 PM
 */
public class ParamRepository {
    private static final ParamRepository instance = new ParamRepository();

    /**
     * The website, that will be used as music source.
     * <p>
     *     Default value: {@code ScraperType.MYZUKA}
     * </p>
     */
    private ScraperType scraperType = ScraperType.MYZUKA;


    /**
     * Private constructor.
     */
    private ParamRepository() {

    }


    /**
     * Returns singleton instance of parameters repository.
     *
     * @return Singleton instance of parameters repository.
     */
    public static ParamRepository getInstance() {
        return instance;
    }


    /**
     * Returns the website, that will be used as music source.
     *
     * @return The website, that will be used as music source.
     */
    public ScraperType getScraperType() {
        return scraperType;
    }

    /**
     * Sets the website, that will be used as music source.
     *
     * @param scraperType - the website
     */
    public void setScraperType(ScraperType scraperType) {
        this.scraperType = scraperType;
    }
}
