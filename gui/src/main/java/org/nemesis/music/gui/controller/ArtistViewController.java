package org.nemesis.music.gui.controller;

import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.gui.component.view.ViewActionEvent;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.impl.AlbumView;
import org.nemesis.music.gui.context.ApplicationContext;
import org.nemesis.music.gui.task.SearchAlbumTask;
import org.nemesis.music.gui.task.callback.SearchAlbumTaskCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 2:56 PM
 */
public class ArtistViewController implements ViewActionListener, SearchAlbumTaskCallback {
    private static final Logger logger = LoggerFactory.getLogger(ArtistViewController.class);


    @Override
    public void onAction(ViewActionEvent event) {
        // Getting event type.
        ViewActionEvent.Type eventType = event.getType();

        switch (eventType) {
            case SEARCH:
                onSearch(event);
                break;
        }
    }

    @Override
    public void onSearchResult(List<Album> albumList) {
        logger.debug("Album search task executed with the result: {}.", albumList);

        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        // Setting the list of albums in album view.
        albumView.setAlbumList(albumList);
    }

    /**
     * Called when artist was selected.
     *
     * @param event - {@link ViewActionEvent}
     */
    private void onSearch(ViewActionEvent event) {
        // Getting artist.
        Artist artist = event.getValue(Artist.class);

        logger.debug("Creating album search task for artist '{}'.", artist.getName());

        // Clearing album view.
        ApplicationContext.getInstance().getAlbumView().reset();

        // Creating task.
        SearchAlbumTask task = new SearchAlbumTask(this, artist);

        // Executing task.
        task.execute();
    }
}
