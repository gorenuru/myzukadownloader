package org.nemesis.music.gui.component.view.impl;

import com.alee.extended.image.WebImage;
import com.alee.laf.button.WebButton;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.slider.WebSlider;
import org.apache.commons.lang3.StringUtils;
import org.nemesis.music.domain.Track;
import org.nemesis.music.gui.component.VLCPlayer;
import org.nemesis.music.gui.component.renderer.TrackCellRenderer;
import org.nemesis.music.gui.component.ui.CustomSliderUI;
import org.nemesis.music.gui.component.view.View;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.ViewActionListenerManager;
import org.nemesis.music.gui.context.ApplicationContext;
import org.nemesis.music.gui.utils.ImageUtils;
import org.nemesis.music.gui.utils.TreeNodeState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * User: Eng
 * Date: 28.12.12
 * Time: 20:18
 */
public class ControllView extends MediaPlayerEventAdapter implements View, ActionListener {
    private static final Logger logger = LoggerFactory.getLogger(ControllView.class);

    private static final String PLAY_BUTTON_NAME = "play";
    private static final String PAUSE_BUTTON_NAME = "pause";
    private static final String BACK_BUTTON_NAME = "back";
    private static final String NEXT_BUTTON_NAME = "next";
    private static final String STOP_BUTTON_NAME = "stop";
    private static final String LOOP_BUTTON_NAME = "loop";
    private static final String SHUFFLE_BUTTON_NAME = "shuffle";

    private ViewActionListenerManager listenerManager;

    private VLCPlayer vlcPlayer;

    private List<Track> trackList;
    private int trackIndex = 0;

    private WebPanel contentPane;

    private WebButton playButton;
    private WebButton pauseButton;
    private WebButton backButton;
    private WebButton stopButton;
    private WebButton nextButton;
    private WebToggleButton loopButton;
    private WebToggleButton shuffleButton;

    private WebSlider seekSlider;
    private WebLabel seekTime;

    private WebImage volumeImage;
    private WebSlider volumeSlider;


    /**
     * Constructor.
     */
    public ControllView() {
        listenerManager = new ViewActionListenerManager();
        vlcPlayer = new VLCPlayer();
        vlcPlayer.setPlayerEventListener(this);

        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        logger.debug("Initialization of the player view.");

        // Creating panel.
        contentPane = new WebPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setMinimumSize(new Dimension(6, 6));
    }

    @Override
    public void initStyle() {
        logger.debug("Initialization style of the player view.");

        contentPane.setUndecorated(true);
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        logger.debug("Initialization of the player view components.");

        // Creating play button.
        playButton = new WebButton();
        playButton.setMargin(0, 4, 0, 0);
        playButton.setName(PLAY_BUTTON_NAME);
        playButton.setRolloverDarkBorderOnly(true);
        playButton.setIcon(ImageUtils.loadImageIcon("images/player/play.png"));
        playButton.addActionListener(this);

        // Creating pause button.
        pauseButton = new WebButton();
        pauseButton.setMargin(0, 2, 0, 2);
        pauseButton.setName(PAUSE_BUTTON_NAME);
        pauseButton.setRolloverDarkBorderOnly(true);
        pauseButton.setIcon(ImageUtils.loadImageIcon("images/player/pause.png"));
        pauseButton.setVisible(false);
        pauseButton.addActionListener(this);

        // Creating back button.
        backButton = new WebButton();
        backButton.setName(BACK_BUTTON_NAME);
        backButton.setRolloverDarkBorderOnly(true);
        backButton.setIcon(ImageUtils.loadImageIcon("images/player/back.png"));
        backButton.addActionListener(this);

        // Creating next button.
        nextButton = new WebButton();
        nextButton.setName(NEXT_BUTTON_NAME);
        nextButton.setRolloverDarkBorderOnly(true);
        nextButton.setIcon(ImageUtils.loadImageIcon("images/player/next.png"));
        nextButton.addActionListener(this);

        // Creating stop button.
        stopButton = new WebButton();
        stopButton.setName(STOP_BUTTON_NAME);
        stopButton.setRolloverDarkBorderOnly(true);
        stopButton.setIcon(ImageUtils.loadImageIcon("images/player/stop.png"));
        stopButton.addActionListener(this);

        // Creating loop button.
        loopButton = new WebToggleButton();
        loopButton.setName(LOOP_BUTTON_NAME);
        loopButton.setRolloverDarkBorderOnly(true);
        loopButton.setIcon(ImageUtils.loadImageIcon("images/player/loop.png"));
        loopButton.addActionListener(this);

        // Creating shuffle button;
        shuffleButton = new WebToggleButton();
        shuffleButton.setName(SHUFFLE_BUTTON_NAME);
        shuffleButton.setRolloverDarkBorderOnly(true);
        shuffleButton.setIcon(ImageUtils.loadImageIcon("images/player/shuffle.png"));
        shuffleButton.addActionListener(this);

        // Creating seek slider.
        seekSlider = new WebSlider(0, 1000, 0);
        seekSlider.setValue(0);
        seekSlider.addMouseListener(seekSliderListener);
        seekSlider.setUI(new CustomSliderUI(seekSlider));

        seekTime = new WebLabel("00:00");
        seekTime.setMargin(0, 5, 0, 0);

        // Creating volume image.
        volumeImage = new WebImage();
        volumeImage.setIcon(ImageUtils.loadImageIcon("images/player/volume_on.png"));
        volumeImage.addMouseListener(volumeIconListener);

        // Creating volume slider.
        volumeSlider = new WebSlider(0, 100, 100);
        volumeSlider.setPreferredSize(new Dimension(150, volumeSlider.getPreferredSize().height));
        volumeSlider.addMouseListener(volumeSliderListener);
        volumeSlider.setUI(new CustomSliderUI(volumeSlider));
    }

    @Override
    public void addComponents() {
        logger.debug("Adding components to the content pane.");

        // Creating panel for media buttons.
        WebPanel mediaPanel = new WebPanel(new FlowLayout());

        mediaPanel.add(playButton);
        mediaPanel.add(pauseButton);
        mediaPanel.add(Box.createHorizontalStrut(4));
        mediaPanel.add(backButton);
        mediaPanel.add(stopButton);
        mediaPanel.add(nextButton);
        mediaPanel.add(Box.createHorizontalStrut(4));
        mediaPanel.add(loopButton);
        mediaPanel.add(shuffleButton);

        // Creatin panel for seek slider.
        WebPanel seekPanel = new WebPanel(new BorderLayout());
        seekPanel.setMargin(0, 15, 0, 15);

        seekPanel.add(seekSlider, BorderLayout.CENTER);
        seekPanel.add(seekTime, BorderLayout.EAST);

        // Creating panel for volume slider.
        WebPanel volumePanel = new WebPanel(new BorderLayout());
        volumePanel.setMargin(0, 0, 0, 5);

        volumePanel.add(volumeImage, BorderLayout.WEST);
        volumePanel.add(Box.createHorizontalStrut(2), BorderLayout.CENTER);
        volumePanel.add(volumeSlider, BorderLayout.EAST);

        // Adding panels to the content pane.
        contentPane.add(mediaPanel, BorderLayout.WEST);
        contentPane.add(seekPanel, BorderLayout.CENTER);
        contentPane.add(volumePanel, BorderLayout.EAST);
    }

    @Override
    public void reset() {
        logger.debug("Resetting player view state.");

        if (trackList != null) {
            // Getting current track.
            Track track = trackList.get(trackIndex);
            // Changing the track node state to normal.
            setTrackNodeState(track, TreeNodeState.NORMAL);
        }

        trackIndex = 0;
    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }

    @Override
    public void addViewActionListener(ViewActionListener listener) {
        listenerManager.addViewActionListener(listener);
    }

    @Override
    public void removeViewActionListener(ViewActionListener listener) {
        listenerManager.removeViewActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AbstractButton button = (AbstractButton) e.getSource();

        String name = button.getName();
        if (StringUtils.isNotEmpty(name)) {
            if (StringUtils.equals(name, PLAY_BUTTON_NAME)) {
                play();
            } else
            if (StringUtils.equals(name, PAUSE_BUTTON_NAME)) {
                pause();
            } else
            if (StringUtils.equals(name, BACK_BUTTON_NAME)) {
                back();
            } else
            if (StringUtils.equals(name, NEXT_BUTTON_NAME)) {
                next();
            }
            if (StringUtils.equals(name, STOP_BUTTON_NAME)) {
                stop();
            }
        }
    }

    @Override
    public void finished(MediaPlayer mediaPlayer) {
        next();
    }

    @Override
    public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
        if (seekSlider.isFocusable()) {
            int position = (int) (newPosition * 1000.0f);

            if (seekSlider.getValue() != position) {
                seekSlider.setValue(position);
            }
        }
    }

    @Override
    public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
        long min = TimeUnit.MILLISECONDS.toMinutes(newTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(newTime));
        long sec = TimeUnit.MILLISECONDS.toSeconds(newTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(newTime));

        String time = String.format("%02d:%02d", min, sec);
        seekTime.setText(time);
    }

    /**
     * Returns the tracklist for playback.
     *
     * @return The tracklist for playback.
     */
    public List<Track> getTrackList() {
        return trackList;
    }

    /**
     * Sets the tracklist for playing.
     *
     * @param trackList - tracklist for playing
     */
    public void setTrackList(List<Track> trackList) {
        reset();

        this.trackList = trackList;
    }

    public void playTrack(Track track) {
        if (track != null && trackList != null && trackList.contains(track)) {
            reset();

            // Setting index of track that will be played.
            trackIndex = trackList.indexOf(track);

            play();
        }
    }

    public void play() {
        if (trackList != null) {
            playButton.setVisible(false);
            pauseButton.setVisible(true);

            Track track = trackList.get(trackIndex);

            if (vlcPlayer.isPaused()) {
                vlcPlayer.pause();
            } else {
                // Reseting seek slider value.
                seekSlider.setValue(0);
                // Stopping the track playback.
                vlcPlayer.stop();
                // Starting the track playback.
                vlcPlayer.play(track);
            }

            if (track != null) {
                setTrackNodeState(track, TreeNodeState.PLAY);
            }
        }
    }

    public void pause() {
        if (trackList != null) {
            pauseButton.setVisible(false);
            playButton.setVisible(true);

            // Getting track from track list.
            Track track = trackList.get(trackIndex);
            if (track != null) {
                vlcPlayer.pause();

                setTrackNodeState(track, TreeNodeState.PAUSE);
            }
        }
    }

    public void stop() {
        if (trackList != null) {
            playButton.setVisible(true);
            pauseButton.setVisible(false);

            Track track = trackList.get(trackIndex);

            if (track != null) {
                // Reseting seek slider value.
                seekSlider.setValue(0);
                // Stopping the track playback.
                vlcPlayer.stop();

                setTrackNodeState(track, TreeNodeState.STOP);
            }
        }
    }

    public void back() {
        if (trackList != null) {
            // Getting the current track.
            Track track = trackList.get(trackIndex);
            // Changing the track node state to normal.
            setTrackNodeState(track, TreeNodeState.NORMAL);

//            if (shuffleButton.isSelected()) {
//                // Generating random track index.
//                trackIndex = random.nextInt(trackList.size());
//                // Starting the track playback.
//                play();
//            } else {
                // Generating previous track index.
                trackIndex -= 1;

                if (trackIndex < 0) {
                    // Checking is the loop button was selected.
                    if (loopButton.isSelected()) {
                        // Resetting the track index to the last track of list.
                        trackIndex = trackList.size() - 1;
                        // Starting the track playback.
                        play();
                    } else {
                        // Resetting the track index to zero.
                        trackIndex = 0;
                        // Stopping the track playback.
                        stop();
                    }
                } else {
                    // Starting the track playback.
                    play();
                }
//            }
        }
    }

    public void next() {
        if (trackList != null) {
            // Getting the current track.
            Track track = trackList.get(trackIndex);
            // Changing the track node state to normal.
            setTrackNodeState(track, TreeNodeState.NORMAL);

//            if (shuffleButton.isSelected()) {
//                // Generating random track index.
//                trackIndex = random.nextInt(trackList.size());
//                // Starting the track playback.
//                play();
//            } else {
                // Generating next track index.
                trackIndex += 1;

                if (trackIndex > trackList.size() - 1) {
                    // Resetting the track index to zero.
                    trackIndex %= trackList.size();

                    // Checking is the loop button was selected.
                    if (loopButton.isSelected()) {
                        // Starting the track playback.
                        play();
                    } else {
                        // Stopping the playback.
                        stop();
                    }
                } else {
                    // Starting the track playback.
                    play();
                }
//            }
        }
    }

    /**
     * Sets the track node state.
     *
     * @param track - the track
     * @param state - the node state
     */
    private void setTrackNodeState(Track track, TreeNodeState state) {
        // Getting album view.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();
        // Receiving track cell renderer.
        TrackCellRenderer trackCellRenderer = albumView.getTrackCellRenderer();

        // Changing track state.
        trackCellRenderer.setTrackState(track, state);
        // Repainting album view.
        albumView.repaint();
    }


    /**
     * Seek slider listener.
     */
    private MouseAdapter seekSliderListener = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
            seekSlider.setFocusable(false);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            vlcPlayer.setPosition(seekSlider.getValue());
            seekSlider.setFocusable(true);
        }
    };

    /**
     * Volume icon mouse listener.
     */
    private MouseAdapter volumeIconListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            int volume = volumeSlider.getValue();

            if (volume == 0) {
                volumeImage.setIcon(ImageUtils.loadImageIcon("images/player/volume_on.png"));
                volumeSlider.setValue(100);
                vlcPlayer.setVolume(100);
            } else {
                volumeImage.setIcon(ImageUtils.loadImageIcon("images/player/volume_off.png"));
                volumeSlider.setValue(0);
                vlcPlayer.setVolume(0);
            }
        }
    };

    /**
     * Volume slider mouse listener.
     */
    private MouseAdapter volumeSliderListener = new MouseAdapter() {
        @Override
        public void mouseReleased(MouseEvent e) {
            vlcPlayer.setVolume(volumeSlider.getValue());
        }
    };
}
