package org.nemesis.music.gui.component.view;

/**
 * View action event.
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 1:30 PM
 */
public class ViewActionEvent {
    /**
     * Enum of event types.
     */
    public enum Type {
        SEARCH,
        CHANGE,
        PROCESS,
        PLAY,
        PAUSE,
        BACK,
        NEXT
    }


    /**
     * The object in which the event has occurred.
     */
    private Object source;

    /**
     * Event value.
     */
    private Object value;

    /**
     * Event type.
     */
    private Type type;


    /**
     * Constructor.
     *
     * @param source - the object in which the event has occurred
     * @param value - event value
     * @param type - event type
     */
    public ViewActionEvent(Object source, Object value, Type type) {
        if (source == null || value == null || type == null) {
            throw new NullPointerException();
        }

        this.source = source;
        this.value = value;
        this.type = type;
    }


    /**
     * Returns the object in which the event has occurred.
     *
     * @return The object in which the event has occurred
     */
    public Object getSource() {
        return source;
    }

    /**
     *
     *
     * @param type
     * @param <T>
     * @return
     */
    public <T> T getSource(Class<T> type) {
        return type.cast(source);
    }

    /**
     * Returns event value.
     *
     * @return Event value.
     */
    public Object getValue() {
        return value;
    }

    /**
     *
     *
     * @param type
     * @param <T>
     * @return
     */
    public <T> T getValue(Class<T> type) {
        return type.cast(value);
    }

    /**
     * Returns event type.
     *
     * @return Even type.
     * @see Type
     */
    public Type getType() {
        return type;
    }
}
