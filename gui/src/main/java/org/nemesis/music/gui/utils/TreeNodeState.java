package org.nemesis.music.gui.utils;

/**
 * User: malbul
 * Date: 12/28/12
 * Time: 10:56 AM
 */
public enum TreeNodeState {
    NORMAL,
    QUEUE,
    IN_PROGRESS,
    DONE,
    ERROR,
    PLAY,
    PAUSE,
    STOP
}
