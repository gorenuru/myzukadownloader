package org.nemesis.music.gui.component.view.impl;

import com.alee.extended.image.WebImage;
import com.alee.extended.progress.WebProgressOverlay;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import org.apache.commons.lang3.StringUtils;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.gui.component.view.View;
import org.nemesis.music.gui.component.view.ViewActionEvent;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.ViewActionListenerManager;
import org.nemesis.music.gui.utils.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 *
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 2:01 PM
 */
public class SearchView implements View, ItemListener, ActionListener {
    private static final Logger logger = LoggerFactory.getLogger(SearchView.class);

    private static final String FIELD_IMAGE_PATH = "images/search.png";


    private ViewActionListenerManager listenerManager;

    private WebPanel contentPane;

    private WebTextField field;
    private WebImage fieldImage;
    private WebComboBox fieldComboBox;
    private WebProgressOverlay fieldOverlay;


    /**
     * Constructor.
     */
    public SearchView() {
        listenerManager = new ViewActionListenerManager();

        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        logger.debug("Initialization of the search view.");

        // Creating panel.
        contentPane = new WebPanel();
        contentPane.setLayout(new BorderLayout());
    }

    @Override
    public void initStyle() {
        logger.debug("Initialization style of the search view.");

        contentPane.setUndecorated(true);
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        logger.debug("Initialization of the search view components.");

        // Creating field image;
        fieldImage = new WebImage(ImageUtils.loadImageIcon(FIELD_IMAGE_PATH));
        fieldImage.setCursor(Cursor.getDefaultCursor());

        // Creating combo box.
        fieldComboBox = new WebComboBox(ScraperType.values());
        fieldComboBox.setCursor(Cursor.getDefaultCursor());
        fieldComboBox.addItemListener(this);

        // Creating field.
        field = new WebTextField(15);
        field.setLeadingComponent(fieldImage);
        field.setTrailingComponent(fieldComboBox);
//        field.setFieldMargin(0, 3, 0, 3);
        field.setMargin(0, 2, 0, 2);
        field.addActionListener(this);
        field.setWebColored(true);

        // Creating field overlay.
        fieldOverlay = new WebProgressOverlay();
        fieldOverlay.setProgressColor(Color.black);
        fieldOverlay.setConsumeEvents(false);
        fieldOverlay.setOpaque(false);
        fieldOverlay.setComponent(field);
    }

    @Override
    public void addComponents() {
        logger.debug("Adding components to the content pane.");

        contentPane.add(fieldOverlay, BorderLayout.EAST);
    }

    @Override
    public void reset() {
        logger.debug("Resetting search view state.");

        // Clearing field.
        field.clear();
    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }

    @Override
    public void addViewActionListener(ViewActionListener listener) {
        listenerManager.addViewActionListener(listener);
    }

    @Override
    public void removeViewActionListener(ViewActionListener listener) {
        listenerManager.removeViewActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String artistName = field.getText();

        if (StringUtils.isNotEmpty(artistName)) {
            // Notifying listeners.
            ViewActionEvent event = new ViewActionEvent(field, artistName, ViewActionEvent.Type.SEARCH);
            listenerManager.fireViewActionEvent(event);
        } else {
            // Shows error dialog.
            WebOptionPane.showMessageDialog(contentPane.getParent(), "Please, enter artist name!", "Warning!", WebOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        int state = e.getStateChange();

        switch (state) {
            case ItemEvent.SELECTED:
                Object obj = e.getItem();

                if (obj != null) {
                    ScraperType scraperType = (ScraperType) obj;

                    // Notifying listeners.
                    ViewActionEvent event = new ViewActionEvent(fieldComboBox, scraperType, ViewActionEvent.Type.CHANGE);
                    listenerManager.fireViewActionEvent(event);
                }
                break;
        }
    }

    /**
     * Returns progress overlay.
     *
     * @return Progress overlay.
     */
    public WebProgressOverlay getProgressOverlay() {
        return fieldOverlay;
    }
}
