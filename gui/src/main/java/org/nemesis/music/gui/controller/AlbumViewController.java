package org.nemesis.music.gui.controller;

import org.nemesis.music.core.downloader.AlbumDownloader;
import org.nemesis.music.core.event.DownloadEvent;
import org.nemesis.music.core.listener.DownloadProgressListener;
import org.nemesis.music.core.service.TrackService;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;
import org.nemesis.music.gui.component.view.ViewActionEvent;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.impl.AlbumView;
import org.nemesis.music.gui.component.view.impl.ControllView;
import org.nemesis.music.gui.context.ApplicationContext;
import org.nemesis.music.gui.task.SearchTrackTask;
import org.nemesis.music.gui.task.callback.SearchTrackTaskCallback;
import org.nemesis.music.gui.utils.TreeNodeState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.List;

/**
 * User: malbul
 * Date: 12/28/12
 * Time: 2:56 PM
 */
public class AlbumViewController implements ViewActionListener, SearchTrackTaskCallback, DownloadProgressListener {
    private static final Logger logger = LoggerFactory.getLogger(AlbumViewController.class);


    @Override
    public void onAction(ViewActionEvent event) {
        // Getting event type.
        ViewActionEvent.Type eventType = event.getType();

        switch (eventType) {
            case SEARCH:
                onSearch(event);
                break;
            case PROCESS:
                onDownload(event);
                break;
            case PLAY:
                onPlay(event);
                break;
        }
    }

    @Override
    public void onSearchResult(Album album, List<Track> trackList) {
        logger.debug("Track search task executed with the result: {}.", trackList);

        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        // Setting the list of tracks in album view.
        albumView.setTrackList(album, trackList);
    }

    /**
     * Called when album was expanded.
     *
     * @param event - {@link ViewActionEvent}
     */
    private void onSearch(ViewActionEvent event) {
        // Getting album.
        Album album = event.getValue(Album.class);

        logger.debug("Creating track search task for album '{}'.", album.getTitle());

        // Creating search task.
        SearchTrackTask task = new SearchTrackTask(this, album);

        // Executing task.
        task.execute();
    }

    /**
     * Called when album was selected for downloading.
     *
     * @param event - {@link ViewActionEvent}
     */
    private void onDownload(ViewActionEvent event) {
        // Getting album.
        Album album = event.getValue(Album.class);

        if (album != null) {
            // Getting album view from application context.
            AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

            // Changing album state.
            albumView.getAlbumCellRenderer().setAlbumState(album, TreeNodeState.QUEUE);
            albumView.repaint();

            // Downloading album.
            AlbumDownloader.getInstance().downloadAlbum(album, this);
        }
    }

    /**
     * Called when album was selected for playing.
     *
     * @param event - {@link ViewActionEvent}
     */
    private void onPlay(ViewActionEvent event) {
        final Object obj = event.getValue();

        if (obj != null) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (Track.class.isInstance(obj)) {
                        Track track = (Track) obj;

                        ControllView controllView = ApplicationContext.getInstance().getControllView();

                        if (controllView.getTrackList() != null && controllView.getTrackList().contains(track)) {
                            controllView.playTrack(track);
                        } else {
                            // Getting album tracks.
                            List<Track> trackList = TrackService.getInstance().getAlbumTracks(track.getAlbum());

                            controllView.setTrackList(trackList);
                            controllView.playTrack(track);
                        }
                    } else
                    if (Album.class.isInstance(obj)) {
                        Album album = (Album) obj;

                        // Getting album tracks.
                        List<Track> trackList = TrackService.getInstance().getAlbumTracks(album);

                        ControllView controllView = ApplicationContext.getInstance().getControllView();

                        controllView.setTrackList(trackList);
                        controllView.play();
                    }
                }
            });
        }
    }


    /**
     * Indicates that download of the track is started
     * @param - download event instance
     */
    @Override
    public void trackDownloadStarted(DownloadEvent event) {
        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        albumView.getTrackCellRenderer().setTrackState(event.getTrack(), TreeNodeState.IN_PROGRESS);
        albumView.repaint();
    }

    /**
     * Indicates that track is downloaded
     */
    @Override
    public void trackDownloaded(DownloadEvent event) {
        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        albumView.getTrackCellRenderer().setTrackState(event.getTrack(), TreeNodeState.DONE);
        albumView.repaint();
    }

    /**
     * Indicates that error occurred during track download process
     */
    @Override
    public void trackDownloadError(DownloadEvent event) {
        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        albumView.getTrackCellRenderer().setTrackState(event.getTrack(), TreeNodeState.ERROR);
        albumView.repaint();
    }

    /**
     * Indicates that album download process is started
     */
    @Override
    public void albumDownloadStarted(DownloadEvent event) {
        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        albumView.getAlbumCellRenderer().setAlbumState(event.getAlbum(), TreeNodeState.IN_PROGRESS);
        albumView.repaint();
    }

    /**
     * Indicates that album download process is finished
     */
    @Override
    public void albumDownloadCompleted(DownloadEvent event) {
        // Getting album view from application context.
        AlbumView albumView = ApplicationContext.getInstance().getAlbumView();

        albumView.getAlbumCellRenderer().setAlbumState(event.getAlbum(), TreeNodeState.DONE);
        albumView.repaint();
    }
}
