package org.nemesis.music.gui.component.view.impl;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.laf.button.WebButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tree.WebTree;
import com.alee.managers.popup.PopupStyle;
import com.alee.managers.popup.WebPopup;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;
import org.nemesis.music.gui.component.renderer.AlbumCellRenderer;
import org.nemesis.music.gui.component.renderer.TrackCellRenderer;
import org.nemesis.music.gui.component.renderer.TreeCellRenderer;
import org.nemesis.music.gui.component.view.View;
import org.nemesis.music.gui.component.view.ViewActionEvent;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.ViewActionListenerManager;
import org.nemesis.music.gui.utils.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: malbul
 * Date: 12/28/12
 * Time: 2:31 PM
 */
public class AlbumView implements View, TreeWillExpandListener, MouseListener {
    private static final Logger logger = LoggerFactory.getLogger(AlbumView.class);

    private ViewActionListenerManager listenerManager;

    private Map<String, DefaultMutableTreeNode> albumMapping;

    private WebPanel contentPane;

    private WebPopup popup;
    private WebButton popupDownload;
    private WebButton popupPlay;

    private AlbumCellRenderer albumCellRenderer;
    private TrackCellRenderer trackCellRenderer;

    private DefaultMutableTreeNode rootNode;
    private DefaultTreeModel treeModel;
    private WebTree tree;
    private WebScrollPane treeScrollPane;


    /**
     * Constructor.
     */
    public AlbumView() {
        listenerManager = new ViewActionListenerManager();
        albumMapping = new HashMap<String, DefaultMutableTreeNode>();

        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        logger.debug("Initialization of the album view.");

        contentPane = new WebPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setMinimumSize(new Dimension(6, 6));
    }

    @Override
    public void initStyle() {
        logger.debug("Initialization style of the album view.");

        contentPane.setUndecorated(false);
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        logger.debug("Initialization of the album view components.");

        // Creating renderers.
        albumCellRenderer = new AlbumCellRenderer();
        trackCellRenderer = new TrackCellRenderer();

        // Creating root node.
        rootNode = new DefaultMutableTreeNode("root");

        // Creating tree model.
        treeModel = new DefaultTreeModel(rootNode);

        // Creating tree.
        tree = new WebTree(treeModel);
        tree.setCellRenderer(new TreeCellRenderer(albumCellRenderer, trackCellRenderer));
        tree.setSelectionMode(WebTree.SINGLE_TREE_SELECTION);
        tree.addTreeWillExpandListener(this);
        tree.addMouseListener(this);
        tree.setShowsRootHandles(true);
        tree.setRootVisible(false);
        tree.setOpaque(false);

        // Creating tree scroll pane.
        treeScrollPane = new WebScrollPane(tree);
        treeScrollPane.setDrawBorder(false);
        treeScrollPane.setDrawFocus(false);

        // Creating popup menu.
        popup = new WebPopup();
        popup.setMargin(5, 5, 5, 5);
        popup.setPopupStyle(PopupStyle.grayDownTip);
        popup.setBackground(Color.lightGray);
        popup.setLayout(new VerticalFlowLayout());

        // Creating popup download item.
        popupDownload = new WebButton("Download");
        popupDownload.addActionListener(popupDownloadListener);
        popupDownload.setIcon(ImageUtils.loadImageIcon("images/menu/download.png"));

        // Creating popup play item.
        popupPlay = new WebButton("Play");
        popupPlay.addActionListener(popupPlayListener);
        popupPlay.setIcon(ImageUtils.loadImageIcon("images/menu/play.png"));
    }

    @Override
    public void addComponents() {
        logger.debug("Adding components to the content pane.");

        contentPane.add(treeScrollPane, BorderLayout.CENTER);

        popup.add(popupDownload);
        popup.add(popupPlay);
        popup.packPopup();
    }

    @Override
    public void reset() {
        logger.debug("Resetting album view state.");

        // Clear album mapping.
        albumMapping.clear();
        // Remove all albums from tree.
        rootNode.removeAllChildren();
        // Reloading model.
        treeModel.reload();
    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }

    @Override
    public void addViewActionListener(ViewActionListener listener) {
        listenerManager.addViewActionListener(listener);
    }

    @Override
    public void removeViewActionListener(ViewActionListener listener) {
        listenerManager.removeViewActionListener(listener);
    }

    @Override
    public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
        DefaultMutableTreeNode albumNode = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();

        if (albumNode != null && albumNode.getChildCount() == 1 && albumNode.getAllowsChildren()) {
            Object obj = albumNode.getUserObject();

            if (obj != null) {
                Album album = (Album) obj;

                // Notifying listeners.
                ViewActionEvent actionEvent = new ViewActionEvent(tree, album, ViewActionEvent.Type.SEARCH);
                listenerManager.fireViewActionEvent(actionEvent);
            }
        }
    }

    @Override
    public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
        // Not implemented.
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            // Getting node path where mouse was clicked.
            TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());

            if (treePath != null) {
                // Getting tree node from this path.
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();

                // Check that it is an album, not track.
                if (treeNode.getAllowsChildren()) {
                    // Set this tree node selected.
                    tree.setSelectionPath(treePath);
                    // Getting bounds for tree node.
                    Rectangle rectangle = tree.getPathBounds(treePath);

                    if (rectangle != null) {
                        // Shows popup menu.
                        popup.showPopup(tree, rectangle.x, rectangle.y + rectangle.height);
                    }
                }
            }
        } else if (SwingUtilities.isLeftMouseButton(e)) {
            if (popup.isShowing() && !popup.contains(e.getPoint())) {
                popup.hidePopup();
            }

            if (e.getClickCount() == 2) {
                // Getting node path where mouse was clicked.
                TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());

                if (treePath != null) {
                    // Getting tree node from this path.
                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();

                    // Check that it is an track, not album.
                    if (!treeNode.getAllowsChildren()) {
                        // Set this tree node selected.
                        tree.setSelectionPath(treePath);
                        // Getting track from tree node.
                        Track track = (Track) treeNode.getUserObject();
                        // Starting the track playback.
                        // Notifying listeners.
                        ViewActionEvent event = new ViewActionEvent(tree, track, ViewActionEvent.Type.PLAY);
                        listenerManager.fireViewActionEvent(event);
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Not implemented.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Not implemented.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Not implemented.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Not implemented.
    }

    /**
     * Returns selected object in tree.
     *
     * @return Selected object in tree.
     */
    private Object getSelectedObject() {
        Object obj = null;

        TreePath treePath = tree.getSelectionPath();
        if (treePath != null) {
            DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
            obj = treeNode.getUserObject();
        }

        return obj;
    }

    /**
     * Repaints tree.
     */
    public void repaint() {
        tree.repaint();
    }

    /**
     * Returns album cell renderer.
     *
     * @return Album cell renderer.
     */
    public AlbumCellRenderer getAlbumCellRenderer() {
        return albumCellRenderer;
    }

    /**
     * Returns track cell renderer.
     *
     * @return Track cell renderer.
     */
    public TrackCellRenderer getTrackCellRenderer() {
        return trackCellRenderer;
    }

    /**
     * Sets list of artist albums to view.
     *
     * @param albumList - list of artist albums
     */
    public void setAlbumList(List<Album> albumList) {
        // Clear album mapping.
        albumMapping.clear();
        // Remove all albums from tree.
        rootNode.removeAllChildren();

        if (albumList != null && albumList.size() > 0) {
            // Adding all albums to root node.
            for (Album album : albumList) {
                DefaultMutableTreeNode albumNode = new DefaultMutableTreeNode(album);
                DefaultMutableTreeNode emptyNode = new DefaultMutableTreeNode();

                albumNode.add(emptyNode);
                rootNode.add(albumNode);

                albumMapping.put(album.getId(), albumNode);
            }

            // Reloading model.
            treeModel.reload();
            // Expanding root node.
            tree.expandPath(new TreePath(rootNode.getPath()));
        }
    }

    /**
     * Sets list of album tracks to view.
     *
     * @param album - tracks album
     * @param trackList - list of album tracks
     */
    public void setTrackList(Album album, List<Track> trackList) {
        if (album != null && trackList != null && trackList.size() > 0) {
            // Getting album node.
            DefaultMutableTreeNode albumNode = albumMapping.get(album.getId());

            // Verify search result.
            if (albumNode != null) {
                albumNode.removeAllChildren();

                // Adding tracks to album node.
                for (Track track : trackList) {
                    DefaultMutableTreeNode trackNode = new DefaultMutableTreeNode(track);

                    trackNode.setAllowsChildren(false);

                    albumNode.add(trackNode);
                }

                // Reloading node.
                treeModel.reload(albumNode);
                // Expanding album node.
                tree.expandPath(new TreePath(albumNode.getPath()));
            }
        }
    }


    /**
     * Implementation of popup download button listener.
     */
    private ActionListener popupDownloadListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            popup.hidePopup();
            Object obj = getSelectedObject();

            if (obj != null) {
                Album album = (Album) obj;

                // Notifying listeners.
                ViewActionEvent event = new ViewActionEvent(tree, album, ViewActionEvent.Type.PROCESS);
                listenerManager.fireViewActionEvent(event);
            }
        }
    };

    /**
     * Implementation of popup play button listener.
     */
    private ActionListener popupPlayListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            popup.hidePopup();
            Object obj = getSelectedObject();

            if (obj != null) {
                Album album = (Album) obj;

                // Notifying listeners.
                ViewActionEvent event = new ViewActionEvent(tree, album, ViewActionEvent.Type.PLAY);
                listenerManager.fireViewActionEvent(event);
            }
        }
    };
}
