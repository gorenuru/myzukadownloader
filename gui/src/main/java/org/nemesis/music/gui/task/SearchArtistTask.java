package org.nemesis.music.gui.task;

import org.nemesis.music.core.service.ArtistService;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.gui.ParamRepository;
import org.nemesis.music.gui.task.callback.SearchArtistTaskCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 12/26/12
 * Time: 5:39 PM
 */
public class SearchArtistTask extends AbstractTask<Void> {
    private static final Logger logger = LoggerFactory.getLogger(SearchArtistTask.class);

    private SearchArtistTaskCallback taskCallback;
    private String artistName;


    /**
     * Constructor.
     */
    public SearchArtistTask(SearchArtistTaskCallback taskCallback, String artistName) {
        this.taskCallback = taskCallback;
        this.artistName = artistName;
    }


    @Override
    public void doInBackgroundImpl() {
        // Getting scraper type.
        ScraperType scraperType = ParamRepository.getInstance().getScraperType();

        logger.debug("Searching for artist '{}', using website '{}'", artistName, scraperType);

        // Getting artist list.
        List<Artist> artistList = ArtistService.getInstance().getArtistsByKeyword(artistName, scraperType);

        logger.debug("Found {} artists.", artistList.size());

        if (!isCancelled()) {
            // Invoking callback method.
            taskCallback.onSearchResult(artistList);
        }
    }

    @Override
    public void onProgressUpdate(List<Void> chunks) {
        // Not implemented.
    }

    @Override
    public void onPostExecute() {
        // Not implemented.
    }

    @Override
    public void onCancelled() {
        // Not implemented.
    }
}
