package org.nemesis.music.gui.task.callback;

import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;

import java.util.List;

/**
 * User: malbul
 * Date: 12/28/12
 * Time: 10:44 AM
 */
public interface SearchTrackTaskCallback {
    /**
     *
     *
     * @param album
     * @param trackList
     */
    public void onSearchResult(Album album, List<Track> trackList);
}
