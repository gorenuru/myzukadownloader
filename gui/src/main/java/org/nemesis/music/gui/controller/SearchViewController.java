package org.nemesis.music.gui.controller;

import org.nemesis.music.domain.Artist;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.gui.ParamRepository;
import org.nemesis.music.gui.component.view.ViewActionEvent;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.impl.ArtistView;
import org.nemesis.music.gui.component.view.impl.SearchView;
import org.nemesis.music.gui.context.ApplicationContext;
import org.nemesis.music.gui.task.SearchArtistTask;
import org.nemesis.music.gui.task.callback.SearchArtistTaskCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 2:55 PM
 */
public class SearchViewController implements ViewActionListener, SearchArtistTaskCallback {
    private static final Logger logger = LoggerFactory.getLogger(SearchViewController.class);


    @Override
    public void onAction(ViewActionEvent event) {
        // Getting event type.
        ViewActionEvent.Type eventType = event.getType();

        switch (eventType) {
            case SEARCH:
                onSearch(event);
                break;

            case CHANGE:
                onScraperTypeChanged(event);
                break;
        }
    }

    @Override
    public void onSearchResult(List<Artist> artistList) {
        logger.debug("Artist search task executed with the result: {}.", artistList);

        // Disabling progress overlay.
        SearchView searchView = ApplicationContext.getInstance().getSearchView();

        searchView.getProgressOverlay().getComponent().setEnabled(true);
        searchView.getProgressOverlay().setShowLoad(false);

        // Getting artist view from application context.
        ArtistView artistView = ApplicationContext.getInstance().getArtistView();

        // Setting the list of artists in artist view.
        artistView.setArtistList(artistList);
    }

    /**
     * Called when search action performed.
     *
     * @param event - {@link ViewActionEvent}
     */
    private void onSearch(ViewActionEvent event) {
        // Getting artist name from event.
        String artistName = event.getValue(String.class);

        logger.debug("Creating artist search task for artist '{}'.", artistName);

        // Enabling progress overlay.
        SearchView searchView = ApplicationContext.getInstance().getSearchView();

        searchView.getProgressOverlay().getComponent().setEnabled(false);
        searchView.getProgressOverlay().setShowLoad(true);

        // Clear artist view and album view.
        ApplicationContext.getInstance().getArtistView().reset();
        ApplicationContext.getInstance().getAlbumView().reset();

        // Creating task.
        SearchArtistTask task = new SearchArtistTask(this, artistName);

        // Executing task.
        task.execute();
    }

    /**
     * Called when scraper type has changed.
     *
     * @param event - {@link ViewActionEvent}
     */
    private void onScraperTypeChanged(ViewActionEvent event) {
        ScraperType scraperType = event.getValue(ScraperType.class);

        logger.debug("Scraper type changed to: {}", scraperType);

        // Getting instance of parameters perository.
        ParamRepository paramRepository = ParamRepository.getInstance();

        // Changing scraper type value.
        paramRepository.setScraperType(scraperType);
    }
}
