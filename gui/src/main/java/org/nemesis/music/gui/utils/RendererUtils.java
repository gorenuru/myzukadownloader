package org.nemesis.music.gui.utils;

import javax.swing.*;
import java.awt.*;

/**
 *
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 10:57 AM
 */
public class RendererUtils {
    public static Color NORMAL_TEXT_COLOR = Color.decode("#000000");
    public static Color IN_PROGRESS_TEXT_COLOR = Color.decode("#4169E1");
    public static Color DONE_TEXT_COLOR = Color.decode("#3CB371");
    public static Color ERROR_TEXT_COLOR = Color.decode("#DC143C");
    public static Color QUEUE_TEXT_COLOR = Color.decode("#8A2BE2");

    public static ImageIcon PROGRESS_ICON = ImageUtils.loadImageIcon("images/state/loading.png");
    public static ImageIcon DONE_ICON = ImageUtils.loadImageIcon("images/state/done.png");
    public static ImageIcon ERROR_ICON = ImageUtils.loadImageIcon("images/state/error.png");
    public static ImageIcon PLAY_ICON = ImageUtils.loadImageIcon("images/state/play.png");
    public static ImageIcon PAUSE_ICON = ImageUtils.loadImageIcon("images/state/pause.png");
    public static ImageIcon STOP_ICON = ImageUtils.loadImageIcon("images/state/stop.png");


    /**
     * Returns the text color for the {@link TreeNodeState}.
     *
     * @param state - {@link TreeNodeState}
     * @return Text color for the {@link TreeNodeState}.
     */
    public static Color getTextColorForState(TreeNodeState state) {
        Color color = NORMAL_TEXT_COLOR;

        if (state != null) {
            switch (state) {
                case QUEUE:
                    color = QUEUE_TEXT_COLOR;
                    break;
                case IN_PROGRESS:
                    color = IN_PROGRESS_TEXT_COLOR;
                    break;
                case DONE:
                    color = DONE_TEXT_COLOR;
                    break;
                case ERROR:
                    color = ERROR_TEXT_COLOR;
                    break;
            }
        }

        return color;
    }

    /**
     * Returns icon for the {@link TreeNodeState}.
     *
     * @param state - {@link TreeNodeState}
     * @return Icon for the {@link TreeNodeState}.
     */
    public static ImageIcon getIconForState(TreeNodeState state) {
        ImageIcon imageIcon = null;

        if (state != null) {
            switch (state) {
                case PLAY:
                    imageIcon = PLAY_ICON;
                    break;
                case PAUSE:
                    imageIcon = PAUSE_ICON;
                    break;
                case STOP:
                    imageIcon = STOP_ICON;
                    break;
                case IN_PROGRESS:
                    imageIcon = PROGRESS_ICON;
                    break;
                case DONE:
                    imageIcon = DONE_ICON;
                    break;
                case ERROR:
                    imageIcon = ERROR_ICON;
                    break;
            }
        }

        return imageIcon;
    }
}
