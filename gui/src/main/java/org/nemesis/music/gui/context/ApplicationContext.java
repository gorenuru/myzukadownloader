package org.nemesis.music.gui.context;

import org.nemesis.music.gui.component.view.impl.AlbumView;
import org.nemesis.music.gui.component.view.impl.ArtistView;
import org.nemesis.music.gui.component.view.impl.ControllView;
import org.nemesis.music.gui.component.view.impl.SearchView;
import org.nemesis.music.gui.component.window.ApplicationWindow;
import org.nemesis.music.gui.controller.AlbumViewController;
import org.nemesis.music.gui.controller.ArtistViewController;
import org.nemesis.music.gui.controller.SearchViewController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application context that contains all instances of application objects.
 *
 * User: Eng
 * Date: 25.12.12
 * Time: 22:15
 */
public class ApplicationContext {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationContext.class);

    private static final ApplicationContext instance = new ApplicationContext();

    private ApplicationWindow applicationWindow;

    private SearchView searchView;
    private ArtistView artistView;
    private AlbumView albumView;
    private ControllView controllView;

    private SearchViewController searchViewController;
    private ArtistViewController artistViewController;
    private AlbumViewController albumViewController;


    /**
     * Private constructor.
     */
    private ApplicationContext() {
        init();
    }


    /**
     * Initializes application context.
     */
    private void init() {
        logger.debug("Initialization of the application context.");

        // Creating application window.
        applicationWindow = new ApplicationWindow();

        // Creating views.
        searchView = new SearchView();
        artistView = new ArtistView();
        albumView = new AlbumView();
        controllView = new ControllView();

        // Adding views to the application window.
        applicationWindow.setTopView(searchView.getContentPane());
        applicationWindow.setLeftView(artistView.getContentPane());
        applicationWindow.setRightView(albumView.getContentPane());
        applicationWindow.setBottomView(controllView.getContentPane());

        // Creating controllers.
        searchViewController = new SearchViewController();
        artistViewController = new ArtistViewController();
        albumViewController = new AlbumViewController();

        // Adding controllers to views.
        searchView.addViewActionListener(searchViewController);
        artistView.addViewActionListener(artistViewController);
        albumView.addViewActionListener(albumViewController);
    }

    /**
     * Returns singleton instance of application context.
     *
     * @return Singleton instance of application context.
     */
    public static ApplicationContext getInstance() {
        return instance;
    }

    /**
     * Shows application window.
     */
    public void showApplicationWindow() {
        applicationWindow.setVisible(true);
    }

    /**
     * Returns instance of search view.
     *
     * @return Instance of search view.
     */
    public SearchView getSearchView() {
        return searchView;
    }

    /**
     * Returns instance of artist view.
     *
     * @return Instance of artist view.
     */
    public ArtistView getArtistView() {
        return artistView;
    }

    /**
     * Returns instance of album view.
     *
     * @return Instance of album view.
     */
    public AlbumView getAlbumView() {
        return albumView;
    }

    /**
     * Returns instance of player view.
     *
     * @return Instance of player view.
     */
    public ControllView getControllView() {
        return controllView;
    }
}
