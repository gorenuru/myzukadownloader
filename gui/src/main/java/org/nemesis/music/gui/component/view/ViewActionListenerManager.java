package org.nemesis.music.gui.component.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Manager, who is responsible for notifying listeners about events.
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 1:53 PM
 */
public class ViewActionListenerManager {
    private static final Logger logger = LoggerFactory.getLogger(ViewActionListenerManager.class);

    private List<ViewActionListener> listenerList;


    /**
     * Constructor.
     */
    public ViewActionListenerManager() {
        listenerList = new ArrayList<ViewActionListener>();
    }

    /**
     * Adds the specified view action listener to receive action events from this component.
     *
     * @param listener - view action listener
     */
    public void addViewActionListener(ViewActionListener listener) {
        logger.debug("Adding view action listener: {}.", listener);

        if (listener != null) {
            listenerList.add(listener);
        }
    }

    /**
     * Removes the specified view action listener so that it no longer receives action events from this component.
     *
     * @param listener - view action listener
     */
    public void removeViewActionListener(ViewActionListener listener) {
        logger.debug("Removing view action listener: {}.", listener);

        if (listener != null) {
            listenerList.remove(listener);
        }
    }

    /**
     * Notifies listeners about action that has occurred.
     *
     * @param event - view action event
     */
    public void fireViewActionEvent(ViewActionEvent event) {
        logger.debug("Notifying listeners about the action that has occurred.");

        for (ViewActionListener listener : listenerList) {
            listener.onAction(event);
        }
    }
}
