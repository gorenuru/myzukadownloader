package org.nemesis.music.gui.task;

import org.nemesis.music.core.service.AlbumService;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.AlbumType;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.gui.task.callback.SearchAlbumTaskCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * User: malbul
 * Date: 12/5/12
 * Time: 12:28 PM
 */
public class SearchAlbumTask extends AbstractTask<Void> {
    private static final Logger logger = LoggerFactory.getLogger(SearchAlbumTask.class);

    private SearchAlbumTaskCallback taskCallback;
    private Artist artist;


    /**
     * Constructor.
     */
    public SearchAlbumTask(SearchAlbumTaskCallback taskCallback, Artist artist) {
        this.taskCallback = taskCallback;
        this.artist = artist;
    }


    @Override
    public void doInBackgroundImpl() {
        logger.debug("Searching for artist '{}' albums.", artist.getName());

        // Getting album list.
        List<Album> albumList = AlbumService.getInstance().getListOfAlbumsFiltered(artist, Collections.singletonList(AlbumType.STUDIO));

        logger.debug("Found {} artist '{}' albums.", albumList.size(), artist.getName());

        if (!isCancelled()) {
            // Invoking callback method.
            taskCallback.onSearchResult(albumList);
        }
    }

    @Override
    public void onProgressUpdate(List<Void> chunks) {
    }

    @Override
    public void onPostExecute() {
    }

    @Override
    public void onCancelled() {
    }
}
