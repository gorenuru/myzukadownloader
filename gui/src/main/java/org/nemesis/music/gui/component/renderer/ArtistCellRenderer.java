package org.nemesis.music.gui.component.renderer;

import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebListCellRenderer;
import org.nemesis.music.domain.Artist;

import javax.swing.*;
import java.awt.*;

/**
 * Class that is responsible for rendering the artist.
 *
 * User: malbul
 * Date: 12/4/12
 * Time: 5:17 PM
 */
public class ArtistCellRenderer extends WebListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        if (value != null) {
            WebLabel label = (WebLabel) component;
            Artist artist = (Artist) value;

            label.setMargin(2);
            label.setText(artist.getName());
        }

        return component;
    }
}
