package org.nemesis.music.gui.component.renderer;

import com.alee.laf.tree.WebTreeCellRenderer;
import com.alee.laf.tree.WebTreeElement;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that is responsible for rendering the objects from tree.
 *
 * User: malbul
 * Date: 12/5/12
 * Time: 4:50 PM
 */
public class TreeCellRenderer extends WebTreeCellRenderer {
    private static final String LOADING_TEXT = "Please wait, loading...";
    private static final String LOADING_ICON = "images/loading.png";

    private Map<String, WebTreeCellRenderer> map;

    private AlbumCellRenderer albumCellRenderer;
    private TrackCellRenderer trackCellRenderer;

    private ImageIcon imageIcon;


    /**
     * Constructor.
     */
    public TreeCellRenderer(AlbumCellRenderer albumCellRenderer, TrackCellRenderer trackCellRenderer) {
        this.albumCellRenderer = albumCellRenderer;
        this.trackCellRenderer = trackCellRenderer;

        init();
    }


    /**
     * Initializes renderer.
     */
    private void init() {
        map = new HashMap<String, WebTreeCellRenderer>();

        map.put(Album.class.getName(), albumCellRenderer);
        map.put(Track.class.getName(), trackCellRenderer);

        // Getting icon url from classpath.
        URL iconURL = TrackCellRenderer.class.getClassLoader().getResource(LOADING_ICON);

        if (iconURL != null) {
            // Loading icon.
            imageIcon = new ImageIcon(iconURL);
        }
    }

    @Override
    public WebTreeElement getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        WebTreeElement component = null;

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

        if (node.getUserObject() == null) {
            component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

            WebTreeElement webTreeElement = (WebTreeElement) component;
            webTreeElement.setText(LOADING_TEXT);
            webTreeElement.setIcon(imageIcon);
        } else {
            Object obj = node.getUserObject();

            WebTreeCellRenderer cellRenderer = map.get(obj.getClass().getName());
            if (cellRenderer != null) {
                component = cellRenderer.getTreeCellRendererComponent(tree, obj, sel, expanded, leaf, row, hasFocus);
            }
        }

        if (component == null) {
            component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }

        return component;
    }
}
