package org.nemesis.music.gui.task.callback;

import org.nemesis.music.domain.Artist;

import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 12/26/12
 * Time: 5:39 PM
 */
public interface SearchArtistTaskCallback {
    /**
     *
     *
     * @param artistList
     */
    public void onSearchResult(List<Artist> artistList);
}
