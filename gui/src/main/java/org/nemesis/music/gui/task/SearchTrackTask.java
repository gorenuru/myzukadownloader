package org.nemesis.music.gui.task;

import org.nemesis.music.core.service.TrackService;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;
import org.nemesis.music.gui.task.callback.SearchTrackTaskCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 12/7/12
 * Time: 10:49 AM
 */
public class SearchTrackTask extends AbstractTask<Void> {
    private static final Logger logger = LoggerFactory.getLogger(SearchTrackTask.class);

    private SearchTrackTaskCallback taskCallback;
    private Album album;


    /**
     * Constructor.
     */
    public SearchTrackTask(SearchTrackTaskCallback taskCallback, Album album) {
        this.taskCallback = taskCallback;
        this.album = album;
    }


    @Override
    public void doInBackgroundImpl() {
        logger.debug("Searching for album '{}' tracks.", album.getTitle());

        // Getting track list of album.
        List<Track> trackList = TrackService.getInstance().getAlbumTracks(album);

        logger.debug("Found {} album '{}' tracks.", trackList.size(), album.getTitle());

        if (!isCancelled()) {
            // Invoking callback method.
            taskCallback.onSearchResult(album, trackList);
        }
    }

    @Override
    public void onProgressUpdate(List<Void> chunks) {
    }

    @Override
    public void onPostExecute() {
    }

    @Override
    public void onCancelled() {
    }
}
