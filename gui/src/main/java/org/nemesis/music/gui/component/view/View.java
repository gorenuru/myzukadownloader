package org.nemesis.music.gui.component.view;

import java.awt.*;

/**
 * View interface, that contains main methods of view.
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 1:16 PM
 */
public interface View {
    /**
     * Initializes view.
     */
    public void init();

    /**
     * Initializes view style.
     */
    public void initStyle();

    /**
     * Initializes view components.
     */
    public void initComponents();

    /**
     * Adds components to content pane.
     */
    public void addComponents();

    /**
     * Resets view state.
     */
    public void reset();

    /**
     * Returns component content pane.
     *
     * @return Component content pane.
     */
    public Component getContentPane();

    /**
     * Adds the specified view action listener to receive action events from this component.
     *
     * @param listener - view action listener
     */
    public void addViewActionListener(ViewActionListener listener);

    /**
     * Removes the specified view action listener so that it no longer receives action events from this component.
     *
     * @param listener - view action listener
     */
    public void removeViewActionListener(ViewActionListener listener);
}
