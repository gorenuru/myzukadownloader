package org.nemesis.music.gui.task.callback;

import org.nemesis.music.domain.Album;

import java.util.List;

/**
 * User: malbul
 * Date: 12/28/12
 * Time: 10:42 AM
 */
public interface SearchAlbumTaskCallback {
    /**
     *
     *
     * @param albumList
     */
    public void onSearchResult(List<Album> albumList);
}
