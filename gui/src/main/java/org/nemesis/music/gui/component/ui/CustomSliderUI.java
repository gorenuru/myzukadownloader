package org.nemesis.music.gui.component.ui;

import com.alee.laf.slider.WebSliderUI;

import javax.swing.*;

/**
 * Custom slider UI.
 *
 * User: malbul
 * Date: 2/12/13
 * Time: 1:40 PM
 */
public class CustomSliderUI extends WebSliderUI {
    /**
     * Constructor.
     */
    public CustomSliderUI(JSlider jSlider) {
        super(jSlider);

        setTrackShadeWidth(0);
    }


    @Override
    protected void scrollDueToClickInTrack(int dir) {
        int value = slider.getValue();

        switch (slider.getOrientation()) {
            case JSlider.HORIZONTAL:
                value = valueForXPosition(slider.getMousePosition().x);
                break;

            case JSlider.VERTICAL:
                value = valueForYPosition(slider.getMousePosition().y);
                break;
        }

        if (value != slider.getValue()) {
            slider.setValue(value);
        }
    }

    @Override
    public void scrollByBlock(int direction) {
        // Disable scrolling when mouse pressed on the track.
    }
}
