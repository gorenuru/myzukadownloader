package org.nemesis.music.gui.task;

import javax.swing.*;
import java.util.List;

/**
 * Abstract class that allows to perform background operations and publish results on the UI thread without having to manipulate threads.
 *
 * User: malbul
 * Date: 12/4/12
 * Time: 4:29 PM
 */
public abstract class AbstractTask<Progress> extends SwingWorker<Void, Progress> {
    @Override
    protected Void doInBackground() {
        try {
            doInBackgroundImpl();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void process(List<Progress> chunks) {
        onProgressUpdate(chunks);
    }

    @Override
    protected void done() {
        onPostExecute();
    }

    /**
     * Attempts to cancel execution of this task.
     */
    public void cancel() {
        cancel(true);
        onCancelled();
    }


    /**
     * Computes a result.
     */
    public abstract void doInBackgroundImpl();

    /**
     * Runs on the UI thread after {@link #publish(Object[])} method is invoked.
     *
     * @param chunks - intermediate results to process
     */
    public abstract void onProgressUpdate(List<Progress> chunks);

    /**
     * Runs on the UI thread after {@link #doInBackground()} method.
     */
    public abstract void onPostExecute();

    /**
     * Runs on the UI thread after {@link #cancel()} method is invoked.
     */
    public abstract void onCancelled();
}
