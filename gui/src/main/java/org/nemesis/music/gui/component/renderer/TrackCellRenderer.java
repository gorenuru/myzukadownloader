package org.nemesis.music.gui.component.renderer;

import com.alee.laf.tree.WebTreeCellRenderer;
import com.alee.laf.tree.WebTreeElement;
import org.nemesis.music.domain.Track;
import org.nemesis.music.gui.utils.RendererUtils;
import org.nemesis.music.gui.utils.TreeNodeState;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that is responsible for rendering the track.
 *
 * User: malbul
 * Date: 12/5/12
 * Time: 4:49 PM
 */
public class TrackCellRenderer extends WebTreeCellRenderer {
    private Map<Track, TreeNodeState> trackStateMap;


    /**
     * Constructor.
     */
    public TrackCellRenderer() {
        super();

        trackStateMap = new HashMap<Track, TreeNodeState>();
    }


    @Override
    public WebTreeElement getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        WebTreeElement component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value != null) {
            Track track = (Track) value;

            String trackTitle = getFormattedTrackTitle(track);
            Color textColor = RendererUtils.getTextColorForState(trackStateMap.get(track));
            ImageIcon icon = RendererUtils.getIconForState(trackStateMap.get(track));

            component.setText(trackTitle);
            component.setForeground(textColor);

            if (icon != null) {
                component.setIcon(icon);
            }
        }

        return component;
    }

    /**
     * Sets track state.
     *
     * @param track - track
     * @param state - state of track
     */
    public void setTrackState(Track track, TreeNodeState state) {
        if (track != null && state != null) {
            trackStateMap.put(track, state);
        }
    }

    /**
     * Formats the track title.
     *
     * @param track - track
     * @return Formatted track title.
     */
    private String getFormattedTrackTitle(Track track) {
        String trackTitle = null;

        if (track.getTrackNo() > 0) {
            trackTitle = String.format("%d - %s (%s)", track.getTrackNo(), track.getTitle(), track.getDuration());
        } else {
            trackTitle = String.format("%s (%s)", track.getTitle(), track.getDuration());
        }

        return trackTitle;
    }
}
