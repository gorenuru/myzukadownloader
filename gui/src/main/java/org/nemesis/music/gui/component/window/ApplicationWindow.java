package org.nemesis.music.gui.component.window;

import com.alee.laf.splitpane.WebSplitPane;
import org.nemesis.music.gui.utils.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Main application window.
 *
 * User: malbul
 * Date: 12/25/12
 * Time: 5:45 PM
 */
public class ApplicationWindow extends JFrame {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationWindow.class);

    private static final String TITLE = "Music Downloader";
    private static final String APP_ICON = "images/application/";
    private static final String TRAY_ICON = "images/application/icon_16.png";

    private TrayIcon trayIcon;

    private WebSplitPane splitPane;


    /**
     * Constructor.
     */
    public ApplicationWindow() {
        init();
        initComponents();
        addComponents();
    }


    /**
     * Initializes application window.
     */
    private void init() {
        logger.debug("Initialization of the application window.");

        setTitle(TITLE);
        setSize(1000, 600);
        setIconImages(ImageUtils.loadImageList(APP_ICON));
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
    }

    /**
     * Initializes components of the window.
     */
    private void initComponents() {
        logger.debug("Initialization of the application window components.");

        // Creating tray popup items.
        MenuItem exitItem = new MenuItem("Exit");
        exitItem.addActionListener(exitActionListener);

        // Creating tray popup.
        PopupMenu trayPopup = new PopupMenu();
        trayPopup.add(exitItem);

        // Creating tray icon.
        trayIcon = new TrayIcon(ImageUtils.loadImageIcon(TRAY_ICON).getImage());
        trayIcon.setToolTip(TITLE);
        trayIcon.setImageAutoSize(false);
        trayIcon.setPopupMenu(trayPopup);
        trayIcon.addActionListener(showActionListener);

        // Creating split pane.
        splitPane = new WebSplitPane(WebSplitPane.HORIZONTAL_SPLIT);
        splitPane.setContinuousLayout(true);
        splitPane.setDividerLocation(270);
        splitPane.setDividerSize(3);
    }

    /**
     * Adds components to the window layout.
     */
    private void addComponents() {
        logger.debug("Adding components to application window layout.");

        add(splitPane, BorderLayout.CENTER);

        try {
            SystemTray.getSystemTray().add(trayIcon);
        } catch (AWTException e) {
            logger.error("Error while adding application icon to system tray.", e);
        }
    }

    /**
     * Sets view to the top of the application window.
     *
     * @param component - {@link Component}
     */
    public void setTopView(Component component) {
        add(component, BorderLayout.NORTH);
    }

    /**
     * Sets view to the bottom of the application window.
     *
     * @param component - {@link Component}
     */
    public void setBottomView(Component component) {
        add(component, BorderLayout.SOUTH);
    }

    /**
     * Sets view to the left of the application window.
     *
     * @param component - {@link Component}
     */
    public void setLeftView(Component component) {
        splitPane.setLeftComponent(component);
    }

    /**
     * Sets view to the right of the application window.
     *
     * @param component - {@link Component}
     */
    public void setRightView(Component component) {
        splitPane.setRightComponent(component);
    }


    /**
     * Show action listener.
     */
    private ActionListener showActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ApplicationWindow.this.setVisible(true);
        }
    };

    /**
     * Exit action listener.
     */
    private ActionListener exitActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };
}
