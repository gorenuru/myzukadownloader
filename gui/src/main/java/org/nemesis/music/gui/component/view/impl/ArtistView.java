package org.nemesis.music.gui.component.view.impl;

import com.alee.laf.list.WebList;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.gui.component.renderer.ArtistCellRenderer;
import org.nemesis.music.gui.component.view.View;
import org.nemesis.music.gui.component.view.ViewActionEvent;
import org.nemesis.music.gui.component.view.ViewActionListener;
import org.nemesis.music.gui.component.view.ViewActionListenerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

/**
 * User: malbul
 * Date: 12/28/12
 * Time: 2:18 PM
 */
public class ArtistView implements View, ListSelectionListener {
    private static final Logger logger = LoggerFactory.getLogger(ArtistView.class);

    private ViewActionListenerManager listenerManager;

    private WebPanel contentPane;

    private DefaultListModel model;
    private WebList list;
    private WebScrollPane listScrollPane;


    /**
     * Constructor.
     */
    public ArtistView() {
        listenerManager = new ViewActionListenerManager();

        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        logger.debug("Initialization of the artist view.");

        // Creating panel.
        contentPane = new WebPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setMinimumSize(new Dimension(6, 6));
    }

    @Override
    public void initStyle() {
        logger.debug("Initialization style of the artist view.");

        contentPane.setUndecorated(false);
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        logger.debug("Initialization of the artist view components.");

        // Creating list model.
        model = new DefaultListModel();

        // Creating list.
        list = new WebList(model);
        list.setOpaque(false);
        list.addListSelectionListener(this);
        list.setCellRenderer(new ArtistCellRenderer());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Creating list scroll pane.
        listScrollPane = new WebScrollPane(list);
        listScrollPane.setDrawBorder(false);
        listScrollPane.setDrawFocus(false);
    }

    @Override
    public void addComponents() {
        logger.debug("Adding components to the content pane.");

        contentPane.add(listScrollPane, BorderLayout.CENTER);
    }

    @Override
    public void reset() {
        logger.debug("Resetting artist view state.");

        // Clear artist list.
        model.clear();
        // Repainting list.
        list.invalidate();
    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }

    @Override
    public void addViewActionListener(ViewActionListener listener) {
        listenerManager.addViewActionListener(listener);
    }

    @Override
    public void removeViewActionListener(ViewActionListener listener) {
        listenerManager.removeViewActionListener(listener);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Object obj = list.getSelectedValue();

            if (obj != null) {
                Artist artist = (Artist) obj;

                // Notifying listeners.
                ViewActionEvent event = new ViewActionEvent(list, artist, ViewActionEvent.Type.SEARCH);
                listenerManager.fireViewActionEvent(event);
            }
        }
    }

    /**
     * Sets list of the artists to the artist view.
     *
     * @param artistList - list of the artists
     */
    public void setArtistList(java.util.List<Artist> artistList) {
        if (artistList != null) {
            // Updating list data.
            list.setListData(artistList.toArray());

            logger.debug("List of artists was changed.");
        }
    }
}
