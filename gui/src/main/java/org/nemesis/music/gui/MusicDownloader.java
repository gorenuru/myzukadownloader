package org.nemesis.music.gui;

import com.alee.laf.StyleConstants;
import com.alee.laf.WebLookAndFeel;
import org.nemesis.music.core.util.LoggerBootstrap;
import org.nemesis.music.gui.context.ApplicationContext;

import java.awt.*;

/**
 * Application launcher.
 *
 * User: malbul
 * Date: 12/25/12
 * Time: 5:39 PM
 */
public class MusicDownloader {
    public static void main(String[] args) {
        // Creating font that will be used for whole application.
        Font font = new Font("Tahoma", Font.PLAIN, 11);

        // Setting font.
        WebLookAndFeel.globalAcceleratorFont = font;
        WebLookAndFeel.globalEditorsFont = font;
        WebLookAndFeel.globalTextFont = font;

        // Disabling border and focus drawing.
        StyleConstants.drawBorder = false;
        StyleConstants.drawFocus = false;

        // Initialize look and feel.
        WebLookAndFeel.install();
        // Initialize the logger.
        LoggerBootstrap.init();

        // Showing application window.
        ApplicationContext.getInstance().showApplicationWindow();
    }
}
