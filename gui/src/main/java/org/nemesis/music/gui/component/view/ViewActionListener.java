package org.nemesis.music.gui.component.view;

/**
 * View action listener interface.
 *
 * User: malbul
 * Date: 12/28/12
 * Time: 1:26 PM
 */
public interface ViewActionListener {
    /**
     * Called when the action occurred in the view.
     *
     * @param event - view action event
     */
    public void onAction(ViewActionEvent event);
}
