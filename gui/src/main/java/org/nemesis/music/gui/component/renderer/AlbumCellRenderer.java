package org.nemesis.music.gui.component.renderer;

import com.alee.laf.tree.WebTreeCellRenderer;
import com.alee.laf.tree.WebTreeElement;
import org.apache.commons.lang3.StringUtils;
import org.nemesis.music.domain.Album;
import org.nemesis.music.gui.utils.TreeNodeState;
import org.nemesis.music.gui.utils.RendererUtils;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that is responsible for rendering the album.
 *
 * User: malbul
 * Date: 12/5/12
 * Time: 4:49 PM
 */
public class AlbumCellRenderer extends WebTreeCellRenderer {
    private Map<Album, TreeNodeState> albumStateMap;


    /**
     * Constructor.
     */
    public AlbumCellRenderer() {
        super();

        albumStateMap = new HashMap<Album, TreeNodeState>();
    }


    @Override
    public WebTreeElement getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        WebTreeElement component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value != null) {
            Album album = (Album) value;
            String albumTitle = getFormattedAlbumTitle(album);
            Color textColor = RendererUtils.getTextColorForState(albumStateMap.get(album));

            component.setText(albumTitle);
            component.setForeground(textColor);
        }

        return component;
    }

    /**
     * Sets album state.
     *
     * @param album - album
     * @param state - state of album
     */
    public void setAlbumState(Album album, TreeNodeState state) {
        if (album != null && state != null) {
            albumStateMap.put(album, state);
        }
    }

    /**
     * Formats the album title.
     *
     * @param album - album
     * @return Formatted album title.
     */
    private String getFormattedAlbumTitle(Album album) {
        String albumTitle = null;

        if (StringUtils.isNotEmpty(album.getReleaseDate())) {
            albumTitle = String.format("[%s] %s", album.getReleaseDate(), album.getTitle());
        } else {
            albumTitle = album.getTitle();
        }

        return albumTitle;
    }
}
