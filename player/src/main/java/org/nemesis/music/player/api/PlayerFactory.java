package org.nemesis.music.player.api;

import org.nemesis.music.player.impl.vlc.VLCPlayer;

/**
 * User: malbul
 * Date: 4/18/13
 * Time: 4:41 PM
 */
public class PlayerFactory {
    /**
     * Implementation of player.
     */
    private static final Player player = new VLCPlayer();


    /**
     * Returns player instance.
     *
     * @return Player instance.
     */
    public static Player getPlayer() {
        return player;
    }
}
