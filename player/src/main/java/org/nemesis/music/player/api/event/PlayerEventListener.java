package org.nemesis.music.player.api.event;

/**
 * User: malbul
 * Date: 4/24/13
 * Time: 9:14 AM
 */
public interface PlayerEventListener {
    public void onOpening();

    public void onPlaying();

    public void onPaused();

    public void onStopped();

    public void onFinished();

    public void onError();

    public void onPositionChanged(float newPosition);

    public void onTimeChanged(long newTime);
}
