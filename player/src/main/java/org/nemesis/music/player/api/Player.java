package org.nemesis.music.player.api;

import org.nemesis.music.player.api.event.PlayerEventListener;

import java.io.InputStream;
import java.net.URI;

/**
 * Interface that contains main methods for player implementation.
 *
 * User: malbul
 * Date: 4/18/13
 * Time: 4:40 PM
 */
public interface Player {
    /**
     * Starts track playback.
     *
     * @param trackURI - URI of the track
     * @param listener - listener, that will receive player events
     */
    public void play(URI trackURI, PlayerEventListener listener);

    /**
     * Starts track playback.
     *
     * @param inputStream - {@link InputStream} of the track
     * @param listener - listener, that will receive player events
     */
    public void play(InputStream inputStream, PlayerEventListener listener);

    /**
     * Pause track playback.
     */
    public void pause();

    /**
     * Stops track playback.
     */
    public void stop();

    /**
     * Jump to a specific position.
     *
     * @param position - position value, a percentage (e.g. 0.15 is 15%)
     */
    public void seek(float position);

    /**
     * Sets playback volume.
     *
     * @param level - volume level
     */
    public void setVolume(int level);

    /**
     * Returns level of the volume.
     *
     * @return Level of the volume.
     */
    public int getVolume();

    /**
     * Returns the playback flag.
     *
     * @return {@code true} if the player is playing, otherwise {@code false}.
     */
    public boolean isPlaying();

    /**
     * Returns the pause flag.
     *
     * @return {@code true} if the player is paused, otherwise {@code false}.
     */
    public boolean isPaused();
}
