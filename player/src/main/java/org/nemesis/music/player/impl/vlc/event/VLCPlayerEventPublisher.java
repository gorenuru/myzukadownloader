package org.nemesis.music.player.impl.vlc.event;

import org.nemesis.music.player.api.event.PlayerEventListener;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;

/**
 * User: malbul
 * Date: 4/24/13
 * Time: 9:14 AM
 */
public class VLCPlayerEventPublisher extends MediaPlayerEventAdapter {
    private PlayerEventListener playerEventListener;

    private MediaPlayer vlcPlayer;


    /**
     * Constructor.
     */
    public VLCPlayerEventPublisher(MediaPlayer vlcPlayer) {
        this.vlcPlayer = vlcPlayer;
    }


    /**
     * Sets player event listener.
     *
     * @param playerEventListener - {@link PlayerEventListener}
     */
    public void setPlayerEventListener(PlayerEventListener playerEventListener) {
        this.playerEventListener = playerEventListener;
    }


    @Override
    public void opening(MediaPlayer mediaPlayer) {
        if (playerEventListener != null) {
            playerEventListener.onOpening();
        }
    }

    @Override
    public void playing(MediaPlayer mediaPlayer) {
        if (playerEventListener != null) {
            playerEventListener.onPlaying();
        }
    }

    @Override
    public void paused(MediaPlayer mediaPlayer) {
        if (playerEventListener != null) {
            playerEventListener.onPaused();
        }
    }

    @Override
    public void stopped(MediaPlayer mediaPlayer) {
        if (playerEventListener != null) {
            playerEventListener.onStopped();
        }
    }

    @Override
    public void finished(MediaPlayer mediaPlayer) {
        if (playerEventListener != null) {
            playerEventListener.onFinished();
        }
    }

    @Override
    public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
        if (playerEventListener != null) {
            playerEventListener.onPositionChanged(newPosition);
        }
    }

    @Override
    public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
        if (playerEventListener != null) {
            playerEventListener.onTimeChanged(newTime);
        }
    }

    @Override
    public void error(MediaPlayer mediaPlayer) {
        if (playerEventListener != null) {
            playerEventListener.onError();

            // Stopping playback.
            vlcPlayer.stop();
        }
    }
}
