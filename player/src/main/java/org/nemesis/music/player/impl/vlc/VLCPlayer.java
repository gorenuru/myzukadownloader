package org.nemesis.music.player.impl.vlc;

import org.nemesis.music.player.api.Player;
import org.nemesis.music.player.api.event.PlayerEventListener;
import org.nemesis.music.player.impl.vlc.event.VLCPlayerEventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.caprica.vlcj.binding.internal.libvlc_state_t;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;

import java.io.InputStream;
import java.net.URI;

/**
 * Implementation of player using VLCj library.
 *
 * User: malbul
 * Date: 4/23/13
 * Time: 2:08 PM
 */
public class VLCPlayer implements Player {
    private static final Logger logger = LoggerFactory.getLogger(VLCPlayer.class);

    private NativeDiscovery nativeDiscovery;

    private MediaPlayer vlcPlayer;
    private VLCPlayerEventPublisher vlcPlayerEventPublisher;


    /**
     * Constructor.
     */
    public VLCPlayer() {
        init();
    }


    /**
     * Initializes VLCj library.
     */
    private void init() {
        try {
            // Creating native discovery.
            nativeDiscovery = new NativeDiscovery();
            // Discovering VLC player.
            nativeDiscovery.discover();
            // Creating headless media player.
            vlcPlayer = new MediaPlayerFactory().newHeadlessMediaPlayer();
            // Changing player volume to maximum.
            vlcPlayer.setVolume(100);
            // Creating player event publisher.
            vlcPlayerEventPublisher = new VLCPlayerEventPublisher(vlcPlayer);
            // Adding player event publisher to player.
            vlcPlayer.addMediaPlayerEventListener(vlcPlayerEventPublisher);
        } catch (Exception e) {
            logger.error("Initialization of VLCj is failed.\nThis happened for one of two reasons:\n\n1) VLC Player is not installed on this computer.\n2) VLCj is 32-bit and Java is 64-bit or vice versa.", e);
        }
    }


    @Override
    public void play(URI trackURI, PlayerEventListener listener) {
        if (trackURI == null) {
            throw new NullPointerException();
        }

        if (vlcPlayer != null) {
            // Registering player event listener.
            vlcPlayerEventPublisher.setPlayerEventListener(listener);
            // Starting track playback.
            vlcPlayer.playMedia(trackURI.toString());
        }
    }

    @Override
    public void play(InputStream inputStream, PlayerEventListener listener) {
        // Not implemented.
    }

    @Override
    public void pause() {
        if (vlcPlayer != null) {
            // Pausing track playback.
            vlcPlayer.setPause(true);
        }
    }

    @Override
    public void stop() {
        if (vlcPlayer != null) {
            // Stopping track playback.
            vlcPlayer.stop();
        }
    }

    @Override
    public void seek(float position) {
        if (vlcPlayer != null) {
            // Checking position.
            if (position > 0.99f) {
                position = 0.99f;
            }

            // Setting playback position.
            vlcPlayer.setPosition(position);
        }
    }

    @Override
    public void setVolume(int level) {
        if (vlcPlayer != null) {
            vlcPlayer.setVolume(level);
        }
    }

    @Override
    public int getVolume() {
        return vlcPlayer != null ? vlcPlayer.getVolume() : -1;
    }

    @Override
    public boolean isPlaying() {
        return vlcPlayer != null && vlcPlayer.getMediaPlayerState() == libvlc_state_t.libvlc_Playing;
    }

    @Override
    public boolean isPaused() {
        return vlcPlayer != null && vlcPlayer.getMediaPlayerState() == libvlc_state_t.libvlc_Paused;
    }
}
