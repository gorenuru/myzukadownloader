import com.xuggle.xuggler.*;

import javax.sound.sampled.*;

/**
 * User: malbul
 * Date: 4/18/13
 * Time: 6:16 PM
 */
public class XuggleTest {
    public static void main(String[] args) throws Exception {
        String trackURL = "http://myzuka.ru/Song/Download/121374?t=635019099765306098&s=d8643f89c2074517b85d240b4ef9376f";

        IContainer container = IContainer.make();

        container.open(trackURL, IContainer.Type.READ, null);

        int streams = container.getNumStreams();
        IStream audioStream = null;
        IStreamCoder audioCoder = null;

        for (int i = 0; i < streams; i++) {
            IStream stream = container.getStream(i);
            IStreamCoder streamCoder = stream.getStreamCoder();

            if (streamCoder.getCodecType() == ICodec.Type.CODEC_TYPE_AUDIO) {
                audioStream = stream;
                audioCoder = streamCoder;
                audioCoder.open(null, null);
                break;
            }
        }

        if (audioStream != null && audioCoder != null) {
            int sampleRate = audioCoder.getSampleRate();
            int sampleSize = (int) IAudioSamples.findSampleBitDepth(audioCoder.getSampleFormat());
            int channels = audioCoder.getChannels();

            AudioFormat audioFormat = new AudioFormat(sampleRate, sampleSize, channels, true, false);

            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
            try {
                SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);

                line.open(audioFormat);
                line.start();

                IPacket packet = IPacket.make();

                while (container.readNextPacket(packet) >= 0) {
                    if (packet.getStreamIndex() == audioStream.getIndex()) {
                        IAudioSamples samples = IAudioSamples.make(1024, audioCoder.getChannels());

                        int offset = 0;
                        while (offset < packet.getSize()) {
                            int bytesDecoded = audioCoder.decodeAudio(samples, packet, offset);

                            if (bytesDecoded < 0) {
                                throw new RuntimeException();
                            }

                            if (samples.isComplete()) {
                                byte[] rawBytes = samples.getData().getByteArray(0, samples.getSize());

                                line.write(rawBytes, 0, samples.getSize());
                            }

                            offset += bytesDecoded;
                        }
                    }
                }
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }
        }
    }
}
