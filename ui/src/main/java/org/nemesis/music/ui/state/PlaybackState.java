package org.nemesis.music.ui.state;

/**
 * User: malbul
 * Date: 4/19/13
 * Time: 11:47 AM
 */
public enum PlaybackState {
    OPENING,          // The track are buffering.
    PLAYING,            // The track are playing.
    PAUSED;             // The track is paused.
}
