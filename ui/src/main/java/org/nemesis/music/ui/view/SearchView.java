package org.nemesis.music.ui.view;

import com.alee.extended.image.WebImage;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.list.WebList;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextField;
import org.nemesis.music.domain.AlbumType;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.ui.renderer.ArtistCellRenderer;
import org.nemesis.music.ui.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Search view.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 1:56 PM
 */
public class SearchView implements View {
    private static final String ICON_SEARCH = "icons/search.png";

    private WebPanel contentPane;

    private WebImage fieldImage;
    private WebComboBox scraperTypeComboBox;
    private WebPopupMenu fieldPopup;
    private WebTextField field;

    private WebComboBox albumTypeComboBox;

    private DefaultListModel listModel;
    private WebScrollPane listScroll;
    private WebList list;

    private ArtistCellRenderer artistCellRenderer;


    /**
     * Constructor.
     */
    public SearchView() {
        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        // Creating content pane.
        contentPane = new WebPanel();
        contentPane.setMargin(2, 2, 2, 2);
        contentPane.setLayout(new BorderLayout());
    }

    @Override
    public void initStyle() {
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        // Creating filed image.
        fieldImage = new WebImage();
        fieldImage.setIcon(ImageUtils.loadImageIcon(ICON_SEARCH));

        // Creating scraper type combobox.
        scraperTypeComboBox = new WebComboBox(ScraperType.values());
        scraperTypeComboBox.setCursor(Cursor.getDefaultCursor());

        // Creating album type combobox.
        albumTypeComboBox = new WebComboBox(AlbumType.values());
        albumTypeComboBox.setDrawBorder(true);
        albumTypeComboBox.setDrawFocus(false);

        // Creating field popup.
        fieldPopup = new WebPopupMenu();
        fieldPopup.setLayout(new BorderLayout());

        // Creating field.
        field = new WebTextField();
        field.setLeadingComponent(fieldImage);
        field.setTrailingComponent(scraperTypeComboBox);
        field.setMargin(0, 2, 0, 2);
        field.setWebColored(true);
        field.setColumns(15);

        // Creating list model.
        listModel = new DefaultListModel();

        // Creating artist cell renderer.
        artistCellRenderer = new ArtistCellRenderer();

        // Creating list.
        list = new WebList(listModel);
        list.setOpaque(false);
        list.setCellRenderer(artistCellRenderer);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Creating list scroll.
        listScroll = new WebScrollPane(list);
        listScroll.setDrawBorder(false);
        listScroll.setDrawFocus(false);
    }

    @Override
    public void addComponents() {
        // Adding components to the field popup.
        fieldPopup.add(listScroll, BorderLayout.CENTER);

        // Adding components to the content pane.
        contentPane.add(field, BorderLayout.CENTER);
        contentPane.add(albumTypeComboBox, BorderLayout.EAST);
    }

    @Override
    public void reset() {

    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }

    /**
     * Shows field popup.
     */
    public void showFieldPopup() {
        Dimension preferredSize = new Dimension(field.getWidth(), 200);

        fieldPopup.setPreferredSize(preferredSize);
        fieldPopup.show(field, 0, field.getHeight());
    }


    /** ##################################### Getters ##################################### **/


    public WebImage getFieldImage() {
        return fieldImage;
    }

    public WebComboBox getScraperTypeComboBox() {
        return scraperTypeComboBox;
    }

    public WebComboBox getAlbumTypeComboBox() {
        return albumTypeComboBox;
    }

    public WebPopupMenu getFieldPopup() {
        return fieldPopup;
    }

    public WebTextField getField() {
        return field;
    }

    public DefaultListModel getListModel() {
        return listModel;
    }

    public WebScrollPane getListScroll() {
        return listScroll;
    }

    public WebList getList() {
        return list;
    }

    public ArtistCellRenderer getArtistCellRenderer() {
        return artistCellRenderer;
    }
}
