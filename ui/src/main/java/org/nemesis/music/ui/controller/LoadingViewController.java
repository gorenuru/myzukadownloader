package org.nemesis.music.ui.controller;

import org.nemesis.music.ui.service.MusicService;
import org.nemesis.music.ui.view.LoadingView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controller for the {@link LoadingView}.
 *
 * User: Eng
 * Date: 4/17/13
 * Time: 10:37 PM
 */
public class LoadingViewController {
    private static final Logger logger = LoggerFactory.getLogger(LoadingViewController.class);

    private static final LoadingViewController instance = new LoadingViewController();

    private LoadingView loadingView;

    private ButtonListener buttonListener;


    /**
     * Private constructr.
     */
    private LoadingViewController() {
        buttonListener = new ButtonListener();
    }


    /**
     * Returns singleton instance of loading view controller.
     *
     * @return Singleton instance of loading view controller.
     */
    public static LoadingViewController getInstance() {
        return instance;
    }


    /**
     * Installs controller to the search view.
     *
     * @param loadingView - {@link LoadingView}
     */
    public void install(LoadingView loadingView) {
        logger.debug("Installing controller to the: {}", loadingView);

        this.loadingView = loadingView;

        loadingView.getButton().addActionListener(buttonListener);
    }


    /**
     * Button listener.
     */
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            MusicService.getInstance().cancel();
        }
    }
}
