package org.nemesis.music.ui.view;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.progressbar.WebProgressBar;

import java.awt.*;

/**
 * Loading view.
 *
 * User: Eng
 * Date: 4/17/13
 * Time: 10:02 PM
 */
public class LoadingView implements View {
    private static final String LABEL_TEXT = "Please wait, loading...";

    private WebPanel contentPane;

    private WebLabel label;
    private WebProgressBar progressBar;
    private WebButton button;


    /**
     * Constructor.
     */
    public LoadingView() {
        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        // Creating content pane.
        contentPane = new WebPanel();
        contentPane.setLayout(new GridBagLayout());
        contentPane.setVisible(false);
    }

    @Override
    public void initStyle() {
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        // Creating label.
        label = new WebLabel();
        label.setText(LABEL_TEXT);
        label.setOpaque(false);

        // Creating progress bar.
        progressBar = new WebProgressBar();
        progressBar.setIndeterminate(true);

        // Creating button.
        button = new WebButton();
        button.setText("Cancel");
    }

    @Override
    public void addComponents() {
        // Creating components constraints.
        GridBagConstraints labelConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints progressBarConstraints = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints buttonConstraints = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);

        // Adding components to the content pane.
        contentPane.add(label, labelConstraints);
        contentPane.add(progressBar, progressBarConstraints);
        contentPane.add(button, buttonConstraints);
    }

    @Override
    public void reset() {

    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }


    /** ##################################### Getters ##################################### **/


    public WebLabel getLabel() {
        return label;
    }

    public WebProgressBar getProgressBar() {
        return progressBar;
    }

    public WebButton getButton() {
        return button;
    }
}
