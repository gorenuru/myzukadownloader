package org.nemesis.music.ui.task;

import org.nemesis.music.core.service.AlbumService;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.AlbumType;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.ui.callback.TaskCallback;
import org.nemesis.music.ui.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 4:37 PM
 */
public class SearchAlbumTask extends AbstractTask<List<Album>> {
    private static final Logger logger = LoggerFactory.getLogger(SearchAlbumTask.class);

    private Artist artist;


    /**
     * Constructor.
     */
    public SearchAlbumTask(Artist artist, TaskCallback<List<Album>> callback) {
        super(callback);

        this.artist = artist;
    }


    @Override
    protected List<Album> doInBackground() throws Exception {
        List<Album> albumList = null;

        try {
            // Receiving current album type.
            AlbumType albumType = ApplicationContext.getInstance().getAlbumType();

            logger.debug("Searching artist '{}' albums, using album type '{}'", artist.getName(), albumType);

            // Performing search for albums.
            albumList = AlbumService.getInstance().getListOfAlbumsFiltered(artist, Collections.singletonList(albumType));
        } catch (Exception e) {
            logger.error("An error occurred while task execution.", e);
            callback.onError(e);
        }

        return albumList;
    }

    @Override
    protected void done() {
        try {
            // Receiving result of task execution.
            List<Album> albumList = get();

            logger.debug("Result of task execution: {}", albumList);

            callback.onDone(albumList);
        } catch (Exception e) {
            logger.error("An error occurred while receiving result of task execution.", e);
            callback.onError(e);
        }
    }
}
