package org.nemesis.music.ui.callback;

/**
 * Adapter for task callback.
 *
 * User: malbul
 * Date: 4/18/13
 * Time: 12:44 PM
 */
public abstract class TaskCallbackAdapter<T> implements TaskCallback<T> {
    @Override
    public void onDone(T result) {
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(Exception e) {
    }
}
