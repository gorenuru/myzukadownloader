package org.nemesis.music.ui.task;

import org.nemesis.music.ui.callback.TaskCallback;

import javax.swing.*;

/**
 * Abstract task.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 3:22 PM
 */
public abstract class AbstractTask<T> extends SwingWorker<T, Void> {
    protected TaskCallback<T> callback;


    /**
     * Constructor.
     */
    public AbstractTask(TaskCallback<T> callback) {
        this.callback = callback;
    }


    /** ##################################### Getters ##################################### **/


    public TaskCallback<T> getCallback() {
        return callback;
    }
}
