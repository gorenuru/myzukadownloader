package org.nemesis.music.ui.controller;

import com.alee.laf.optionpane.WebOptionPane;
import org.apache.commons.lang3.StringUtils;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.AlbumType;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.ui.callback.TaskCallback;
import org.nemesis.music.ui.context.ApplicationContext;
import org.nemesis.music.ui.service.MusicService;
import org.nemesis.music.ui.view.SearchView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.util.List;

/**
 * Controller for the {@link SearchView}.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 3:16 PM
 */
public class SearchViewController {
    private static final Logger logger = LoggerFactory.getLogger(SearchViewController.class);

    private static final SearchViewController instance = new SearchViewController();

    private SearchView searchView;

    private ScraperTypeChangeListener scraperTypeChangeListener;
    private AlbumTypeChangeListener albumTypeChangeListener;
    private FieldActionListener fieldActionListener;
    private PopupListener popupListener;
    private ArtistSelectionListener artistSelectionListener;


    /**
     * Private constructor.
     */
    private SearchViewController() {
        scraperTypeChangeListener = new ScraperTypeChangeListener();
        albumTypeChangeListener = new AlbumTypeChangeListener();
        fieldActionListener = new FieldActionListener();
        popupListener = new PopupListener();
        artistSelectionListener = new ArtistSelectionListener();
    }


    /**
     * Returns singleton instance of search view controller.
     *
     * @return Singleton instance of search view controller.
     */
    public static SearchViewController getInstance() {
        return instance;
    }


    /**
     * Installs controller to the search view.
     *
     * @param searchView - {@link SearchView}
     */
    public void install(SearchView searchView) {
        logger.debug("Installing controller to the: {}", searchView);

        this.searchView = searchView;

        searchView.getScraperTypeComboBox().addItemListener(scraperTypeChangeListener);
        searchView.getAlbumTypeComboBox().addItemListener(albumTypeChangeListener);
        searchView.getField().addActionListener(fieldActionListener);
        searchView.getField().addKeyListener(popupListener);
        searchView.getList().addListSelectionListener(artistSelectionListener);
    }


    /**
     * Field action listener.
     */
    private class FieldActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String artistName = searchView.getField().getText();

            if (StringUtils.isEmpty(artistName)) {
                String message = "Please, enter Artist name!";
                WebOptionPane.showMessageDialog(ApplicationContext.getInstance().getApplicationWindow(), message, "Error!", WebOptionPane.ERROR_MESSAGE);
            } else {
                // Showing loading view.
                ApplicationContext.getInstance().setLoading(true);

                // Remove all artists from list.
                searchView.getListModel().removeAllElements();

                // Searching for Artists.
                MusicService.getInstance().getArtistsByKeyword(artistName, new TaskCallback<List<Artist>>() {
                    @Override
                    public void onDone(List<Artist> result) {
                        // Hiding loading view.
                        ApplicationContext.getInstance().setLoading(false);

                        if (result != null) {
                            for (Artist artist : result) {
                                // Adding artist to the popup list.
                                searchView.getListModel().addElement(artist);
                            }
                        }

                        // Showing popup.
                        searchView.showFieldPopup();
                    }

                    @Override
                    public void onCancel() {
                        // Hiding loading view.
                        ApplicationContext.getInstance().setLoading(false);
                    }

                    @Override
                    public void onError(Exception e) {
                        // Hiding loading view.
                        ApplicationContext.getInstance().setLoading(false);
                    }
                });
            }
        }
    }


    /**
     * Popup listener.
     */
    private class PopupListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            boolean isCtrlSpace = e.isControlDown() && e.getKeyCode() == KeyEvent.VK_SPACE;

            if (isCtrlSpace && searchView.getListModel().getSize() > 0) {
                searchView.showFieldPopup();
            }
        }
    }


    /**
     * Artist selection listener.
     */
    private class ArtistSelectionListener implements ListSelectionListener {
        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting()) {
                searchView.getFieldPopup().setVisible(false);

                Object obj = searchView.getList().getSelectedValue();

                if (obj != null) {
                    Artist artist = (Artist) obj;

                    // Changing Artist in the search field.
                    searchView.getField().setText(artist.getName());

                    // Showing loading view.
                    ApplicationContext.getInstance().setLoading(true);

                    // Searching for albums of selected Artist.
                    MusicService.getInstance().getListOfAlbums(artist, new TaskCallback<List<Album>>() {
                        @Override
                        public void onDone(List<Album> result) {
                            // Hiding loading view.
                            ApplicationContext.getInstance().setLoading(false);

                            AlbumViewController.getInstance().setAlbumList(result);
                        }

                        @Override
                        public void onCancel() {
                            // Hiding loading view.
                            ApplicationContext.getInstance().setLoading(false);
                        }

                        @Override
                        public void onError(Exception e) {
                            // Hiding loading view.
                            ApplicationContext.getInstance().setLoading(false);
                        }
                    });
                }

                searchView.getList().clearSelection();
            }
        }
    }


    /**
     * Scraper type change listener.
     */
    private class ScraperTypeChangeListener implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent e) {
            int state = e.getStateChange();

            if (state == ItemEvent.SELECTED) {
                Object obj = searchView.getScraperTypeComboBox().getSelectedItem();

                if (obj != null) {
                    ScraperType scraperType = (ScraperType) obj;
                    ApplicationContext.getInstance().setScraperType(scraperType);

                    logger.debug("Scraper type changed: {}", scraperType);
                }
            }
        }
    }


    /**
     * Album type change listener.
     */
    private class AlbumTypeChangeListener implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent e) {
            int state = e.getStateChange();

            if (state == ItemEvent.SELECTED) {
                Object obj = searchView.getAlbumTypeComboBox().getSelectedItem();

                if (obj != null) {
                    AlbumType albumType = (AlbumType) obj;
                    ApplicationContext.getInstance().setAlbumType(albumType);

                    logger.debug("Album type changed: {}", albumType);
                }
            }
        }
    }
}
