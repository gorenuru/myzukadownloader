package org.nemesis.music.ui.renderer;

import com.alee.laf.tree.WebTreeCellRenderer;
import com.alee.laf.tree.WebTreeElement;
import org.apache.commons.lang3.StringUtils;
import org.nemesis.music.domain.Album;
import org.nemesis.music.ui.state.DownloadState;
import org.nemesis.music.ui.utils.RendererUtils;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Album cell renderer.
 *
 * User: Eng
 * Date: 4/17/13
 * Time: 8:14 PM
 */
public class AlbumCellRenderer extends WebTreeCellRenderer {
    private Map<Album, DownloadState> albumDownloadStateMap;


    /**
     * Constructor.
     */
    public AlbumCellRenderer() {
        albumDownloadStateMap = new HashMap<Album, DownloadState>();
    }


    /**
     * Sets album download state.
     *
     * @param album - album
     * @param state - download state
     */
    public void setAlbumState(Album album, DownloadState state) {
        if (album != null) {
            if (state == null) {
                // Removing album from map.
                albumDownloadStateMap.remove(album);
            } else {
                // Changing album state.
                albumDownloadStateMap.put(album, state);
            }
        }
    }

    @Override
    public WebTreeElement getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        WebTreeElement component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value != null) {
            Album album = (Album) value;

            // Receiving album download state from map.
            DownloadState state = albumDownloadStateMap.get(album);

            // Formatting album title.
            String albumTitle = getFormattedAlbumTitle(album);

            if (state != null) {
                albumTitle = album.getArtist().getName() + " - " + albumTitle;
            }

            // Getting text color.
            Color textColor = RendererUtils.getTextColor(state);
            // Getting icon.
            ImageIcon icon = RendererUtils.getImageIcon(state);

            // Setting album title.
            component.setText(albumTitle);
            // Setting text color.
            component.setForeground(textColor);

            if (icon != null) {
                // Setting icon.
                component.setIcon(icon);
            }
        }

        return component;
    }

    /**
     * Formats the album title.
     *
     * @param album - album
     * @return Formatted album title.
     */
    private String getFormattedAlbumTitle(Album album) {
        String albumTitle = null;

        if (StringUtils.isNotEmpty(album.getReleaseDate())) {
            albumTitle = String.format("%s [%s]", album.getTitle(), album.getReleaseDate());
        } else {
            albumTitle = album.getTitle();
        }

        return albumTitle;
    }
}
