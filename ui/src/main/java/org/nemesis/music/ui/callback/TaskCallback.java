package org.nemesis.music.ui.callback;

/**
 * Callback for {@link org.nemesis.music.ui.task.AbstractTask}.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 3:27 PM
 */
public interface TaskCallback<T> {
    /**
     * Called when task was successfully executed.
     *
     * @param result - result of the task execution
     */
    public void onDone(T result);

    /**
     * Called when task was cancelled.
     */
    public void onCancel();

    /**
     * Called when task was executed with exception.
     *
     * <p>
     *     <strong>Note!</strong>
     *     This method called from the background thread.
     * </p>
     *
     * @param e - thrown exception
     */
    public void onError(Exception e);
}
