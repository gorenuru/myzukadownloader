package org.nemesis.music.ui.task;

import org.nemesis.music.core.service.TrackService;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;
import org.nemesis.music.ui.callback.TaskCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 *
 * User: Eng
 * Date: 4/17/13
 * Time: 9:18 PM
 */
public class SearchTrackTask extends AbstractTask<List<Track>> {
    private static final Logger logger = LoggerFactory.getLogger(SearchTrackTask.class);

    private Album album;


    /**
     * Constructor.
     */
    public SearchTrackTask(Album album, TaskCallback<List<Track>> callback) {
        super(callback);

        this.album = album;
    }


    @Override
    protected List<Track> doInBackground() throws Exception {
        List<Track> trackList = null;

        try {
            logger.debug("Searching album '{}' tracks.", album.getTitle());

            // Performing search for tracks.
            trackList = TrackService.getInstance().getAlbumTracks(album);
        } catch (Exception e) {
            logger.error("An error occurred while task execution.", e);
            callback.onError(e);
        }

        return trackList;
    }

    @Override
    protected void done() {
        try {
            // Receiving result of task execution.
            List<Track> trackList = get();

            logger.debug("Result of task execution: {}", trackList);

            callback.onDone(trackList);
        } catch (Exception e) {
            logger.error("An error occurred while receiving result of task execution.", e);
            callback.onError(e);
        }
    }
}
