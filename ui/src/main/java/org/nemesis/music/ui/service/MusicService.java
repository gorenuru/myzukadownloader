package org.nemesis.music.ui.service;

import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.domain.Track;
import org.nemesis.music.ui.callback.TaskCallback;
import org.nemesis.music.ui.task.AbstractTask;
import org.nemesis.music.ui.task.SearchAlbumTask;
import org.nemesis.music.ui.task.SearchArtistTask;
import org.nemesis.music.ui.task.SearchTrackTask;

import java.util.List;

/**
 * User: malbul
 * Date: 4/17/13
 * Time: 3:34 PM
 */
public class MusicService {
    private static final MusicService instance = new MusicService();

    private AbstractTask task;


    /**
     * Private constructor.
     */
    private MusicService() {

    }


    /**
     * Returns singleton instance of music service.
     *
     * @return Singleton instance of music service.
     */
    public static MusicService getInstance() {
        return instance;
    }


    /**
     * Retrieves list of Artists by keyword.
     *
     * @param artistName - artist name
     * @param callback - task callback
     */
    public void getArtistsByKeyword(String artistName, TaskCallback<List<Artist>> callback) {
        // Creating task.
        task = new SearchArtistTask(artistName, callback);
        // Executing task.
        task.execute();
    }

    /**
     * Retrieves list of albums by album type.
     *
     * @param artist - artist instance
     * @param callback - task callback
     */
    public void getListOfAlbums(Artist artist, TaskCallback<List<Album>> callback) {
        // Creating task.
        task = new SearchAlbumTask(artist, callback);
        // Executing task.
        task.execute();
    }

    /**
     * Retrieves list of tracks for specified album.
     *
     * @param album - album instance
     * @param callback - task callback
     */
    public void getAlbumTracks(Album album, TaskCallback<List<Track>> callback) {
        // Creating task.
        task = new SearchTrackTask(album, callback);
        // Executing task.
        task.execute();
    }

    /**
     * Cancels current task execution.
     */
    public void cancel() {
        if (task != null && (!task.isCancelled() || !task.isDone())) {
            // Invoking callback.
            task.getCallback().onCancel();
            // Canceling task execution.
            task.cancel(true);
        }
    }
}
