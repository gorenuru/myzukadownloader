package org.nemesis.music.ui.utils;

import com.alee.laf.tree.WebTreeUI;
import org.nemesis.music.ui.state.DownloadState;
import org.nemesis.music.ui.state.PlaybackState;

import javax.swing.*;
import java.awt.*;

/**
 *
 *
 * User: malbul
 * Date: 4/18/13
 * Time: 2:20 PM
 */
public class RendererUtils {
    public static Color TEXT_COLOR_NORMAL = Color.decode("#000000");
    public static Color TEXT_COLOR_QUEUE = Color.decode("#8A2BE2");
    public static Color TEXT_COLOR_IN_PROGRESS = Color.decode("#4169E1");
    public static Color TEXT_COLOR_DONE = Color.decode("#3CB371");
    public static Color TEXT_COLOR_ERROR = Color.decode("#DC143C");

    public static ImageIcon ICON_IN_PROGRESS = ImageUtils.loadImageIcon("icons/state/loading.png");
    public static ImageIcon ICON_DONE = ImageUtils.loadImageIcon("icons/state/done.png");
    public static ImageIcon ICON_ERROR = ImageUtils.loadImageIcon("icons/state/error.png");

    public static ImageIcon ICON_OPEN = ImageUtils.loadImageIcon("icons/state/loading.png");
    public static ImageIcon ICON_PLAY = ImageUtils.loadImageIcon("icons/state/play.png");
    public static ImageIcon ICON_PAUSE = ImageUtils.loadImageIcon("icons/state/pause.png");


    /**
     * Returns text color for {@link DownloadState}.
     *
     * @param state - {@link DownloadState}
     * @return Text colro for {@link DownloadState}.
     */
    public static Color getTextColor(DownloadState state) {
        Color textColor = TEXT_COLOR_NORMAL;

        if (state != null) {
            switch (state) {
                case QUEUE:
                    textColor = TEXT_COLOR_QUEUE;
                    break;
                case IN_PROGRESS:
                    textColor = TEXT_COLOR_IN_PROGRESS;
                    break;
                case DONE:
                    textColor = TEXT_COLOR_DONE;
                    break;
                case ERROR:
                    textColor = TEXT_COLOR_ERROR;
                    break;
            }
        }

        return textColor;
    }

    /**
     * Returns image icon for {@link DownloadState}.
     *
     * @param state - {@link DownloadState}
     * @return Image icon for {@link DownloadState}.
     */
    public static ImageIcon getImageIcon(DownloadState state) {
        ImageIcon imageIcon = null;

        if (state != null) {
            switch (state) {
                case IN_PROGRESS:
                    imageIcon = ICON_IN_PROGRESS;
                    break;
                case DONE:
                    imageIcon = ICON_DONE;
                    break;
                case ERROR:
                    imageIcon = ICON_ERROR;
                    break;
            }
        }

        return imageIcon;
    }

    /**
     * Returns image icon for {@link PlaybackState}.
     *
     * @param state - {@link PlaybackState}
     * @return Image icon for {@link PlaybackState}.
     */
    public static ImageIcon getImageIcon(PlaybackState state) {
        ImageIcon imageIcon = WebTreeUI.LEAF_ICON;

        if (state != null) {
            switch (state) {
                case OPENING:
                    imageIcon = ICON_OPEN;
                    break;
                case PLAYING:
                    imageIcon = ICON_PLAY;
                    break;
                case PAUSED:
                    imageIcon = ICON_PAUSE;
                    break;
            }
        }

        return imageIcon;
    }
}
