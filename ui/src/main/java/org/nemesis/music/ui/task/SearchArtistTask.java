package org.nemesis.music.ui.task;

import org.nemesis.music.core.service.ArtistService;
import org.nemesis.music.domain.Artist;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.ui.callback.TaskCallback;
import org.nemesis.music.ui.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 3:31 PM
 */
public class SearchArtistTask extends AbstractTask<List<Artist>> {
    private static final Logger logger = LoggerFactory.getLogger(SearchArtistTask.class);

    private String artistName;


    /**
     * Constructor.
     */
    public SearchArtistTask(String artistName, TaskCallback<List<Artist>> callback) {
        super(callback);

        this.artistName = artistName;
    }


    @Override
    protected List<Artist> doInBackground() {
        List<Artist> artistList = null;

        try {
            // Receiving current scraper type.
            ScraperType scraperType = ApplicationContext.getInstance().getScraperType();

            logger.debug("Searching for artist '{}', using scraper '{}'", artistName, scraperType);

            // Performing search for artist.
            artistList = ArtistService.getInstance().getArtistsByKeyword(artistName, scraperType);
        } catch (Exception e) {
            logger.error("An error occurred while task execution.", e);
            callback.onError(e);
        }

        return artistList;
    }

    @Override
    protected void done() {
        try {
            // Receiving result of task execution.
            List<Artist> artistList = get();

            logger.debug("Result of task execution: {}", artistList);

            callback.onDone(artistList);
        } catch (Exception e) {
            logger.error("An error occurred while receiving result of task execution.", e);
            callback.onError(e);
        }
    }
}
