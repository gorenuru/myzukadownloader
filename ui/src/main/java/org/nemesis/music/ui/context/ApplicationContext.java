package org.nemesis.music.ui.context;

import org.nemesis.music.domain.AlbumType;
import org.nemesis.music.domain.ScraperType;
import org.nemesis.music.ui.controller.*;
import org.nemesis.music.ui.view.*;
import org.nemesis.music.ui.window.ApplicationWindow;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Application context that contains all instances of application objects.
 *
 * User: malbul
 * Date: 4/11/13
 * Time: 12:10 PM
 */
public class ApplicationContext {
    private static ApplicationContext instance = new ApplicationContext();

    private ScraperType scraperType = ScraperType.MYZUKA;
    private AlbumType albumType = AlbumType.STUDIO;

    private ApplicationWindow applicationWindow;

    private PlayerView playerView;
    private SearchView searchView;
    private AlbumView albumView;
    private DownloadView downloadView;
    private LoadingView loadingView;


    /**
     * Private constructor.
     */
    private ApplicationContext() {
        init();
    }


    /**
     * Returns singleton instance of application context.
     *
     * @return Singleton instance of application context.
     */
    public static ApplicationContext getInstance() {
        return instance;
    }


    /**
     * Initializes application context.
     */
    private void init() {
        // Creating views.
        playerView = new PlayerView();
        searchView = new SearchView();
        albumView = new AlbumView();
        loadingView = new LoadingView();
        downloadView = new DownloadView();

        // Installing controllers to the views.
        PlayerViewController.getInstance().install(playerView);
        SearchViewController.getInstance().install(searchView);
        AlbumViewController.getInstance().install(albumView);
        DownloadViewController.getInstance().install(downloadView);
        LoadingViewController.getInstance().install(loadingView);

        // Creating application window.
        applicationWindow = new ApplicationWindow(loadingView.getContentPane(), downloadView.getContentPane());

        // Adding views to the application window.
        applicationWindow.addToLeftPanel(playerView.getContentPane(), BorderLayout.CENTER);
        applicationWindow.addToRightPanel(searchView.getContentPane(), BorderLayout.NORTH);
        applicationWindow.addToRightPanel(albumView.getContentPane(), BorderLayout.CENTER);

        // Registering global key listener.
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
                boolean result = false;

                if (e.getID() == KeyEvent.KEY_RELEASED) {
                    // Checking for Ctrl + J.
                    if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_J) {
                        boolean visible = downloadView.getContentPane().isVisible();

                        downloadView.getContentPane().setVisible(!visible);
                        setEnabled(applicationWindow.getRightPanel(), visible);

                        result = true;
                    }
                }

                return result;
            }
        });
    }

    /**
     * Enables/Disables container components.
     *
     * @param container - {@link java.awt.Container}
     * @param value - {@code true} - enable, {@code false} - disable
     */
    private void setEnabled(Container container, boolean value) {
        Component[] components = container.getComponents();

        for (Component component : components) {
            component.setEnabled(value);

            if (Container.class.isInstance(component)) {
                setEnabled((Container) component, value);
            }
        }
    }


    /**
     * Starts application.
     */
    public void startApplication() {
        // Showing application window.
        applicationWindow.setVisible(true);
        // Requesting focus for the field.
        searchView.getField().requestFocus();
    }

    /**
     * Shows/hides loading dialog.
     *
     * @param value - {@code true} - show, {@code false} - hide
     */
    public void setLoading(boolean value) {
        loadingView.getContentPane().setVisible(value);
        setEnabled(applicationWindow.getRightPanel(), !value);
    }

    /**
     * Returns current scraper type.
     *
     * @return Current scraper type.
     */
    public ScraperType getScraperType() {
        return scraperType;
    }

    /**
     * Sets scraper type.
     *
     * @param scraperType - {@link ScraperType}
     */
    public void setScraperType(ScraperType scraperType) {
        this.scraperType = scraperType;
    }

    /**
     * Returns current album type.
     *
     * @return Current album type.
     */
    public AlbumType getAlbumType() {
        return albumType;
    }

    /**
     * Sets album type.
     *
     * @param albumType - {@link AlbumType}
     */
    public void setAlbumType(AlbumType albumType) {
        this.albumType = albumType;
    }

    /**
     * Returns application window.
     *
     * @return {@link ApplicationWindow}.
     */
    public ApplicationWindow getApplicationWindow() {
        return applicationWindow;
    }

    /**
     * Returns player view.
     *
     * @return {@link PlayerView}.
     */
    public PlayerView getPlayerView() {
        return playerView;
    }

    /**
     * Returns search view.
     *
     * @return {@link SearchView}.
     */
    public SearchView getSearchView() {
        return searchView;
    }

    /**
     * Returns album view.
     *
     * @return {@link AlbumView}.
     */
    public AlbumView getAlbumView() {
        return albumView;
    }
}
