package org.nemesis.music.ui.controller;

import com.alee.managers.tooltip.TooltipManager;
import com.alee.managers.tooltip.WebCustomTooltip;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;
import org.nemesis.music.ui.callback.TaskCallbackAdapter;
import org.nemesis.music.ui.context.ApplicationContext;
import org.nemesis.music.ui.service.MusicService;
import org.nemesis.music.ui.view.AlbumView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for the {@link AlbumView}.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 5:03 PM
 */
public class AlbumViewController {
    private static final Logger logger = LoggerFactory.getLogger(AlbumViewController.class);

    private static final AlbumViewController instance = new AlbumViewController();

    private Map<String, DefaultMutableTreeNode> albumMapping;

    private AlbumView albumView;

    private AlbumExpandListener albumExpandListener;
    private TreeMouseListener treeMouseListener;

    private AlbumAddToPlayListListener albumAddToPlayListListener;
    private AlbumDownloadListener albumDownloadListener;

    private TrackAddToPlaylistListeenr trackAddToPlaylistListeenr;


    /**
     * Private constructor.
     */
    private AlbumViewController() {
        albumMapping = new HashMap<String, DefaultMutableTreeNode>();

        albumExpandListener = new AlbumExpandListener();
        treeMouseListener = new TreeMouseListener();

        albumAddToPlayListListener = new AlbumAddToPlayListListener();
        albumDownloadListener = new AlbumDownloadListener();

        trackAddToPlaylistListeenr = new TrackAddToPlaylistListeenr();
    }


    /**
     * Returns singleton instance of album view controller.
     *
     * @return Singleton instance of album view controller.
     */
    public static AlbumViewController getInstance() {
        return instance;
    }


    /**
     * Installs controller to the album view.
     *
     * @param albumView - {@link AlbumView}
     */
    public void install(AlbumView albumView) {
        logger.debug("Installing controller to the: {}", albumView);

        this.albumView = albumView;

        albumView.getTree().addTreeWillExpandListener(albumExpandListener);
        albumView.getTree().addMouseListener(treeMouseListener);

        albumView.getAlbumAddToPlaylistItem().addActionListener(albumAddToPlayListListener);
        albumView.getAlbumDownloadItem().addActionListener(albumDownloadListener);

        albumView.getTrackAddToPlaylistItem().addActionListener(trackAddToPlaylistListeenr);
    }

    /**
     * Sets list of artist albums to tree.
     *
     * @param albumList - list of artist albums
     */
    public void setAlbumList(List<Album> albumList) {
        // Receiving root node.
        DefaultMutableTreeNode rootNode = albumView.getRootNode();

        // Clear album mapping.
        albumMapping.clear();
        // Remove all albums from tree.
        rootNode.removeAllChildren();

        if (albumList != null) {
            // Adding albums to album root node.
            for (Album album : albumList) {
                DefaultMutableTreeNode albumNode = new DefaultMutableTreeNode(album);
                DefaultMutableTreeNode emptyNode = new DefaultMutableTreeNode();

                albumNode.add(emptyNode);
                rootNode.add(albumNode);

                albumMapping.put(album.getId(), albumNode);
            }

            // Reloading model.
            albumView.getTreeModel().reload();
            // Expanding root node.
            albumView.getTree().expandPath(new TreePath(rootNode.getPath()));
        }
    }

    /**
     * Sets list of album tracks to view.
     *
     * @param trackList - list of album tracks
     * @param expand - {@code true}, if needs to expand album node after adding tracks, otherwise - {@code false}.
     */
    public void setTrackList(Album album, List<Track> trackList, boolean expand) {
        if (trackList != null && trackList.size() > 0) {
            // Reciving album node from album mapping.
            DefaultMutableTreeNode albumNode = albumMapping.get(album.getId());

            if (albumNode != null) {
                // Removing all tracks from album node.
                albumNode.removeAllChildren();

                for (Track track : trackList) {
                    // Creating track node.
                    DefaultMutableTreeNode trackNode = new DefaultMutableTreeNode(track);
                    // Disabling child nodes.
                    trackNode.setAllowsChildren(false);
                    // Adding track node to album node.
                    albumNode.add(trackNode);
                }

                // Reloading album node.
                albumView.getTreeModel().reload(albumNode);

                if (expand) {
                    // Expanding album node.
                    albumView.getTree().expandPath(new TreePath(albumNode.getPath()));
                }
            }
        }
    }


    /**
     * Returns selected album node.
     *
     * @return Selected album node.
     */
    public DefaultMutableTreeNode getSelectedAlbumNode() {
        DefaultMutableTreeNode albumNode = null;

        // Getting selected node path.
        TreePath nodePath = albumView.getTree().getSelectionPath();

        if (nodePath != null) {
            // Getting album node from selected path.
            albumNode = (DefaultMutableTreeNode) nodePath.getLastPathComponent();
        }


        return albumNode;
    }


    /**
     * Album expand listener.
     */
    private class AlbumExpandListener implements TreeWillExpandListener {
        @Override
        public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
            // Getting selected album node.
            DefaultMutableTreeNode albumNode = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();

            if (albumNode != null && albumNode.getChildCount() == 1 && albumNode.getAllowsChildren()) {
                // Getting album from album node.
                final Album album = (Album) albumNode.getUserObject();

                if (album != null) {
                    // Searching for tracks of selected album.
                    MusicService.getInstance().getAlbumTracks(album, new TaskCallbackAdapter<List<Track>>() {
                        @Override
                        public void onDone(List<Track> result) {
                            // Hiding loading view.
                            ApplicationContext.getInstance().setLoading(false);

                            setTrackList(album, result, true);
                        }
                    });
                }
            }
        }

        @Override
        public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
            // Not implemented.
        }
    }


    /**
     * Tree mouse listener.
     */
    private class TreeMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            // Getting node path where mouse was clicked.
            TreePath nodePath = albumView.getTree().getPathForLocation(e.getX(), e.getY());

            if (nodePath != null) {
                // Getting tree node from this path.
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) nodePath.getLastPathComponent();

                if (treeNode.getUserObject() instanceof Album) {
                    // Processing mouse click on album node.
                    processAlbumMouseClick(nodePath, e);
                } else {
                    // Processing mouse click on track node.
                    processTrackMouseClick(nodePath, e);
                }
            }
        }

        /**
         * Processes mouse click on album node.
         *
         * @param e - mouse event
         * @param nodePath - node path
         */
        private void processAlbumMouseClick(TreePath nodePath, MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e)) {
                // Selecting node where mouse was clicked.
                albumView.getTree().setSelectionPath(nodePath);
                // Processing right mouse click on album node.
                albumView.getAlbumPopupMenu().show(albumView.getTree(), e.getX(), e.getY());
            }
        }

        /**
         * Processes mouse click on track node.
         *
         * @param e - mouse event
         * @param nodePath - node path
         */
        private void processTrackMouseClick(TreePath nodePath, MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                // Processing left mouse double click on track node.
                trackAddToPlaylistListeenr.actionPerformed(null);
            } else
            if (SwingUtilities.isRightMouseButton(e)) {
                // Getting selection paths.
                List selectedNodes = albumView.getTree().getSelectedNodes();

                if (selectedNodes != null && !selectedNodes.contains(nodePath.getLastPathComponent())) {
                    // Selecting node where mouse was clicked.
                    albumView.getTree().setSelectionPath(nodePath);
                }

                // Processing right mouse click on track node.
                albumView.getTrackPopupMenu().show(albumView.getTree(), e.getX(), e.getY());
            }
        }
    }


    /**
     * Album add to playlist listener.
     */
    private class AlbumAddToPlayListListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Getting album node.
            final DefaultMutableTreeNode albumNode = getSelectedAlbumNode();

            if (albumNode != null) {
                // Getting album from album node.
                final Album album = (Album) albumNode.getUserObject();

                if (album != null) {
                    // Receiving list of tracks for album.
                    MusicService.getInstance().getAlbumTracks(album, new TaskCallbackAdapter<List<Track>>() {
                        @Override
                        public void onDone(List<Track> result) {
                            if (albumNode.getChildCount() < 2) {
                                // Setting list of tracks to album node.
                                setTrackList(album, result, false);
                            }

                            // Adding list of tracks to playlist.
                            PlayerViewController.getInstance().addToPlayList(result);
                        }
                    });
                }
            }
        }
    }


    /**
     * Album download listener.
     */
    private class AlbumDownloadListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Getting album node.
            DefaultMutableTreeNode albumNode = getSelectedAlbumNode();

            if (albumNode != null) {
                // Getting album from album node.
                Album album = (Album) albumNode.getUserObject();

                if (album != null) {
                    // Adding album to the download queue.
                    DownloadViewController.getInstance().downloadAlbum(album);
                    // Creating custom tooltip.
                    WebCustomTooltip customTooltip = new WebCustomTooltip(albumView.getContentPane(), "Press 'Ctrl + J' to see download screen.");
                    // Calculating tooltip location.
                    int x = albumView.getContentPane().getWidth() / 2;
                    int y = albumView.getContentPane().getHeight();
                    // Setting tooltip location.
                    customTooltip.setDisplayLocation(x, y);
                    // Showing tooltip.
                    TooltipManager.showOneTimeTooltip(customTooltip);
                }
            }
        }
    }


    /**
     * Track add to playlist listener.
     */
    private class TrackAddToPlaylistListeenr implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Getting selected nodes.
            List selectedNodes = albumView.getTree().getSelectedNodes();

            if (selectedNodes != null) {
                for (Object node : selectedNodes) {
                    // Getting track node from selected nodes.
                    DefaultMutableTreeNode trackNode = (DefaultMutableTreeNode) node;
                    // Getting track from track node.
                    Track track = (Track) trackNode.getUserObject();

                    if (track != null) {
                        // Adding track to playlist.
                        PlayerViewController.getInstance().addToPlayList(track);
                    }
                }
            }
        }
    }
}
