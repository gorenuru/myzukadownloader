package org.nemesis.music.ui.window;

import com.alee.extended.panel.WebOverlay;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.splitpane.WebSplitPane;
import org.nemesis.music.ui.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;

/**
 * User: malbul
 * Date: 4/17/13
 * Time: 2:57 PM
 */
public class ApplicationWindow extends JFrame {
    private static final String TITLE = "Music Downloader";

    private static final String ICON_APP = "icons/app/";
//    private static final String ICON_TRAY = "icons/app/icon_24.png";

    private WebSplitPane contentPane;

    private WebOverlay rightInnerPanelOverlay;
    private Component rightInnerPanelLoadingOverlayComponent;
    private Component rigthInnerPanelDownloadOverlayComponent;

    private WebPanel leftPanel;
    private WebPanel rightPanel;


    /**
     * Constructor.
     */
    public ApplicationWindow(Component rightInnerPanelLoadingOverlayComponent, Component rigthInnerPanelDownloadOverlayComponent) {
        this.rightInnerPanelLoadingOverlayComponent = rightInnerPanelLoadingOverlayComponent;
        this.rigthInnerPanelDownloadOverlayComponent = rigthInnerPanelDownloadOverlayComponent;

        init();
        initComponents();
        addComponents();
    }


    /**
     * Initializes application window.
     */
    private void init() {
        setTitle(TITLE);
        setSize(1000, 600);
        setMinimumSize(new Dimension(800, 400));
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        setIconImages(ImageUtils.loadImageList(ICON_APP));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Initializes components of the window.
     */
    private void initComponents() {
        // Creating content pane.
        contentPane = new WebSplitPane();
        contentPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        contentPane.setContinuousLayout(true);
        contentPane.setDividerLocation(270);
        contentPane.setDividerSize(2);

        // Creating left panel.
        leftPanel = new WebPanel();
        leftPanel.setUndecorated(false);
        leftPanel.setLayout(new BorderLayout());

        // Creating right inner panel.
        rightPanel = new WebPanel();
        rightPanel.setUndecorated(false);
        rightPanel.setLayout(new BorderLayout());

        // Creating loading overlay.
        rightInnerPanelOverlay = new WebOverlay();
        rightInnerPanelOverlay.setDrawBackground(true);
        rightInnerPanelOverlay.addOverlay(rightInnerPanelLoadingOverlayComponent);
        rightInnerPanelOverlay.addOverlay(rigthInnerPanelDownloadOverlayComponent);
        rightInnerPanelOverlay.setComponent(rightPanel);
    }

    /**
     * Adds components to the window layout.
     */
    private void addComponents() {
        contentPane.setLeftComponent(leftPanel);
        contentPane.setRightComponent(rightInnerPanelOverlay);

        add(contentPane, BorderLayout.CENTER);
    }


    /**
     * Adds component to the left panel.
     *
     * @param component - the component to be added
     * @param constraints - an object expressing layout contraints for this component
     */
    public void addToLeftPanel(Component component, Object constraints) {
        leftPanel.add(component, constraints);
    }

    /**
     * Adds component to the right panel.
     *
     * @param component - the component to be added
     * @param constraints - an object expressing layout contraints for this component
     */
    public void addToRightPanel(Component component, Object constraints) {
        rightPanel.add(component, constraints);
    }


    /** ##################################### Getters ##################################### **/


    public WebOverlay getRightInnerPanelOverlay() {
        return rightInnerPanelOverlay;
    }

    public Component getRightInnerPanelLoadingOverlayComponent() {
        return rightInnerPanelLoadingOverlayComponent;
    }

    public WebPanel getLeftPanel() {
        return leftPanel;
    }

    public WebPanel getRightPanel() {
        return rightPanel;
    }
}
