package org.nemesis.music.ui.component;

import com.alee.laf.label.WebLabel;
import com.alee.laf.label.WebLabelUI;

import javax.swing.*;
import java.awt.*;

/**
 * User: malbul
 * Date: 4/23/13
 * Time: 4:20 PM
 */
public class AutoWidthLabelUI extends WebLabelUI {
    private Container container;
    private WebLabel label;


    /**
     * Constructor.
     */
    public AutoWidthLabelUI(Container container, WebLabel label) {
        super();

        this.container = container;
        this.label = label;
    }


    @Override
    public Dimension getPreferredSize(JComponent component) {
        Dimension size = super.getPreferredSize(component);

        if (container != null && label != null) {
            // Receiving parent width.
            int parentWidth = container.getWidth();
            // Calculating label text width.
            int textWidth = label.getFontMetrics(label.getFont()).stringWidth(label.getText());

            if (parentWidth > 0 && parentWidth < textWidth) {
                // Calculating label margin.
                int margin = label.getMargin().left + label.getMargin().right;
                // Calculating label insets.
                int insets = label.getInsets().left + label.getInsets().right;

                // Setting label width.
                size.width = parentWidth - (margin + insets) - 20;
            }
        }

        return size;
    }
}
