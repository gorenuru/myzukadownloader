package org.nemesis.music.ui.controller;

import com.alee.laf.optionpane.WebOptionPane;
import org.nemesis.music.core.service.DownloadLinkService;
import org.nemesis.music.domain.Track;
import org.nemesis.music.player.api.Player;
import org.nemesis.music.player.api.PlayerFactory;
import org.nemesis.music.player.api.event.PlayerEventListener;
import org.nemesis.music.ui.context.ApplicationContext;
import org.nemesis.music.ui.state.PlaybackState;
import org.nemesis.music.ui.view.PlayerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Controller for the {@link PlayerView}.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 5:01 PM
 */
public class PlayerViewController {
    private static final Logger logger = LoggerFactory.getLogger(PlayerViewController.class);

    private static final PlayerViewController instance = new PlayerViewController();

    private Player player;

    private URI currentTrackURI;
    private int currentTrackIndex;
    private float currentTrackPosition;

    private PlayerView playerView;

    private SeekBarMouseListener seekBarMouseListener;
    private PlayerEventConsumer playerEventConsumer;
    private PlayButtonListener playButtonListener;
    private PauseButtonListener pauseButtonListener;
    private NextButtonListener nextButtonListener;
    private BackButtonListener backButtonListener;
    private PlaylistMouseListener playlistMouseListener;
    private PlaylistPopupMenuListener playlistPopupMenuListener;
    private RemoveItemListener removeItemListener;
    private RemoveAllItemListener removeAllItemListener;


    /**
     * Private constructor.
     */
    private PlayerViewController() {
        player = PlayerFactory.getPlayer();

        seekBarMouseListener = new SeekBarMouseListener();
        playerEventConsumer = new PlayerEventConsumer();
        playButtonListener = new PlayButtonListener();
        pauseButtonListener = new PauseButtonListener();
        nextButtonListener = new NextButtonListener();
        backButtonListener = new BackButtonListener();
        playlistMouseListener = new PlaylistMouseListener();
        playlistPopupMenuListener = new PlaylistPopupMenuListener();
        removeItemListener = new RemoveItemListener();
        removeAllItemListener = new RemoveAllItemListener();
    }


    /**
     * Returns singleton instance of player view controller.
     *
     * @return Singleton instance of player view controller.
     */
    public static PlayerViewController getInstance() {
        return instance;
    }


    /**
     * Installs controller to the player view.
     *
     * @param playerView - {@link PlayerView}
     */
    public void install(PlayerView playerView) {
        logger.debug("Installing controller to the: {}", playerView);

        this.playerView = playerView;

        playerView.getSeekBar().addMouseListener(seekBarMouseListener);
        playerView.getPlayButton().addActionListener(playButtonListener);
        playerView.getPauseButton().addActionListener(pauseButtonListener);
        playerView.getNextButton().addActionListener(nextButtonListener);
        playerView.getBackButton().addActionListener(backButtonListener);
        playerView.getList().addMouseListener(playlistMouseListener);
        playerView.getPlaylistPopupMenu().addPopupMenuListener(playlistPopupMenuListener);
        playerView.getRemoveItem().addActionListener(removeItemListener);
        playerView.getRemoveAllItem().addActionListener(removeAllItemListener);
    }

    /**
     * Adds track to the playlist.
     *
     * @param track - the track to be added
     */
    public void addToPlayList(Track track) {
        if (track != null) {
            // Disallowing to add duplicate tracks to playlist.
            if (!playerView.getListModel().contains(track)) {
                // Adding track to playlist.
                playerView.getListModel().addElement(track);
            }
        }
    }

    /**
     * Adds list of tracks to the playlist.
     *
     * @param trackList - list of the tracks to be added
     */
    public void addToPlayList(List<Track> trackList) {
        if (trackList != null) {
            // Adding all tracks to the playlist.
            for (Track track : trackList) {
                // Adding track to the playlist.
                addToPlayList(track);
            }
        }
    }


    /**
     * Starts track playback.
     */
    private void play() {
        if (player.isPaused()) {
            logger.info("Resuming track playback.");

            // Stopping track playback.
            player.stop();
            // Starting track playback.
            player.play(currentTrackURI, playerEventConsumer);
            // Changing track position.
            player.seek(currentTrackPosition);
            // Showing pause button.
            setPauseVisible(true);
        } else {
            // Receiving track.
            Track track = getTrack();
            // Starting track playback.
            play(track);
        }
    }

    /**
     * Starts track playback.
     *
     * @param track - track to be played
     */
    private void play(Track track) {
        if (track != null) {
            logger.info("Trying to play track: {} - {}", track.getAlbum().getArtist().getName(), track.getTitle());

            // Receiving track URI.
            URI trackURI = DownloadLinkService.getInstance().getDownloadLinkForTrack(track);

            if (trackURI == null) {
                logger.error("Error while trying to playback the track: Can't resolve track URI!");

                // Creating error message.
                String message = "Can't resolve track URI!";
                // Showing error.
                WebOptionPane.showMessageDialog(ApplicationContext.getInstance().getApplicationWindow(), message, "Error!", WebOptionPane.ERROR_MESSAGE);
            } else {
                logger.info("Playback of the track successfully started.");

                // Setting track info.
                setTrackInfo(track);
                // Showing pause button.
                setPauseVisible(true);
                // Setting current track URI.
                currentTrackURI = trackURI;
                // Stopping previous track playback.
                player.stop();
                // Starting track playback.
                player.play(trackURI, playerEventConsumer);
            }
        }
    }

    /**
     * Pauses track playback.
     */
    private void pause() {
        if (player.isPlaying()) {
            logger.info("Playback of the track is paused.");

            // Pausing track playback.
            player.pause();
            // Showing play button.
            setPauseVisible(false);
        }
    }

    /**
     * Starts playback of the next track.
     */
    private void next() {
        // Receiving list model.
        DefaultListModel listModel = playerView.getListModel();

        if (listModel.getSize() > 0) {
            // Stopping playback.
            player.stop();

            // Calculating next track index.
            currentTrackIndex += 1;

            if (currentTrackIndex >= listModel.getSize()) {
                // Reseting track index to zero.
                currentTrackIndex %= listModel.getSize();

                // Checking is the loop button is selected.
                if (playerView.getLoopButton().isSelected()) {
                    // Starting playback.
                    play();
                } else {
                    // Resetting track info.
                    playerView.reset();
                    // Showing play button.
                    setPauseVisible(false);
                }
            } else {
                // Starting playback.
                play();
            }
        }
    }

    /**
     * Starts playback of the previous track.
     */
    private void back() {
        // Receiving list model.
        DefaultListModel listModel = playerView.getListModel();

        if (listModel.getSize() > 0) {
            // Stopping playback.
            player.stop();

            // Calculating previous track index.
            currentTrackIndex -= 1;

            if (currentTrackIndex < 0) {
                // Checking is the loop button was selected.
                if (playerView.getLoopButton().isSelected()) {
                    // Resetting the track index to the last track of list.
                    currentTrackIndex = listModel.getSize() - 1;
                    // Starting playback.
                    play();
                } else {
                    // Resetting the track index to zero.
                    currentTrackIndex = 0;
                    // Resetting track info.
                    playerView.reset();
                    // Showing play button.
                    setPauseVisible(false);
                }
            } else {
                // Starting playback.
                play();
            }
        }
    }

    /**
     * Shows/hides pause button.
     *
     * @param value - {@code true} - show, otherwise - {@code false}.
     */
    private void setPauseVisible(boolean value) {
        // Changing play button visibility.
        playerView.getPlayButton().setVisible(!value);
        // Changing pause button visibility.
        playerView.getPauseButton().setVisible(value);
    }

    /**
     * Sets track info to the player view.
     *
     * @param track - {@link Track}
     */
    private void setTrackInfo(Track track) {
        if (track != null) {
            // Resetting player view.
            playerView.reset();
            // Setting track title.
            playerView.getTrackLabel().setText(track.getTitle());
            // Setting album title.
            playerView.getAlbumLabel().setText(track.getAlbum().getTitle());
            // Setting artist title.
            playerView.getArtistLabel().setText(track.getAlbum().getArtist().getName());
            // Setting track length.
            playerView.getTotalTimeLabel().setText(track.getDuration());
        }
    }

    /**
     * Sets current track state.
     *
     * @param state - {@link PlaybackState}
     */
    private void setTrackState(PlaybackState state) {
        Track track = getTrack();

        if (track != null) {
            // Changing track state.
            playerView.getPlaylistCellRenderer().setTrackState(track, state);
            playerView.getList().repaint();
        }
    }

    /**
     * Returns current track.
     *
     * @return Current track.
     */
    private Track getTrack() {
        Track track = null;

        if (currentTrackIndex == -1) {
            currentTrackIndex = 0;
        }

        if (currentTrackIndex < playerView.getListModel().getSize()) {
            // Receiving track.
            track = (Track) playerView.getListModel().get(currentTrackIndex);
        }

        return track;
    }


    /**
     * Seek bar mouse listener.
     */
    private class SeekBarMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) && player.isPlaying()) {
                int width = playerView.getSeekBar().getWidth();
                int max = playerView.getSeekBar().getMaximum();
                int x = e.getX() + (e.getX() > (width / 2) ? 5 : 0);

                // Calculating seek bar value.
                int value = Math.round((x * max) / width);
                // Calculating player playback position.
                float position = (float) value / 100;

                // Setting seek bar value.
                playerView.getSeekBar().setValue(value);
                // Setting player playback position.
                player.seek(position);
            }
        }
    }


    /**
     * Player event consumer.
     */
    private class PlayerEventConsumer implements PlayerEventListener {
        @Override
        public void onOpening() {
            setTrackState(PlaybackState.OPENING);
        }

        @Override
        public void onPlaying() {
            setTrackState(PlaybackState.PLAYING);
        }

        @Override
        public void onPaused() {
            setTrackState(PlaybackState.PAUSED);

        }

        @Override
        public void onStopped() {
            setTrackState(null);
        }

        @Override
        public void onFinished() {
            setTrackState(null);
            next();
        }

        @Override
        public void onError() {
            setTrackState(null);
        }

        @Override
        public void onPositionChanged(float newPosition) {
            // Calculating seek bar value.
            int value = (int) (newPosition * 100);
            // Setting seek bar value.
            playerView.getSeekBar().setValue(value);
            // Setting current track position.
            currentTrackPosition = newPosition;
        }

        @Override
        public void onTimeChanged(long newTime) {
            long min = TimeUnit.MILLISECONDS.toMinutes(newTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(newTime));
            long sec = TimeUnit.MILLISECONDS.toSeconds(newTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(newTime));

            // Formatting time.
            String time = String.format("%02d:%02d", min, sec);
            // Setting current time.
            playerView.getCurrentTimeLabel().setText(time);
        }
    }


    /**
     * Play button listener.
     */
    private class PlayButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            play();
        }
    }


    /**
     * Pause button listener.
     */
    private class PauseButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            pause();
        }
    }


    /**
     * Next button listener.
     */
    private class NextButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            next();
        }
    }


    /**
     * Back button listener.
     */
    private class BackButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            back();
        }
    }


    /**
     * Playlist mouse listener.
     */
    private class PlaylistMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                // Receiving track index.
                int trackIndex = playerView.getList().getSelectedIndex();

                if (trackIndex != -1) {
                    // Changing current track state.
                    setTrackState(null);
                    // Changing current track index.
                    currentTrackIndex = trackIndex;
                    // Receiving track.
                    Track track = getTrack();
                    // Starting track playback.
                    play(track);
                }
            }
        }
    }


    /**
     * Playlist popup menu listener.
     */
    private class PlaylistPopupMenuListener implements PopupMenuListener {
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            boolean isItemSelected = playerView.getList().getSelectedIndices().length > 0;
            boolean isEmpty = playerView.getListModel().getSize() == 0;

            playerView.getRemoveItem().setEnabled(isItemSelected);
            playerView.getRemoveAllItem().setEnabled(!isEmpty);
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            // Not implemented.
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            // Not implemented.
        }
    }


    /**
     * Remove item listener.
     */
    private class RemoveItemListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Receiving selected indexes.
            int[] indexArray = playerView.getList().getSelectedIndices();

            for (int index : indexArray) {
                // Removing track from playlist.
                playerView.getListModel().remove(index);
                // Reseting track index.
                currentTrackIndex = -1;
            }
        }
    }


    /**
     * Remove all item listener.
     */
    private class RemoveAllItemListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Removing all tracks.
            playerView.getListModel().removeAllElements();
            // Reseting track index.
            currentTrackIndex = -1;
        }
    }
}
