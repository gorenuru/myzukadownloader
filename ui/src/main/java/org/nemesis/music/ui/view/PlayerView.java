package org.nemesis.music.ui.view;

import com.alee.laf.button.WebButton;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebList;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.scroll.WebScrollPane;
import org.nemesis.music.ui.component.AutoWidthLabelUI;
import org.nemesis.music.ui.renderer.PlaylistCellRenderer;
import org.nemesis.music.ui.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Player view.
 *
 * User: malbul
 * Date: 4/11/13
 * Time: 12:19 PM
 */
public class PlayerView implements View {
    private static final Font trackLabelFont = new Font("Tahoma", Font.BOLD, 11);
    private static final Font albumLabelFont = new Font("Tahoma", Font.PLAIN, 11);
    private static final Font artistLabelFont = new Font("Tahoma", Font.PLAIN, 11);

    private static final String ICON_PLAY = "icons/player/play.png";
    private static final String ICON_PAUSE = "icons/player/pause.png";
    private static final String ICON_NEXT = "icons/player/next.png";
    private static final String ICON_BACK = "icons/player/back.png";
    private static final String ICON_SHUFFLE = "icons/player/shuffle.png";
    private static final String ICON_LOOP = "icons/player/loop.png";

    private static final String ICON_REMOVE = "icons/playlist/remove.png";
    private static final String ICON_REMOVE_ALL = "icons/playlist/remove_all.png";


    private WebPanel contentPane;

    private WebLabel trackLabel;
    private WebLabel albumLabel;
    private WebLabel artistLabel;

    private WebLabel currentTimeLabel;
    private WebLabel totalTimeLabel;

    private WebButton playButton;
    private WebButton pauseButton;
    private WebButton backButton;
    private WebButton nextButton;
    private WebToggleButton shuffleButton;
    private WebToggleButton loopButton;

    private WebProgressBar seekBar;

    private DefaultListModel listModel;
    private WebScrollPane listScroll;
    private WebList list;

    private WebPopupMenu playlistPopupMenu;

    private WebMenuItem removeItem;
    private WebMenuItem removeAllItem;

    private PlaylistCellRenderer playlistCellRenderer;


    /**
     * Constructor.
     */
    public PlayerView() {
        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        // Creating content pane.
        contentPane = new WebPanel();
        contentPane.setLayout(new GridBagLayout());
    }

    @Override
    public void initStyle() {
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        // Creating track label.
        trackLabel = new WebLabel();
        trackLabel.setFont(trackLabelFont);
        trackLabel.setText(" ");
        trackLabel.setUI(new AutoWidthLabelUI(contentPane, trackLabel));

        // Creating album label.
        albumLabel = new WebLabel();
        albumLabel.setFont(albumLabelFont);
        albumLabel.setText(" ");
        albumLabel.setUI(new AutoWidthLabelUI(contentPane, albumLabel));

        // Creating artist label.
        artistLabel = new WebLabel();
        artistLabel.setFont(artistLabelFont);
        artistLabel.setText(" ");
        artistLabel.setUI(new AutoWidthLabelUI(contentPane, artistLabel));

        // Creating current time label.
        currentTimeLabel = new WebLabel();
        currentTimeLabel.setMargin(2, 5, 2, 2);
        currentTimeLabel.setText("00:00");

        // Creating total time label.
        totalTimeLabel = new WebLabel();
        totalTimeLabel.setMargin(2, 2, 2, 5);
        totalTimeLabel.setText("00:00");

        // Creating play button.
        playButton = new WebButton();
        playButton.setMargin(0, 4, 0, 0);
        playButton.setIcon(ImageUtils.loadImageIcon(ICON_PLAY));

        // Creating pause button.
        pauseButton = new WebButton();
        pauseButton.setVisible(false);
        pauseButton.setMargin(0, 1, 0, 2);
        pauseButton.setIcon(ImageUtils.loadImageIcon(ICON_PAUSE));

        // Creating back button.
        backButton = new WebButton();
        backButton.setIcon(ImageUtils.loadImageIcon(ICON_BACK));

        // Creating next button.
        nextButton = new WebButton();
        nextButton.setIcon(ImageUtils.loadImageIcon(ICON_NEXT));

        // Creating shuffle button.
        shuffleButton = new WebToggleButton();
        shuffleButton.setIcon(ImageUtils.loadImageIcon(ICON_SHUFFLE));

        // Creating loop button.
        loopButton = new WebToggleButton();
        loopButton.setIcon(ImageUtils.loadImageIcon(ICON_LOOP));

        // Creating progress bar.
        seekBar = new WebProgressBar();
        seekBar.setValue(0);
        seekBar.setMinimum(0);
        seekBar.setMaximum(100);

        // Creating list model.
        listModel = new DefaultListModel();

        // Creating remove item.
        removeItem = new WebMenuItem();
        removeItem.setText("Remove");
        removeItem.setIcon(ImageUtils.loadImageIcon(ICON_REMOVE));

        // Creating remove all item.
        removeAllItem = new WebMenuItem();
        removeAllItem.setText("Remove All");
        removeAllItem.setIcon(ImageUtils.loadImageIcon(ICON_REMOVE_ALL));

        // Creating playlist popup menu.
        playlistPopupMenu = new WebPopupMenu();
        playlistPopupMenu.add(removeItem);
        playlistPopupMenu.add(removeAllItem);

        // Creating playlist cell renderer.
        playlistCellRenderer = new PlaylistCellRenderer();

        // Creating list.
        list = new WebList(listModel);
        list.setOpaque(false);
        list.setCellRenderer(playlistCellRenderer);
        list.setComponentPopupMenu(playlistPopupMenu);
        list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        // Creating list scroll.
        listScroll = new WebScrollPane(list);
        listScroll.setDrawBorder(false);
        listScroll.setDrawFocus(false);
        listScroll.setOpaque(false);
    }

    @Override
    public void addComponents() {
        // Creating track panel.
        WebPanel trackPanel = new WebPanel();

        trackPanel.setOpaque(false);
        trackPanel.setLayout(new GridBagLayout());

        // Creating components constraints.
        GridBagConstraints trackLabelConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints albumLabelConstraints = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints artistLabelConstraints = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);

        // Adding components to the track panel.
        trackPanel.add(trackLabel, trackLabelConstraints);
        trackPanel.add(albumLabel, albumLabelConstraints);
        trackPanel.add(artistLabel, artistLabelConstraints);


        // Creating progress panel.
        WebPanel progressPanel = new WebPanel();

        progressPanel.setOpaque(false);
        progressPanel.setLayout(new BorderLayout());

        // Adding components to the progress panel.
        progressPanel.add(currentTimeLabel, BorderLayout.WEST);
        progressPanel.add(totalTimeLabel, BorderLayout.EAST);
        progressPanel.add(seekBar, BorderLayout.SOUTH);


        // Creating button panel.
        WebPanel buttonPanel = new WebPanel();

        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new GridBagLayout());

        // Creating button constraints.
        GridBagConstraints shuffleButtonConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2, 2, 2, 5), 0, 0);
        GridBagConstraints backButtonConstraints = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints playButtonConstraints = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints pauseButtonConstraints = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints nextButtonConstraints = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints loopButtonConstraints = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(2, 5, 2, 2), 0, 0);

        // Adding buttons to the button panel.
        buttonPanel.add(shuffleButton, shuffleButtonConstraints);
        buttonPanel.add(backButton, backButtonConstraints);
        buttonPanel.add(playButton, playButtonConstraints);
        buttonPanel.add(pauseButton, pauseButtonConstraints);
        buttonPanel.add(nextButton, nextButtonConstraints);
        buttonPanel.add(loopButton, loopButtonConstraints);

        // Creating panels constraints.
        GridBagConstraints trackPanelConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints progressPanelConstraints = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints buttonPanelConstraints = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0);
        GridBagConstraints listConstraints = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0);


        // Adding components to the content pane.
        contentPane.add(trackPanel, trackPanelConstraints);
        contentPane.add(progressPanel, progressPanelConstraints);
        contentPane.add(buttonPanel, buttonPanelConstraints);
        contentPane.add(listScroll, listConstraints);
    }

    @Override
    public void reset() {
        trackLabel.setText(" ");
        albumLabel.setText(" ");
        artistLabel.setText(" ");

        currentTimeLabel.setText("00:00");
        totalTimeLabel.setText("00:00");

        seekBar.setValue(0);
    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }


    /** ##################################### Getters ##################################### **/


    public WebLabel getTrackLabel() {
        return trackLabel;
    }

    public WebLabel getAlbumLabel() {
        return albumLabel;
    }

    public WebLabel getArtistLabel() {
        return artistLabel;
    }

    public WebLabel getCurrentTimeLabel() {
        return currentTimeLabel;
    }

    public WebLabel getTotalTimeLabel() {
        return totalTimeLabel;
    }

    public WebButton getPlayButton() {
        return playButton;
    }

    public WebButton getPauseButton() {
        return pauseButton;
    }

    public WebButton getBackButton() {
        return backButton;
    }

    public WebButton getNextButton() {
        return nextButton;
    }

    public WebToggleButton getShuffleButton() {
        return shuffleButton;
    }

    public WebToggleButton getLoopButton() {
        return loopButton;
    }

    public WebProgressBar getSeekBar() {
        return seekBar;
    }

    public DefaultListModel getListModel() {
        return listModel;
    }

    public WebScrollPane getListScroll() {
        return listScroll;
    }

    public WebList getList() {
        return list;
    }

    public WebPopupMenu getPlaylistPopupMenu() {
        return playlistPopupMenu;
    }

    public WebMenuItem getRemoveItem() {
        return removeItem;
    }

    public WebMenuItem getRemoveAllItem() {
        return removeAllItem;
    }

    public PlaylistCellRenderer getPlaylistCellRenderer() {
        return playlistCellRenderer;
    }
}
