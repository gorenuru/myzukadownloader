package org.nemesis.music.ui.controller;

import org.nemesis.music.core.downloader.AlbumDownloader;
import org.nemesis.music.core.event.DownloadEvent;
import org.nemesis.music.core.listener.DownloadProgressListener;
import org.nemesis.music.domain.Album;
import org.nemesis.music.domain.Track;
import org.nemesis.music.ui.callback.TaskCallbackAdapter;
import org.nemesis.music.ui.service.MusicService;
import org.nemesis.music.ui.state.DownloadState;
import org.nemesis.music.ui.view.DownloadView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: malbul
 * Date: 4/19/13
 * Time: 11:34 AM
 */
public class DownloadViewController {
    private static final Logger logger = LoggerFactory.getLogger(DownloadViewController.class);

    private static final DownloadViewController instance = new DownloadViewController();

    private Map<Album, DefaultMutableTreeNode> albumMapping;

    private DownloadView downloadView;

    private AlbumDownloadProgressListener albumDownloadProgressListener;


    /**
     * Private constructor.
     */
    private DownloadViewController() {
        albumMapping = new HashMap<Album, DefaultMutableTreeNode>();

        albumDownloadProgressListener = new AlbumDownloadProgressListener();
    }


    /**
     * Returns singleton instance of download view controller.
     *
     * @return Singleton instance of download view controller.
     */
    public static DownloadViewController getInstance() {
        return instance;
    }


    /**
     * Installs controller to the download view.
     *
     * @param downloadView - {@link DownloadView}
     */
    public void install(DownloadView downloadView) {
        logger.debug("Installing controller to the: {}", downloadView);

        this.downloadView = downloadView;


    }

    /**
     * Downloads album.
     *
     * @param album - album to be downloaded
     */
    public void downloadAlbum(final Album album) {
        if (album != null && !albumMapping.keySet().contains(album)) {
            // Receiving album tracks.
            MusicService.getInstance().getAlbumTracks(album, new TaskCallbackAdapter<List<Track>>() {
                @Override
                public void onDone(List<Track> result) {
                    if (result != null) {
                        // Creating album node.
                        DefaultMutableTreeNode albumNode = new DefaultMutableTreeNode(album);

                        for (Track track : result) {
                            // Creating track node.
                            DefaultMutableTreeNode trackNode = new DefaultMutableTreeNode(track);
                            // Disabling child nodes.
                            trackNode.setAllowsChildren(false);
                            // Adding track node to album node.
                            albumNode.add(trackNode);
                        }

                        // Adding album node to the root node.
                        downloadView.getRootNode().add(albumNode);
                        // Reloading model.
                        downloadView.getTreeModel().reload();
                        // Changing album download state.
                        downloadView.getAlbumCellRenderer().setAlbumState(album, DownloadState.QUEUE);

                        // Downloading album.
                        AlbumDownloader.getInstance().downloadAlbum(album, albumDownloadProgressListener);
                    }
                }
            });
        }
    }


    /**
     * Album downloader listener.
     */
    private class AlbumDownloadProgressListener implements DownloadProgressListener {
        @Override
        public void albumDownloadStarted(DownloadEvent event) {
            downloadView.getAlbumCellRenderer().setAlbumState(event.getAlbum(), DownloadState.IN_PROGRESS);
            downloadView.getContentPane().repaint();
        }

        @Override
        public void albumDownloadCompleted(DownloadEvent event) {
            downloadView.getAlbumCellRenderer().setAlbumState(event.getAlbum(), DownloadState.DONE);
            downloadView.getContentPane().repaint();

            if (event.getException() != null) {
                System.out.println(event.getAlbum().getTitle());
            }
        }

        @Override
        public void trackDownloadStarted(DownloadEvent event) {
            downloadView.getTrackCellRenderer().setTrackState(event.getTrack(), DownloadState.IN_PROGRESS);
            downloadView.getContentPane().repaint();
        }

        @Override
        public void trackDownloaded(DownloadEvent event) {
            downloadView.getTrackCellRenderer().setTrackState(event.getTrack(), DownloadState.DONE);
            downloadView.getContentPane().repaint();
        }

        @Override
        public void trackDownloadError(DownloadEvent event) {
            downloadView.getAlbumCellRenderer().setAlbumState(event.getAlbum(), DownloadState.ERROR);
            downloadView.getTrackCellRenderer().setTrackState(event.getTrack(), DownloadState.ERROR);
            downloadView.getContentPane().repaint();
        }
    }
}
