package org.nemesis.music.ui.view;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tree.WebTree;
import org.nemesis.music.ui.renderer.AlbumCellRenderer;
import org.nemesis.music.ui.renderer.TrackCellRenderer;
import org.nemesis.music.ui.renderer.TreeCellRenderer;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;

/**
 * User: malbul
 * Date: 4/18/13
 * Time: 4:01 PM
 */
public class DownloadView implements View {
    private static final String ICON = "icons/download.png";
    private static final String TITLE = "Downloads";

    private WebPanel contentPane;

    private DefaultMutableTreeNode rootNode;
    private DefaultTreeModel treeModel;
    private WebScrollPane treeScroll;
    private WebTree tree;

    private TreeCellRenderer treeCellRenderer;
    private AlbumCellRenderer albumCellRenderer;
    private TrackCellRenderer trackCellRenderer;


    /**
     * Constructor.
     */
    public DownloadView() {
        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        // Creating content pane.
        contentPane = new WebPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setVisible(false);
    }

    @Override
    public void initStyle() {
        contentPane.setUndecorated(false);
        contentPane.setOpaque(true);
    }

    @Override
    public void initComponents() {
        // Creating root node.
        rootNode = new DefaultMutableTreeNode("root");

        // Creating tree model.
        treeModel = new DefaultTreeModel(rootNode);

        // Creating album cell renderer.
        albumCellRenderer = new AlbumCellRenderer();

        // Creating track cell renderer.
        trackCellRenderer = new TrackCellRenderer();

        // Creating tree cell renderer.
        treeCellRenderer = new TreeCellRenderer(albumCellRenderer, trackCellRenderer);

        // Creating tree.
        tree = new WebTree(treeModel);
        tree.setCellRenderer(treeCellRenderer);
        tree.setShowsRootHandles(true);
        tree.setRootVisible(false);
        tree.setOpaque(false);
        tree.setAutoscrolls(true);

        // Creating tree scroll.
        treeScroll = new WebScrollPane(tree);
        treeScroll.setDrawBorder(false);
        treeScroll.setDrawFocus(false);
    }

    @Override
    public void addComponents() {
        // Adding components to the content pane.
        contentPane.add(treeScroll, BorderLayout.CENTER);
    }

    @Override
    public void reset() {

    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }


    /** ##################################### Getters ##################################### **/


    public DefaultMutableTreeNode getRootNode() {
        return rootNode;
    }

    public DefaultTreeModel getTreeModel() {
        return treeModel;
    }

    public WebScrollPane getTreeScroll() {
        return treeScroll;
    }

    public WebTree getTree() {
        return tree;
    }

    public TreeCellRenderer getTreeCellRenderer() {
        return treeCellRenderer;
    }

    public AlbumCellRenderer getAlbumCellRenderer() {
        return albumCellRenderer;
    }

    public TrackCellRenderer getTrackCellRenderer() {
        return trackCellRenderer;
    }
}
