package org.nemesis.music.ui.state;

/**
 * User: malbul
 * Date: 4/19/13
 * Time: 11:47 AM
 */
public enum DownloadState {
    QUEUE,              // Album are in queue on download.
    IN_PROGRESS,        // Downloading of album in progress.
    DONE,               // Downloading of album successfully completed.
    ERROR;              // Error while downloading of album.
}
