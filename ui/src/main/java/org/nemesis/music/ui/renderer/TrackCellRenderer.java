package org.nemesis.music.ui.renderer;

import com.alee.laf.tree.WebTreeCellRenderer;
import com.alee.laf.tree.WebTreeElement;
import org.nemesis.music.domain.Track;
import org.nemesis.music.ui.state.DownloadState;
import org.nemesis.music.ui.utils.RendererUtils;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * User: Eng
 * Date: 4/17/13
 * Time: 8:44 PM
 */
public class TrackCellRenderer extends WebTreeCellRenderer {
    private Map<Track, DownloadState> trackDownloadStateMap;


    /**
     * Constructor.
     */
    public TrackCellRenderer() {
        trackDownloadStateMap = new HashMap<Track, DownloadState>();
    }


    /**
     * Sets track download state.
     *
     * @param track - track
     * @param state - download state
     */
    public void setTrackState(Track track, DownloadState state) {
        if (track != null) {
            if (state == null) {
                // Removing track from map.
                trackDownloadStateMap.remove(track);
            } else {
                // Changing track state.
                trackDownloadStateMap.put(track, state);
            }
        }
    }

    @Override
    public WebTreeElement getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        WebTreeElement component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value != null) {
            Track track = (Track) value;

            // Receiving track download state from map.
            DownloadState state = trackDownloadStateMap.get(track);

            // Formatting track title.
            String trackTitle = getFormattedTrackTitle(track);
            // Getting text color.
            Color textColor = RendererUtils.getTextColor(state);
            // Getting icon.
            ImageIcon icon = RendererUtils.getImageIcon(state);

            // Setting track title.
            component.setText(trackTitle);
            // Setting text color.
            component.setForeground(textColor);

            if (icon != null) {
                // Setting icon.
                component.setIcon(icon);
            }
        }

        return component;
    }

    /**
     * Formats the track title.
     *
     * @param track - track
     * @return Formatted track title.
     */
    private String getFormattedTrackTitle(Track track) {
        String trackTitle = null;

        if (track.getTrackNo() > 0) {
            trackTitle = String.format("%d - %s (%s)", track.getTrackNo(), track.getTitle(), track.getDuration());
        } else {
            trackTitle = String.format("%s (%s)", track.getTitle(), track.getDuration());
        }

        return trackTitle;
    }
}
