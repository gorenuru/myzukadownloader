package org.nemesis.music.ui.renderer;

import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebListCellRenderer;
import org.nemesis.music.domain.Track;
import org.nemesis.music.ui.state.PlaybackState;
import org.nemesis.music.ui.utils.RendererUtils;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * User: malbul
 * Date: 4/18/13
 * Time: 12:53 PM
 */
public class PlaylistCellRenderer extends WebListCellRenderer {
    private Map<Track, PlaybackState> trackPlaybackStateMap;


    /**
     * Constructor.
     */
    public PlaylistCellRenderer() {
        trackPlaybackStateMap = new HashMap<Track, PlaybackState>();
    }


    /**
     * Sets track download state.
     *
     * @param track - track
     * @param state - download state
     */
    public void setTrackState(Track track, PlaybackState state) {
        if (track != null) {
            if (state == null) {
                // Removing track from map.
                trackPlaybackStateMap.remove(track);
            } else {
                // Changing track state.
                trackPlaybackStateMap.put(track, state);
            }
        }
    }


    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        if (value != null) {
            WebLabel label = (WebLabel) component;
            Track track = (Track) value;

            // Receiving track playback state.
            PlaybackState state = trackPlaybackStateMap.get(track);

            // Formatting track title.
            String trackTitle = String.format("%s. %s - %s (%s)", index + 1, track.getAlbum().getArtist().getName(), track.getTitle(), track.getDuration());
            // Receiving icon.
            ImageIcon icon = RendererUtils.getImageIcon(state);

            // Setting track title.
            label.setText(trackTitle);
            // Setting track icon.
            label.setIcon(icon);

            if (list.getWidth() > 0) {
                int margin = label.getMargin().left + label.getMargin().right;
                int insets = label.getInsets().left + label.getInsets().right;

                int width = list.getWidth() - (margin + insets);
                int height = label.getPreferredSize().height;

                label.setPreferredSize(new Dimension(width, height));
            }
        }

        return component;
    }
}
