package org.nemesis.music.ui.view;

import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tree.WebTree;
import org.nemesis.music.ui.renderer.AlbumCellRenderer;
import org.nemesis.music.ui.renderer.TrackCellRenderer;
import org.nemesis.music.ui.renderer.TreeCellRenderer;
import org.nemesis.music.ui.utils.ImageUtils;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;

/**
 * Album view.
 *
 * User: malbul
 * Date: 4/17/13
 * Time: 4:53 PM
 */
public class AlbumView implements View {
    private static final String ICON_ADD = "icons/add.png";
    private static final String ICON_DOWNLOAD = "icons/download.png";

    private WebPanel contentPane;

    private DefaultMutableTreeNode rootNode;
    private DefaultTreeModel treeModel;
    private WebScrollPane treeScroll;
    private WebTree tree;

    private WebPopupMenu albumPopupMenu;
    private WebPopupMenu trackPopupMenu;

    private WebMenuItem albumAddToPlaylistItem;
    private WebMenuItem albumDownloadItem;

    private WebMenuItem trackAddToPlaylistItem;

    private AlbumCellRenderer albumCellRenderer;
    private TrackCellRenderer trackCellRenderer;
    private TreeCellRenderer treeCellRenderer;


    /**
     * Constructor.
     */
    public AlbumView() {
        init();
        initStyle();
        initComponents();
        addComponents();
    }


    @Override
    public void init() {
        // Creating content pane.
        contentPane = new WebPanel();
        contentPane.setLayout(new BorderLayout());
    }

    @Override
    public void initStyle() {
        contentPane.setOpaque(false);
    }

    @Override
    public void initComponents() {
        // Creating root node.
        rootNode = new DefaultMutableTreeNode("root");

        // Creating tree model.
        treeModel = new DefaultTreeModel(rootNode);

        // Creating album cell renderer.
        albumCellRenderer = new AlbumCellRenderer();

        // Creating track cell renderer.
        trackCellRenderer = new TrackCellRenderer();

        // Creating tree cell renderer;
        treeCellRenderer = new TreeCellRenderer(albumCellRenderer, trackCellRenderer);

        // Creating tree.
        tree = new WebTree(treeModel);
        tree.setCellRenderer(treeCellRenderer);
        tree.setShowsRootHandles(true);
        tree.setRootVisible(false);
        tree.setOpaque(false);
        tree.setAutoscrolls(true);

        // Creating tree scroll pane.
        treeScroll = new WebScrollPane(tree);
        treeScroll.setDrawBorder(false);
        treeScroll.setDrawFocus(false);

        // Creating album add to playlist item.
        albumAddToPlaylistItem = new WebMenuItem();
        albumAddToPlaylistItem.setText("Add to playlist");
        albumAddToPlaylistItem.setIcon(ImageUtils.loadImageIcon(ICON_ADD));

        // Creating album download item.
        albumDownloadItem = new WebMenuItem();
        albumDownloadItem.setText("Download");
        albumDownloadItem.setIcon(ImageUtils.loadImageIcon(ICON_DOWNLOAD));

        // Creating track add to playlist item.
        trackAddToPlaylistItem = new WebMenuItem();
        trackAddToPlaylistItem.setText("Add to playlist");
        trackAddToPlaylistItem.setIcon(ImageUtils.loadImageIcon(ICON_ADD));

        // Creating album popup menu.
        albumPopupMenu = new WebPopupMenu();
        albumPopupMenu.add(albumAddToPlaylistItem);
        albumPopupMenu.add(albumDownloadItem);

        // Creating track popup menu.
        trackPopupMenu = new WebPopupMenu();
        trackPopupMenu.add(trackAddToPlaylistItem);
    }

    @Override
    public void addComponents() {
        // Adding components to the content pane.
        contentPane.add(treeScroll, BorderLayout.CENTER);
    }

    @Override
    public void reset() {

    }

    @Override
    public Component getContentPane() {
        return contentPane;
    }


    /** ##################################### Getters ##################################### **/


    public DefaultMutableTreeNode getRootNode() {
        return rootNode;
    }

    public DefaultTreeModel getTreeModel() {
        return treeModel;
    }

    public WebScrollPane getTreeScroll() {
        return treeScroll;
    }

    public WebTree getTree() {
        return tree;
    }

    public AlbumCellRenderer getAlbumCellRenderer() {
        return albumCellRenderer;
    }

    public TrackCellRenderer getTrackCellRenderer() {
        return trackCellRenderer;
    }

    public TreeCellRenderer getTreeCellRenderer() {
        return treeCellRenderer;
    }

    public WebPopupMenu getAlbumPopupMenu() {
        return albumPopupMenu;
    }

    public WebPopupMenu getTrackPopupMenu() {
        return trackPopupMenu;
    }

    public WebMenuItem getAlbumAddToPlaylistItem() {
        return albumAddToPlaylistItem;
    }

    public WebMenuItem getAlbumDownloadItem() {
        return albumDownloadItem;
    }

    public WebMenuItem getTrackAddToPlaylistItem() {
        return trackAddToPlaylistItem;
    }
}
