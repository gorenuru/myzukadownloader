package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 11/13/12
 * Time: 3:12 PM
 */
class Track extends ScrapableObject {
    String id
    Integer trackNo
    String title
    String duration
    String size
    String bitRate
    URI link
    Album album

    @Override
    public String toString() {
        return "Track{" +
                "id='" + id + '\'' +
                ", trackNo=" + trackNo +
                ", title='" + title + '\'' +
                ", duration='" + duration + '\'' +
                ", size='" + size + '\'' +
                ", bitRate='" + bitRate + '\'' +
                ", link=" + link +
                '}';
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Track track = (Track) o

        if (id != track.id) return false

        return true
    }

    int hashCode() {
        return (id != null ? id.hashCode() : 0)
    }
}
