package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 11/13/12
 * Time: 3:10 PM
 */
class Album extends ScrapableObject {
    String id
    String title
    String releaseDate
    Integer trackCount
    String rating
    URI link
    Artist artist
    AlbumType type = AlbumType.STUDIO

    @Override
    public String toString() {
        return "Album{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", trackCount=" + trackCount +
                ", rating='" + rating + '\'' +
                ", link=" + link +
                ", artist=" + artist +
                ", type=" + type +
                '}';
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Album album = (Album) o

        if (id != album.id) return false

        return true
    }

    int hashCode() {
        return (id != null ? id.hashCode() : 0)
    }
}
