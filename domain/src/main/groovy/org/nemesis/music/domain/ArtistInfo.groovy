package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 4/23/13
 * Time: 10:36 AM
 */
class ArtistInfo {
    private URI photo
    private String name
    private List<String> genres = new ArrayList<>()
    private List<String> styles = new ArrayList<>()
    private String active
    private String formed
    private List<String> groupMembers = new ArrayList<>()
    private String biography

    URI getPhoto() {
        return photo
    }

    void setPhoto(URI photo) {
        this.photo = photo
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    List<String> getGenres() {
        return genres
    }

    void setGenres(List<String> genres) {
        this.genres = genres
    }

    List<String> getStyles() {
        return styles
    }

    void setStyles(List<String> styles) {
        this.styles = styles
    }

    String getActive() {
        return active
    }

    void setActive(String active) {
        this.active = active
    }

    String getFormed() {
        return formed
    }

    void setFormed(String formed) {
        this.formed = formed
    }

    List<String> getGroupMembers() {
        return groupMembers
    }

    void setGroupMembers(List<String> groupMembers) {
        this.groupMembers = groupMembers
    }

    String getBiography() {
        return biography
    }

    void setBiography(String biography) {
        this.biography = biography
    }
}
