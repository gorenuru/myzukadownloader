package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 12:49 PM
 *
 * Enumeration for scraper type
 */
public enum ScraperType {
    MYZUKA,
    JETUNE,
    MUSICMP3SPB,
    VKONTAKTE;
}