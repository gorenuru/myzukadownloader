package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 4/23/13
 * Time: 3:39 PM
 */
class AlbumInfo {
    private URI cover
    private String name
    private BigDecimal editorRating
    private String releaseDate
    private String duration
    private List<String> genres = new ArrayList<>()
    private List<String> styles = new ArrayList<>()
    private String description

    URI getCover() {
        return cover
    }

    void setCover(URI cover) {
        this.cover = cover
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    BigDecimal getEditorRating() {
        return editorRating
    }

    void setEditorRating(BigDecimal editorRating) {
        this.editorRating = editorRating
    }

    String getReleaseDate() {
        return releaseDate
    }

    void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate
    }

    String getDuration() {
        return duration
    }

    void setDuration(String duration) {
        this.duration = duration
    }

    List<String> getGenres() {
        return genres
    }

    void setGenres(List<String> genres) {
        this.genres = genres
    }

    List<String> getStyles() {
        return styles
    }

    void setStyles(List<String> styles) {
        this.styles = styles
    }

    String getDescription() {
        return description
    }

    void setDescription(String description) {
        this.description = description
    }
}
