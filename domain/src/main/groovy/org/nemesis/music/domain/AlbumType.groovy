package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 12/25/12
 * Time: 1:20 PM
 *
 * The type describes what is this album about (Studio, EP, Compilation e.t.c)
 */
public enum AlbumType {
    STUDIO,
    EP,
    SINGLE,
    COMPILATION_OF_ARTIST,
    DEMO,
    SOUNDTRACK,
    LIVE,
    MIXTAPE,
    DJ_MIX,
    BOOTLEG,
    COMPILATION_OF_DIFFERENT_ARTISTS,
    UNOFFICIAL_COMPILATIONS,
    OTHER
}