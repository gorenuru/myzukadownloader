package org.nemesis.music.domain

/**
 * User: aalbul
 * Date: 11/13/12
 * Time: 3:09 PM
 */
class Artist extends ScrapableObject {
    String id
    String name
    Integer trackCount
    URI link

    @Override
    public String toString() {
        return "Artist{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", trackCount=" + trackCount +
                ", link=" + link +
                '}';
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Artist artist = (Artist) o

        if (id != artist.id) return false

        return true
    }

    int hashCode() {
        return (id != null ? id.hashCode() : 0)
    }
}

