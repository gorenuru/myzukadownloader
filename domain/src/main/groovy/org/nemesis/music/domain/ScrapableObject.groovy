package org.nemesis.music.domain

/**
 * User: nuru
 * Date: 26.12.12
 * Time: 21:00
 *
 * This class must be a supper class for all the objects that are returned from scrapers
 * It holds the type of scraper that scraped this object and helps to determine what scrapers to use in future
 */
abstract class ScrapableObject {
    ScraperType scraperType
}
